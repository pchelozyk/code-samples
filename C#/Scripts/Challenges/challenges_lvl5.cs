﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using System.Collections;
using System.Text;

public class challenges_lvl5 : Challenges
{
    //Объект для запуска cutScene
    public PlayableDirector interactiveCutScene;

    //уровень сложности уровня
    //1 -> min уровень сложности
    //3 -> max уровень сложности
    private int difficulty;
    //время начала уровня 
    private float startTime;

    private int savedUnits;
    private int destroyedGates;
    private int placedLadders;
    private int destroyedObj;
    private int casualties;

    private bool allChallengesDisplayed;

    private void Awake()
    {
        //инициализация сложности для текущего уровня 
        difficulty = DifficultySettings.currentDifficulty;
    }

    private void Start()
    {
        //инициализация времени начала уровня
        startTime = Time.time;

        //инициализация стартовых значений перед началом уровня
        savedUnits = -1;
        destroyedGates = -1;
        placedLadders = -1;
        destroyedObj = 0;
        casualties = 0;

        //Очистка всех значений из других скриптов
        GameForShield.completedGameCount = 0;
        GameForArcher.completedGameCount = 0;
        GameForEngineer.completedGameCount = 0;
        GameForRam.completedGameCount = 0;
        GameForLadder.completedGameCount = 0;
        GameForSpears.completedGameCount = 0;
        GameForBridge.completedGameCount = 0;

        allChallengesDisplayed = false;

        //создание панели вывода для каждого испытания в зависимости от уровня сложности
        if (difficulty == 1)
            ChallengePanelPrepare(4);
        else if (difficulty == 2)
            ChallengePanelPrepare(5);
        else if (difficulty == 3)
            ChallengePanelPrepare(4);

        continueGameButton.onGameContinue.AddListener(ShowFinalChallenges);
    }

    private void ShowFinalChallenges()
    {
        savedUnits = -1;
        destroyedGates = -1;
        placedLadders = -1;
    }

    //расчет и вывод данных о прогрессе выполнения заданий
    private void Update()
    {
        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 1 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Easy.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Easy.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Easy.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Easy.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Easy.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Easy.savedUnitsLimit && destroyedGates >= Easy.destroyedGatesLimit
            && placedLadders >= Easy.placedLaddersLimit && casualties <= Easy.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Easy.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                StartCoroutine(LevelCompleteDelayed());
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Easy.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Easy.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Easy.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 2 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Normal.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Normal.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Normal.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (destroyedObj != GameForEngineer.completedGameCount)
            {
                int currentValue = destroyedObj;
                destroyedObj = GameForEngineer.completedGameCount;

                if (currentValue < Normal.destroyedObjLimit)
                {
                    ChallengePanelSetText(3, 12, destroyedObj, Normal.destroyedObjLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Normal.casualtiesLimit)
                {
                    ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                    ChallengePanelActivate(4, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Normal.savedUnitsLimit && destroyedGates >= Normal.destroyedGatesLimit
            && placedLadders >= Normal.placedLaddersLimit && destroyedObj >= Normal.destroyedObjLimit
            && casualties <= Normal.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 12, destroyedObj, Normal.destroyedObjLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Normal.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 12, destroyedObj, Normal.destroyedObjLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 12, destroyedObj, Normal.destroyedObjLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);
                ChallengePanelDeactivate(4);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 3 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Hard.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Hard.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Hard.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Hard.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Hard.savedUnitsLimit && destroyedGates >= Hard.destroyedGatesLimit
            && placedLadders >= Hard.placedLaddersLimit && casualties <= Hard.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Hard.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }
    }

    private IEnumerator LevelCompleteDelayed()
    {
        progressUpdate.challengesComplete = true;

        interactiveCutScene.enabled = true;
        double timeDelay = interactiveCutScene.duration;

        yield return new WaitForSeconds((float)timeDelay);

        OnLevelComplete(startTime);
    }

    private static class Easy
    {
        public static readonly int savedUnitsLimit = 15;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 2;
        public static readonly int casualtiesLimit = 40;
    }

    private static class Normal
    {
        public static readonly int savedUnitsLimit = 30;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 2;
        public static readonly int destroyedObjLimit = 5;
        public static readonly int casualtiesLimit = 20;
    }

    private static class Hard
    {
        public static readonly int savedUnitsLimit = 40;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 4;
        public static readonly int casualtiesLimit = 10;
    }
}