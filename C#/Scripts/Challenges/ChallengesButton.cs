﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengesButton : MonoBehaviour
{
    public Transform challengesHolder;
    public Sprite defaultSprite;
    public Sprite activeSprite;
    private Button button;
    private Image buttonImage;

    private Animator buttonAnimator;
    private int refreshAnimHash;

    private bool activeState;

    private void Start()
    {
        buttonImage = GetComponent<Image>();
        button = GetComponent<Button>();
        button.onClick.AddListener(ChangeState);

        buttonAnimator = GetComponent<Animator>();
        refreshAnimHash = Animator.StringToHash("Refresh");

        activeState = false;
    }

    public bool InActiveState()
    {
        return activeState;
    }

    private void ChangeState()
    {
        if (activeState)
        {
            buttonImage.sprite = defaultSprite;
            buttonAnimator.Play(refreshAnimHash, 0, 0);

            TimeManager.TimeUnfreeze();

            activeState = false;
        }
        else
        {
            buttonImage.sprite = activeSprite;
            buttonAnimator.Play(refreshAnimHash, 0, 0);

            TimeManager.TimeFreeze();

            activeState = true;
        }

        button.interactable = false;
        StartCoroutine(SetInteractableDelayed(3.0f));
    }

    private IEnumerator SetInteractableDelayed(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        button.interactable = true;
    }

    public void OnDisable()
    {
        StopAllCoroutines();
        button.interactable = true;

        if (activeState)
        {
            buttonImage.sprite = defaultSprite;
            activeState = false;
        }

        for (int i = 0; i < challengesHolder.childCount; i++)
        {
            challengesHolder.GetChild(i).gameObject.SetActive(false);
        }
    }
}