﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class Challenges : MonoBehaviour
{
    public ContinueGame continueGameButton;
    public Button pauseButton;
    public ChallengesButton challengesButton;
    public ChallengePanel challengePanelInstance;
    public Transform challengesHolder;
    public GameObject winPanel;
    public GameObject losePanel;

    //экземпляр класса, который хранит состояние прохождения уровня
    public ProgressUpdate progressUpdate;

    //панели вывода итогового времени и потерь
    public Text winTimeText;
    public Text winCasualtiesText;
    public Text loseTimeText;
    public Text loseCasualtiesText;


    //поля вывода состояния испытаний при победе/поражении
    public Text winPanelText;
    public Text losePanelText;

    //список всех панелей с испытаниями
    private List<ChallengePanel> challengePanelList = new List<ChallengePanel>();
    //генерируемый текст для отображения
    private StringBuilder challengeText = new StringBuilder(32);

    //метод, который выполняется при выполнении всех условий для прохождения уровня
    protected void OnLevelComplete(float startTime)
    {
        LineBrain.SetGlobalDragState(false);
        //Отключение кнопки паузы
        pauseButton.gameObject.SetActive(false);
        //Отключение кнопки отображения испытаний
        challengesButton.gameObject.SetActive(false);

        //подтверждение выполнения условий уровня
        progressUpdate.challengesComplete = true;

        //отображение победной панели
        winPanel.SetActive(true);

        //Вывод времени окончания уровня
        int Timer = (int)(Time.time - startTime);

        if (Timer < 60) // когда время прохождения уровня меньше 1 минуты
        {
            //формирование строки вывода
            string temp = LocalizationManager.instance.GetLocalizedValue("time_text") + Timer.ToString() + LocalizationManager.instance.GetLocalizedValue("sec_text");
            //вывод данных
            winTimeText.text = temp;
        }
        else
        {
            //формирование строки вывода
            string temp = LocalizationManager.instance.GetLocalizedValue("time_text") + (Timer / 60).ToString() + LocalizationManager.instance.GetLocalizedValue("min_text") + (Timer % 60).ToString() + LocalizationManager.instance.GetLocalizedValue("sec_text");
            //вывод данных
            winTimeText.text = temp;
        }

        //вывод количества потерь
        winCasualtiesText.text = LocalizationManager.instance.GetLocalizedValue("casualties_text") + CasualtiesManager.GetCasualties().ToString();
    }

    //метод, который выполняется при нарушении одного из условий при прохождении уровня
    protected void OnLevelFailed(float startTime)
    {
        LineBrain.SetGlobalDragState(false);
        //Отключение кнопки паузы
        pauseButton.gameObject.SetActive(false);
        //Отключение кнопки отображения испытаний
        challengesButton.gameObject.SetActive(false);
        //отображение панели поражения
        losePanel.SetActive(true);

        TimeManager.TimeFreeze();

        //Вывод времени провала 
        int Timer = (int)(Time.time - startTime);

        if (Timer < 60)
        {
            //формирование строки вывода
            string temp = LocalizationManager.instance.GetLocalizedValue("time_text") + Timer.ToString() + LocalizationManager.instance.GetLocalizedValue("sec_text");
            //вывод данных
            loseTimeText.text = temp;
        }
        else
        {
            //формирование строки вывода
            string temp = LocalizationManager.instance.GetLocalizedValue("time_text") + (Timer / 60).ToString() + LocalizationManager.instance.GetLocalizedValue("min_text") + (Timer % 60).ToString() + LocalizationManager.instance.GetLocalizedValue("sec_text");
            //вывод данных
            loseTimeText.text = temp;
        }

        //вывод количества потерь
        loseCasualtiesText.text = LocalizationManager.instance.GetLocalizedValue("casualties_text") + CasualtiesManager.GetCasualties().ToString();
    }

    protected string GetFullText()
    {
        challengeText.Clear();

        for (int i = 0; i < challengePanelList.Count; i++)
        {
            challengeText.AppendFormat($"{challengePanelList[i].GetText()}\n");
        }

        return challengeText.ToString();
    }

    protected void ChallengePanelSetText(int panelIndex, int messageIndex, int valueCurrent, int valueMax, bool clampValue)
    {
        if (challengePanelList.Count <= panelIndex)
            return;

        challengeText.Clear();

        if (valueCurrent < 0)
            valueCurrent = 0;

        if (clampValue && valueCurrent > valueMax)
            valueCurrent = valueMax;

        switch (messageIndex)
        {
            case 0:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("units_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 1:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("destroyed_walls_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 2:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("blocked_shots_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 3:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("casualties_text")} {valueCurrent}/{valueMax}");
                break;
            case 4:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("fired_shots_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 5:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("destroyed_gates_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 6:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("stairs_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 7:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("torch_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 8:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("preparation_challenges_lvl3_veryhard2")} ");
                if (valueCurrent == 1)
                    challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("active_challenge_text")}");
                else if (valueCurrent == 0)
                    challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("failed_challenge_text")}");
                break;
            case 9:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("bridges_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 10:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("cavalry_counter_text")} {valueCurrent}/{valueMax}");
                break;
            case 11:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("preparation_challenges_lvl4_veryhard2")} ");
                if (valueCurrent == 1)
                    challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("active_challenge_text")}");
                else if (valueCurrent == 0)
                    challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("failed_challenge_text")}");
                break;
            case 12:
                challengeText.AppendFormat($"{LocalizationManager.instance.GetLocalizedValue("destroyed_palings_counter_text")} {valueCurrent}/{valueMax}");
                break;
            default:
                challengeText.AppendFormat("Missing challenge!");
                break;
        }

        challengePanelList[panelIndex].SetText(challengeText.ToString());
    }

    protected void ChallengePanelSetText(int panelIndex, int messageIndex, int valueCurrent, int valueMax)
    {
        ChallengePanelSetText(panelIndex, messageIndex, valueCurrent, valueMax, true);
    }

    protected void ChallengePanelSetText(int panelIndex, int messageIndex, int value)
    {
        ChallengePanelSetText(panelIndex, messageIndex, value, 0, false);
    }

    protected void ChallengePanelPrepare(int panelCount)
    {
        challengePanelList.Clear();

        for (int i = 0; i < panelCount; i++)
        {
            ChallengePanel additionalPanel = Instantiate(challengePanelInstance, challengesHolder);
            additionalPanel.name = $"Challenge {i}";
            challengePanelList.Add(additionalPanel);
        }
    }

    protected void ChallengePanelActivate(int panelIndex, bool hasExitTime)
    {
        if (challengePanelList.Count <= panelIndex)
            return;

        challengePanelList[panelIndex].Show(hasExitTime);
    }

    protected void ChallengePanelDeactivate(int panelIndex)
    {
        if (challengePanelList.Count <= panelIndex)
            return;

        challengePanelList[panelIndex].Hide();
    }

    protected void ShowAll()
    {
        for (int i = 0; i < challengePanelList.Count; i++)
        {
            challengePanelList[i].Show(false, true);
        }
    }

    protected void HideAllImmediate()
    {
        for (int i = 0; i < challengePanelList.Count; i++)
        {
            challengePanelList[i].gameObject.SetActive(false);
        }
    }
}
