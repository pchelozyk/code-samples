﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class challenges_lvl4 : Challenges
{
    //уровень сложности уровня
    //1 -> min уровень сложности
    //3 -> max уровень сложности
    private int difficulty;
    //время начала уровня 
    private float startTime;

    private int savedUnits;
    private int placedBridges;
    private int shotCount;
    private int blockCount;
    private int destroyedCentaurs;
    private int casualties;

    private bool allChallengesDisplayed;

    private void Awake()
    {
        //инициализация сложности для текущего уровня 
        difficulty = DifficultySettings.currentDifficulty;
    }

    private void Start()
    {
        //инициализация времени начала уровня
        startTime = Time.time;

        //инициализация стартовых значений перед началом уровня
        savedUnits = -1;
        placedBridges = -1;
        shotCount = 0;
        blockCount = 0;
        destroyedCentaurs = 0;
        casualties = 0;

        //Очистка всех значений из других скриптов
        GameForShield.completedGameCount = 0;
        GameForArcher.completedGameCount = 0;
        GameForSpears.completedGameCount = 0;
        GameForBridge.completedGameCount = 0;

        allChallengesDisplayed = false;

        //создание панели вывода для каждого испытания в зависимости от уровня сложности
        if (difficulty == 1)
            ChallengePanelPrepare(5);
        else if (difficulty == 2)
            ChallengePanelPrepare(5);
        else if (difficulty == 3)
            ChallengePanelPrepare(4);

        continueGameButton.onGameContinue.AddListener(ShowFinalChallenges);
    }

    private void ShowFinalChallenges()
    {
        savedUnits = -1;
        placedBridges = -1;
    }

    //расчет и вывод данных о прогрессе выполнения заданий
    private void Update()
    {
        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 1 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Easy.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (placedBridges != GameForBridge.completedGameCount)
                {
                    int currentValue = placedBridges;
                    placedBridges = GameForBridge.completedGameCount;

                    if (currentValue < Easy.placedBridgesLimit)
                    {
                        ChallengePanelSetText(1, 9, placedBridges, Easy.placedBridgesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (shotCount != GameForArcher.completedGameCount)
            {
                int currentValue = shotCount;
                shotCount = GameForArcher.completedGameCount;

                if (currentValue < Easy.shotCountLimit)
                {
                    ChallengePanelSetText(2, 4, shotCount, Easy.shotCountLimit);
                    ChallengePanelActivate(2, true);
                }
            }

            if (destroyedCentaurs != GameForSpears.completedGameCount)
            {
                int currentValue = destroyedCentaurs;
                destroyedCentaurs = GameForSpears.completedGameCount;

                if (currentValue < Easy.destroyedCentaursLimit)
                {
                    ChallengePanelSetText(3, 10, destroyedCentaurs, Easy.destroyedCentaursLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Easy.casualtiesLimit)
                {
                    ChallengePanelSetText(4, 3, casualties, Easy.casualtiesLimit);
                    ChallengePanelActivate(4, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Easy.savedUnitsLimit && placedBridges >= Easy.placedBridgesLimit
            && shotCount >= Easy.shotCountLimit && destroyedCentaurs >= Easy.destroyedCentaursLimit
            && casualties <= Easy.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Easy.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Easy.shotCountLimit);
                ChallengePanelSetText(3, 10, destroyedCentaurs, Easy.destroyedCentaursLimit);
                ChallengePanelSetText(4, 3, casualties, Easy.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Easy.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Easy.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Easy.shotCountLimit);
                ChallengePanelSetText(3, 10, destroyedCentaurs, Easy.destroyedCentaursLimit);
                ChallengePanelSetText(4, 3, casualties, Easy.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Easy.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Easy.shotCountLimit);
                ChallengePanelSetText(3, 10, destroyedCentaurs, Easy.destroyedCentaursLimit);
                ChallengePanelSetText(4, 3, casualties, Easy.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                ChallengePanelDeactivate(2);
                ChallengePanelDeactivate(3);
                ChallengePanelDeactivate(4);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 2 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Normal.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (placedBridges != GameForBridge.completedGameCount)
                {
                    int currentValue = placedBridges;
                    placedBridges = GameForBridge.completedGameCount;

                    if (currentValue < Normal.placedBridgesLimit)
                    {
                        ChallengePanelSetText(1, 9, placedBridges, Normal.placedBridgesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (shotCount != GameForArcher.completedGameCount)
            {
                int currentValue = shotCount;
                shotCount = GameForArcher.completedGameCount;

                if (currentValue < Normal.shotCountLimit)
                {
                    ChallengePanelSetText(2, 4, shotCount, Normal.shotCountLimit);
                    ChallengePanelActivate(2, true);
                }
            }

            if (blockCount != GameForShield.completedGameCount)
            {
                int currentValue = blockCount;
                blockCount = GameForShield.completedGameCount;

                if (currentValue < Normal.blockCountLimit)
                {
                    ChallengePanelSetText(3, 2, blockCount, Normal.blockCountLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Normal.casualtiesLimit)
                {
                    ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                    ChallengePanelActivate(4, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Normal.savedUnitsLimit && placedBridges >= Normal.placedBridgesLimit
            && shotCount >= Normal.shotCountLimit && blockCount >= Normal.blockCountLimit
            && casualties <= Normal.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Normal.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Normal.shotCountLimit);
                ChallengePanelSetText(3, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Normal.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Normal.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Normal.shotCountLimit);
                ChallengePanelSetText(3, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Normal.placedBridgesLimit);
                ChallengePanelSetText(2, 4, shotCount, Normal.shotCountLimit);
                ChallengePanelSetText(3, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(4, 3, casualties, Normal.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                ChallengePanelDeactivate(2);
                ChallengePanelDeactivate(3);
                ChallengePanelDeactivate(4);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 3 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Hard.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (placedBridges != GameForBridge.completedGameCount)
                {
                    int currentValue = placedBridges;
                    placedBridges = GameForBridge.completedGameCount;

                    if (currentValue < Hard.placedBridgesLimit)
                    {
                        ChallengePanelSetText(1, 9, placedBridges, Hard.placedBridgesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Hard.casualtiesLimit)
                {
                    ChallengePanelSetText(2, 3, casualties, Hard.casualtiesLimit);
                    ChallengePanelActivate(2, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Hard.savedUnitsLimit && placedBridges >= Hard.placedBridgesLimit
            && casualties <= Hard.casualtiesLimit && !Centaur.isFullDamageOccurred)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Hard.placedBridgesLimit);
                ChallengePanelSetText(2, 3, casualties, Hard.casualtiesLimit);
                ChallengePanelSetText(3, 11, 1);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if ((casualties > Hard.casualtiesLimit || Centaur.isFullDamageOccurred)
            && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Hard.placedBridgesLimit);
                ChallengePanelSetText(2, 3, casualties, Hard.casualtiesLimit, false);
                int complexChallengeValue = Centaur.isFullDamageOccurred ? 0 : 1;
                ChallengePanelSetText(3, 11, complexChallengeValue);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 9, placedBridges, Hard.placedBridgesLimit);
                ChallengePanelSetText(2, 3, casualties, Hard.casualtiesLimit);
                ChallengePanelSetText(3, 11, 1);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                ChallengePanelDeactivate(2);
                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }
    }

    private static class Easy
    {
        public static readonly int savedUnitsLimit = 20;
        public static readonly int placedBridgesLimit = 3;
        public static readonly int shotCountLimit = 2;
        public static readonly int destroyedCentaursLimit = 1;
        public static readonly int casualtiesLimit = 15;
    }

    private static class Normal
    {
        public static readonly int savedUnitsLimit = 30;
        public static readonly int placedBridgesLimit = 3;
        public static readonly int shotCountLimit = 2;
        public static readonly int blockCountLimit = 2;
        public static readonly int casualtiesLimit = 10;
    }

    private static class Hard
    {
        public static readonly int savedUnitsLimit = 40;
        public static readonly int placedBridgesLimit = 5;
        public static readonly int casualtiesLimit = 0;
    }
}