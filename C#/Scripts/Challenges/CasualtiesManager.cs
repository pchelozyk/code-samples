﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasualtiesManager : MonoBehaviour
{
    //единственный существующий экземпляр этого класса 
    private static CasualtiesManager instance;
    //общее количество потерь
    private static int totalCasualties;

    private void Awake()
    {
        //сброс информации о существующем экземпляре при запуске сцены
        instance = null;
    }

    private void Start()
    {
        //инициализация существующего экземпляра и удаление лишних экземпляров 
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //инициализация стартового значения общих потерь
        totalCasualties = 0;
    }

    public static void AddDeadWarrior(Transform warrior)
    {
        totalCasualties++;
        warrior.SetParent(instance.transform);
    }

    public static int GetCasualties()
    {
        return totalCasualties;
    }
}