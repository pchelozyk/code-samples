﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengePanel : MonoBehaviour
{
    private Animator animator;
    private Image background;
    private Text textField;

    private int enableStateHash;
    private int disableStateHash;
    private int refreshStateHash;

    private void Start()
    {
        animator = GetComponent<Animator>();
        background = GetComponent<Image>();
        textField = transform.GetChild(0).GetComponent<Text>();

        disableStateHash = Animator.StringToHash("Disable");
        enableStateHash = Animator.StringToHash("Enable");
        refreshStateHash = Animator.StringToHash("Refresh");

        gameObject.SetActive(false);
    }

    public void SetText(string value)
    {
        textField.text = value;
    }

    public string GetText()
    {
        return textField.text;
    }

    public void Show(bool hasExitTime, bool ignoreRefresh)
    {
        StopAllCoroutines();

        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
            animator.Play(enableStateHash, 0, 0);
            animator.Update(0);
        }
        else
        {
            int nextStateHash;
            if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == disableStateHash)
                nextStateHash = enableStateHash;
            else
                nextStateHash = refreshStateHash;

            if (ignoreRefresh && nextStateHash == refreshStateHash)
                return;

            animator.Play(nextStateHash, 0, 0);
            animator.Update(0);
        }

        if (hasExitTime)
            StartCoroutine(Deactivate());
    }

    public void Show(bool hasExitTime)
    {
        Show(hasExitTime, false);
    }

    public void Hide()
    {
        if (gameObject.activeInHierarchy)
            StartCoroutine(Deactivate());
    }

    private IEnumerator Deactivate()
    {
        //задержка на время проигрывания текущей активной анимации
        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        {
            if (!gameObject.activeInHierarchy)
                yield break;

            yield return null;
        }

        animator.Play(disableStateHash, 0, 0);
        animator.Update(0);

        //задержка на время проигрывания анимации исчезновения
        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        {
            if (!gameObject.activeInHierarchy)
                yield break;

            yield return null;
        }

        gameObject.SetActive(false);
    }
}
