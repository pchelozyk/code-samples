﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class challenges_lvl3 : Challenges
{
    //уровень сложност уровня
    //1 -> min уровень сложности
    //3 -> max уровень сложности
    private int difficulty;
    //время начала уровня 
    private float startTime;

    private int savedUnits;
    private int destroyedGates;
    private int placedLadders;
    private int torchCount;
    private int casualties;

    private bool allChallengesDisplayed;

    private void Awake()
    {
        //инициализация сложности для текущего уровня 
        difficulty = DifficultySettings.currentDifficulty;
    }

    private void Start()
    {
        //инициализация времени начала уровня
        startTime = Time.time;

        //инициализация стартовых значений перед началом уровня
        savedUnits = -1;
        destroyedGates = -1;
        placedLadders = -1;
        torchCount = 0;
        casualties = 0;

        //Очистка всех значений из других скриптов
        GameForShield.completedGameCount = 0;
        GameForArcher.completedGameCount = 0;
        GameForEngineer.completedGameCount = 0;
        GameForRam.completedGameCount = 0;
        GameForLadder.completedGameCount = 0;
        GameForTorch.lightFaded = false;

        //счетчик кол-ва итераций в линии для обращения к слоту
        int stepCounter = 0;
        //номер текущей линии
        int lineNum = 0;

        //инициализация стартового значения кол-ва юнитов определенного типа
        //в данном случае - отрядов с факелами
        for (int i = 0; i < 25; i++)
        {
            //после обработки 5 слотов - переход к следующей линии
            if (stepCounter > 4)
            {
                stepCounter = 0;
                lineNum++;
            }

            //сравнение индекса отряда в слоте с индексом отряда "Факел"
            if (ArmyPresetManager.armyPreset[i] == 6)
            {
                //увеличение счетчика
                torchCount++;
                //инициализация компонента Hp для текущего отряда итерации
                Hp squadHp = LineBrain.linesHolder[lineNum].squadHolder[stepCounter].GetComponentHp();
                //установка доп. события, которое должно произойти при уничтожении отряда
                squadHp.onSquadDeath.AddListener(DecreaseTorchCount);
            }

            //увеличение счетчика итераций
            stepCounter++;
        }

        allChallengesDisplayed = false;

        //создание панели вывода для каждого испытания в зависимости от уровня сложности
        if (difficulty == 1)
            ChallengePanelPrepare(4);
        else if (difficulty == 2)
            ChallengePanelPrepare(5);
        else if (difficulty == 3)
            ChallengePanelPrepare(5);

        continueGameButton.onGameContinue.AddListener(ShowFinalChallenges);
    }

    private void ShowFinalChallenges()
    {
        savedUnits = -1;
        destroyedGates = -1;
        placedLadders = -1;
    }

    //метод, который обновляет данные по кол-ву "живых" отрядов с факелами
    private void DecreaseTorchCount()
    {
        if (torchCount > 0)
            torchCount--;
    }

    //расчет и вывод данных о прогрессе выполнения заданий
    private void Update()
    {
        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 1 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Easy.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Easy.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Easy.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Easy.savedUnitsLimit && destroyedGates >= Easy.destroyedGatesLimit
            && placedLadders >= Easy.placedLaddersLimit && torchCount >= Easy.torchCountLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 7, torchCount, Easy.torchCountLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено испытание
            if (torchCount < Easy.torchCountLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 7, torchCount, Easy.torchCountLimit);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Easy.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Easy.placedLaddersLimit);
                ChallengePanelSetText(3, 7, torchCount, Easy.torchCountLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 2 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Normal.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Normal.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Normal.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Normal.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Normal.savedUnitsLimit && destroyedGates >= Normal.destroyedGatesLimit
            && placedLadders >= Normal.placedLaddersLimit && casualties <= Normal.casualtiesLimit
            && torchCount >= Normal.torchCountLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                ChallengePanelSetText(4, 7, torchCount, Normal.torchCountLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if ((casualties > Normal.casualtiesLimit || torchCount < Normal.torchCountLimit)
            && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit, false);
                ChallengePanelSetText(4, 7, torchCount, Normal.torchCountLimit);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Normal.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Normal.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                ChallengePanelSetText(4, 7, torchCount, Normal.torchCountLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);
                ChallengePanelDeactivate(4);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 3 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Hard.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedGates != GameForRam.completedGameCount)
                {
                    int currentValue = destroyedGates;
                    destroyedGates = GameForRam.completedGameCount;

                    if (currentValue < Hard.destroyedGatesLimit)
                    {
                        ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                        ChallengePanelActivate(1, false);
                    }
                }

                if (placedLadders != GameForLadder.completedGameCount)
                {
                    int currentValue = placedLadders;
                    placedLadders = GameForLadder.completedGameCount;

                    if (currentValue < Hard.placedLaddersLimit)
                    {
                        ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                        ChallengePanelActivate(2, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Hard.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Hard.savedUnitsLimit && destroyedGates >= Hard.destroyedGatesLimit
            && placedLadders >= Hard.placedLaddersLimit && casualties <= Hard.casualtiesLimit
            && torchCount >= Hard.torchCountLimit && !GameForTorch.lightFaded)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                ChallengePanelSetText(4, 8, 1);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if ((casualties > Hard.casualtiesLimit || torchCount < Hard.torchCountLimit
            || GameForTorch.lightFaded) && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit, false);
                int complexChallengeValue = torchCount >= Hard.torchCountLimit && !GameForTorch.lightFaded ? 1 : 0;
                ChallengePanelSetText(4, 8, complexChallengeValue);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 5, destroyedGates, Hard.destroyedGatesLimit);
                ChallengePanelSetText(2, 6, placedLadders, Hard.placedLaddersLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                ChallengePanelSetText(4, 8, 1);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                    ChallengePanelDeactivate(2);
                }

                ChallengePanelDeactivate(3);
                ChallengePanelDeactivate(4);

                allChallengesDisplayed = false;
            }
        }
    }

    private static class Easy
    {
        public static readonly int savedUnitsLimit = 20;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 2;
        public static readonly int torchCountLimit = 2;
    }

    private static class Normal
    {
        public static readonly int savedUnitsLimit = 30;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 3;
        public static readonly int casualtiesLimit = 15;
        public static readonly int torchCountLimit = 2;
    }

    private static class Hard
    {
        public static readonly int savedUnitsLimit = 40;
        public static readonly int destroyedGatesLimit = 1;
        public static readonly int placedLaddersLimit = 4;
        public static readonly int casualtiesLimit = 5;
        public static readonly int torchCountLimit = 4;
    }
}