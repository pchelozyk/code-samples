﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;

public class challenges_lvl1 : Challenges
{
    //уровень сложност уровня
    //1 -> min уровень сложности
    //3 -> max уровень сложности
    private int difficulty;
    //время начала уровня 
    private float startTime;

    private int savedUnits;
    private int destroyedWalls;
    private int blockCount;
    private int casualties;
    private int shotCount;

    private bool allChallengesDisplayed;

    private void Awake()
    {
        //инициализация сложности для текущего уровня 
        difficulty = DifficultySettings.currentDifficulty;
    }

    //TO DO отключение всплывающих подсказок при появлении панели поражения/победы
    private void Start()
    {
        //инициализация времени начала уровня
        startTime = Time.time;

        //инициализация стартовых значений перед началом уровня
        savedUnits = -1;
        destroyedWalls = -1;
        blockCount = 0;
        casualties = 0;
        shotCount = 0;

        //Очистка всех значений из других скриптов
        GameForShield.completedGameCount = 0;
        GameForArcher.completedGameCount = 0;
        GameForEngineer.completedGameCount = 0;

        allChallengesDisplayed = false;

        //создание панели вывода для каждого испытания в зависимости от уровня сложности
        if (difficulty == 1)
            ChallengePanelPrepare(2);
        else if (difficulty == 2)
            ChallengePanelPrepare(4);
        else if (difficulty == 3)
            ChallengePanelPrepare(4);

        continueGameButton.onGameContinue.AddListener(ShowFinalChallenges);
    }

    public void ShowFinalChallenges()
    {
        savedUnits = -1;
        destroyedWalls = -1;
    }

    //расчет и вывод данных о прогрессе выполнения заданий
    private void Update()
    {
        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 1 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Easy.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedWalls != GameForEngineer.completedGameCount)
                {
                    int currentValue = destroyedWalls;
                    destroyedWalls = GameForEngineer.completedGameCount;

                    if (currentValue < Easy.destroyedWallsLimit)
                    {
                        ChallengePanelSetText(1, 1, destroyedWalls, Easy.destroyedWallsLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Easy.savedUnitsLimit && destroyedWalls >= Easy.destroyedWallsLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Easy.destroyedWallsLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Easy.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Easy.destroyedWallsLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 2 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Normal.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedWalls != GameForEngineer.completedGameCount)
                {
                    int currentValue = destroyedWalls;
                    destroyedWalls = GameForEngineer.completedGameCount;

                    if (currentValue < Normal.destroyedWallsLimit)
                    {
                        ChallengePanelSetText(1, 1, destroyedWalls, Normal.destroyedWallsLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (blockCount != GameForShield.completedGameCount)
            {
                int currentValue = blockCount;
                blockCount = GameForShield.completedGameCount;

                if (currentValue < Normal.blockCountLimit)
                {
                    ChallengePanelSetText(2, 2, blockCount, Normal.blockCountLimit);
                    ChallengePanelActivate(2, true);
                }
            }

            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Normal.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Normal.savedUnitsLimit && destroyedWalls >= Normal.destroyedWallsLimit
            && blockCount >= Normal.blockCountLimit && casualties <= Normal.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Normal.destroyedWallsLimit);
                ChallengePanelSetText(2, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Normal.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Normal.destroyedWallsLimit);
                ChallengePanelSetText(2, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Normal.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Normal.destroyedWallsLimit);
                ChallengePanelSetText(2, 2, blockCount, Normal.blockCountLimit);
                ChallengePanelSetText(3, 3, casualties, Normal.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                ChallengePanelDeactivate(2);
                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }

        //выполнится только 1 раз после выполнения условий уровня
        if (difficulty == 3 && !progressUpdate.challengesComplete)
        {
            //обновление значений для финальных испытаний
            if (ZonesInfo.IsLastZoneReached())
            {
                if (savedUnits != ZoneExit.GetUnitCount())
                {
                    int currentValue = savedUnits;
                    savedUnits = ZoneExit.GetUnitCount();

                    if (currentValue < Hard.savedUnitsLimit)
                    {
                        ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                        ChallengePanelActivate(0, false);
                    }
                }

                if (destroyedWalls != GameForEngineer.completedGameCount)
                {
                    int currentValue = destroyedWalls;
                    destroyedWalls = GameForEngineer.completedGameCount;

                    if (currentValue < Hard.destroyedWallsLimit)
                    {
                        ChallengePanelSetText(1, 1, destroyedWalls, Hard.destroyedWallsLimit);
                        ChallengePanelActivate(1, false);
                    }
                }
            }

            //обновление значений для стандартных испытаний
            if (shotCount != GameForArcher.completedGameCount)
            {
                int currentValue = shotCount;
                shotCount = GameForArcher.completedGameCount;

                if (currentValue < Hard.shotCountLimit)
                {
                    ChallengePanelSetText(2, 4, shotCount, Hard.shotCountLimit);
                    ChallengePanelActivate(2, true);
                }
            }

            if (casualties != CasualtiesManager.GetCasualties())
            {
                int currentValue = casualties;
                casualties = CasualtiesManager.GetCasualties();

                if (currentValue < Hard.casualtiesLimit)
                {
                    ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                    ChallengePanelActivate(3, true);
                }
            }

            //условия прохождения уровня
            if (savedUnits >= Hard.savedUnitsLimit && destroyedWalls >= Hard.destroyedWallsLimit
            && shotCount >= Hard.shotCountLimit && casualties <= Hard.casualtiesLimit)
            {
                //заполнение победной панели актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Hard.destroyedWallsLimit);
                ChallengePanelSetText(2, 4, shotCount, Hard.shotCountLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                winPanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelComplete(startTime);
            }

            //Состояние когда провалено одно из испытаний
            if (casualties > Hard.casualtiesLimit && !transform.GetChild(0).gameObject.activeSelf)
            {
                //заполнение панели поражения актуальным текстом
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Hard.destroyedWallsLimit);
                ChallengePanelSetText(2, 4, shotCount, Hard.shotCountLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit, false);
                losePanelText.text = GetFullText();

                HideAllImmediate();
                OnLevelFailed(startTime);
            }

            //отображение всех испытаний при нажатии на кнопку
            if (challengesButton.InActiveState() && !allChallengesDisplayed)
            {
                //обновление текста во всех панелях
                ChallengePanelSetText(0, 0, savedUnits, Hard.savedUnitsLimit);
                ChallengePanelSetText(1, 1, destroyedWalls, Hard.destroyedWallsLimit);
                ChallengePanelSetText(2, 4, shotCount, Hard.shotCountLimit);
                ChallengePanelSetText(3, 3, casualties, Hard.casualtiesLimit);
                //отображение всех панелей
                ShowAll();

                allChallengesDisplayed = true;
            }

            //скрытие испытаний при повторном нажатии на кнопку
            if (!challengesButton.InActiveState() && allChallengesDisplayed)
            {
                if (!ZonesInfo.IsLastZoneReached())
                {
                    ChallengePanelDeactivate(0);
                    ChallengePanelDeactivate(1);
                }

                ChallengePanelDeactivate(2);
                ChallengePanelDeactivate(3);

                allChallengesDisplayed = false;
            }
        }
    }

    private static class Easy
    {
        public static readonly int savedUnitsLimit = 20;
        public static readonly int destroyedWallsLimit = 3;
    }

    private static class Normal
    {
        public static readonly int savedUnitsLimit = 30;
        public static readonly int destroyedWallsLimit = 4;
        public static readonly int blockCountLimit = 2;
        public static readonly int casualtiesLimit = 10;
    }

    private static class Hard
    {
        public static readonly int savedUnitsLimit = 30;
        public static readonly int destroyedWallsLimit = 4;
        public static readonly int shotCountLimit = 2;
        public static readonly int casualtiesLimit = 0;
    }
}