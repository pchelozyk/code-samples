﻿using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class PromptLevel : MonoBehaviour
{
    //активный экземпляр класса
    public static PromptLevel instance;

    //кнопка паузы
    public GameObject pauseButton;
    //кнопка отображения испытаний
    public GameObject challengesButton;

    public challenges_lvl1 challengesInstance;

    //CinemachineBrain для основной камеры
    public CinemachineBrain mainCamBrain;
    //доп виртуальная камера
    public CinemachineVirtualCamera vcam_exrta;
    //аниматор панели отображения подсказок
    private Animator promptPanelAnim;
    //текст панели отображения подсказок
    private Text promptPanelText;

    //состояние дополнительной камеры
    private bool vcam_exrtaEnabled;
    //состояния отображений сообщений
    private bool[] messageStatus = new bool[4];
    //триггер сброса перехода камеры между отображением подсказок
    private bool skipTransition;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        //инициализация панели отображения подсказок
        promptPanelAnim = transform.GetChild(0).GetComponent<Animator>();
        promptPanelText = transform.GetChild(0).GetChild(0).GetComponent<Text>();

        //иcходный статус отображения сообщений
        messageStatus = new bool[] { false, false, false, false };

        vcam_exrtaEnabled = false;
        skipTransition = false;

        //уничтожение панели подсказок, когда не стартовая сложность
        if (DifficultySettings.currentDifficulty != 1)
        {
            Destroy(transform.parent.gameObject);
        }
    }

    void Update()
    {
        if (vcam_exrtaEnabled)
        {
            //состояние, когда управление передано основной виртуальной камере
            //переход к новой камере завершен
            if ((vcam_exrta.Priority == 9) && (!mainCamBrain.IsBlending))
            {
                //полное включение D&D
                LineBrain.SetGlobalDragState(true);

                //отображение кнопки паузы
                pauseButton.SetActive(true);
                //отображение списка испытаний
                challengesButton.SetActive(true);

                //урезание плавности перехода при смене виртуальной камеры
                CinemachineBlendDefinition m_CustomBlend = new CinemachineBlendDefinition(CinemachineBlendDefinition.Style.Cut, 0);
                mainCamBrain.m_DefaultBlend = m_CustomBlend;

                //возврат исходного значения
                vcam_exrtaEnabled = false;

                //удаление всего объекта после отображения последней подсказки
                if (messageStatus[2]) Destroy(transform.parent.gameObject);
            }

            //скрытие подсказки и возврат в исходное состояние
            if ((promptPanelAnim.gameObject.activeSelf) && (Input.anyKeyDown))
            {
                //состояние, когда должен произойти моментальный переход ко второй подсказке после отображения первой
                //прерывание отменяет передачу управления основной камере
                if (skipTransition)
                {
                    skipTransition = false;
                    promptPanelAnim.SetTrigger("In");
                    promptPanelText.text = LocalizationManager.instance.GetLocalizedValue("hint_text_2");

                    return;
                }

                //отмена остановки времени
                TimeManager.TimeUnfreeze();
                //включение перемещения камеры
                CamDrag.UnlockCamera(1);

                //централизация камеры на объекте сообщения
                vcam_exrta.Follow = null;
                //обновление текущей активной камеры
                vcam_exrta.Priority = 9;

                //скрытие подсказки
                promptPanelAnim.gameObject.SetActive(false);
                //отображение финальных испытаний
                challengesInstance.ShowFinalChallenges();
            }
        }
    }

    public void showMessage(int messageNum, DragItem messageTarget)
    {
        //прерывание при повторных запросах на отображение подсказки
        if (messageStatus[messageNum]) return;
        else messageStatus[messageNum] = true;

        //скрытие кнопки паузы
        pauseButton.SetActive(false);
        //скрытие списка испытаний
        challengesButton.SetActive(false);
        //полное отключение D&D
        LineBrain.SetGlobalDragState(false);
        //остановка времени
        TimeManager.TimeFreeze();
        //отключение перемещения камеры
        CamDrag.LockCamera(1);
        //изменение состояния виртуальной камеры
        vcam_exrtaEnabled = true;

        //добавление плавности перехода при смене виртуальной камеры
        CinemachineBlendDefinition m_CustomBlend = new CinemachineBlendDefinition(CinemachineBlendDefinition.Style.Linear, 0.5f);
        mainCamBrain.m_DefaultBlend = m_CustomBlend;

        //центрирование камеры на объекте сообщения
        vcam_exrta.Follow = messageTarget.transform;
        //обновление текущей активной камеры
        vcam_exrta.Priority = 11;

        //инициализация ключа для отображаемой подсказки
        string m_key = $"hint_text_{messageNum + 1}";
        //обновление текста подсказки
        promptPanelText.text = LocalizationManager.instance.GetLocalizedValue(m_key);

        //отображение подсказки
        promptPanelAnim.gameObject.SetActive(true);
        promptPanelAnim.SetTrigger("In");

        if ((messageNum == 0) && (messageTarget.miniGame.GetType() == typeof(GameForShield)))
        {
            skipTransition = true;
            messageStatus[1] = true;
        }
    }

    public void showMessage(int messageNum, Transform messageTarget)
    {
        //прерывание при повторных запросах на отображение подсказки
        if (messageStatus[messageNum]) return;
        else messageStatus[messageNum] = true;

        //скрытие кнопки паузы
        pauseButton.SetActive(false);
        //скрытие списка испытаний
        challengesButton.SetActive(false);
        //полное отключение D&D
        LineBrain.SetGlobalDragState(false);
        //остановка времени
        TimeManager.TimeFreeze();
        //отключение перемещения камеры
        CamDrag.LockCamera(1);
        //изменение состояния виртуальной камеры
        vcam_exrtaEnabled = true;

        //добавление плавности перехода при смене виртуальной камеры
        CinemachineBlendDefinition m_CustomBlend = new CinemachineBlendDefinition(CinemachineBlendDefinition.Style.Linear, 0.5f);
        mainCamBrain.m_DefaultBlend = m_CustomBlend;

        //центрирование камеры на объекте сообщения
        vcam_exrta.Follow = messageTarget;
        //обновление текущей активной камеры
        vcam_exrta.Priority = 11;

        //инициализация ключа для отображаемой подсказки
        string m_key = $"hint_text_{messageNum + 1}";
        //обновление текста подсказки
        promptPanelText.text = LocalizationManager.instance.GetLocalizedValue(m_key);

        //отображение подсказки
        promptPanelAnim.gameObject.SetActive(true);
        promptPanelAnim.SetTrigger("In");
    }
}