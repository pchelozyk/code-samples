﻿using UnityEngine;

public class MoveCannon : MonoBehaviour
{
    [Header("Вращение против часовой стрелки")]
    public bool reverseRotation;
    private Move lineMove;
    private float speed;

    private void Start()
    {
        lineMove = transform.parent.parent.parent.parent.GetComponent<Move>();
    }

    private void FixedUpdate()
    {
        //скорость движения линии
        speed = lineMove.GetSpeed();

        //состояние, когда колеса должны вращаться
        if (speed != 0)
        {
            //определение напрвления вращения
            Vector3 m_dir = reverseRotation ? Vector3.right : Vector3.left;

            //величина, которая влияет на скорость вращения (привязана к скорости движения линии)
            //без нее колесо вращается на 1 градус за 1 секунду
            float rotationBoost = speed * 150.0f;

            //вращение колес
            transform.Rotate(m_dir * Time.deltaTime * rotationBoost);
        }
    }
}