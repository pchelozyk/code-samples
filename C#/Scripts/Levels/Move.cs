﻿using UnityEngine;
using System;
using System.Collections;

public class Move : MonoBehaviour
{
    public float speed;
    public Vector3 direction;
    public AudioClip defaultSound;
    public AudioClip combinedSound;
    private int[] stopMovementRequest = new int[4];
    private int mechanismCounter;
    private AudioSource audioSource;
    private LineBrain lineBrain;

    private void Start()
    {
        //исходное значение скорости
        speed = 0;

        //Запрос от щитовиков
        stopMovementRequest[0] = 0;
        //Запрос от лучников в атаке
        stopMovementRequest[1] = 0;
        //Запрос от копейщиков
        stopMovementRequest[2] = 0;
        //Запрос от баллисты
        stopMovementRequest[3] = 0;

        audioSource = GetComponent<AudioSource>();
        lineBrain = GetComponent<LineBrain>();

        //определение звуковой дорожки для линии
        mechanismCounter = 0;

        for (int i = 0; i < lineBrain.squadHolder.Length; i++)
        {
            if (lineBrain.squadHolder[i] != null)
            {
                Hp squadHp = lineBrain.squadHolder[i].GetComponentHp();
                Type miniGameType = lineBrain.squadHolder[i].miniGame.GetType();
                if (miniGameType == typeof(GameForLadder)
                || miniGameType == typeof(GameForRam)
                || miniGameType == typeof(GameForBridge))
                {
                    mechanismCounter++;
                    squadHp.onSquadDeath.AddListener(delegate { UpdateSound(true); });
                }
                else squadHp.onSquadDeath.AddListener(delegate { UpdateSound(false); });
            }
        }

        audioSource.clip = mechanismCounter > 0 ? combinedSound : defaultSound;
        audioSource.volume = 0.3f;

        //установка исходной скорости в зависимости от заполнения линии
        for (int i = 0; i < 5; i++)
        {
            if (lineBrain.squadHolder[i] != null)
            {
                SetSpeed(0.4F);
                break;
            }

            if (i == 4)
            {
                //остановка линии и отключение звука для неё
                SetSpeed(0);
            }
        }
    }

    private void Update()
    {
        if (speed != 0) transform.Translate(direction * speed * Time.deltaTime);
    }

    public void SetSpeed(float value)
    {
        if (value == speed) return;

        if (value == 0)
        {
            speed = value;
            SetAnimation(1);
            audioSource.Pause();
        }
        else if (!lineBrain.IsEmpty())
        {
            for (int i = 0; i < stopMovementRequest.Length; i++)
            {
                if (stopMovementRequest[i] > 0) return;
            }

            speed = value;
            SetAnimation(2);
            audioSource.Play();
        }
    }

    public void SetSpeed(float value, int eventIndex)
    {
        if (value == 0) stopMovementRequest[eventIndex]++;
        else if (stopMovementRequest[eventIndex] > 0) stopMovementRequest[eventIndex]--;

        SetSpeed(value);
    }

    public void ResetMovementLock(int eventIndex)
    {
        stopMovementRequest[eventIndex] = 0;
    }

    public float GetSpeed()
    {
        return speed;
    }

    private void SetAnimation(int animationType)
    {
        for (int i = 0; i < 5; i++)
        {
            DragItem squad = lineBrain.squadHolder[i];
            if (squad != null)
            {
                if (animationType == 1)
                {
                    //сброс стандартного триггера для всех отрядов
                    squad.ResetAnimationTrigger("Walk");
                    //сброс уникальных триггеров для конкретных отрядов
                    if (squad.miniGame.GetType() == typeof(GameForArcher)) squad.ResetAnimationTrigger("Shot");
                    //установка нового триггера
                    squad.SetAnimationTrigger("Idle");
                }

                if (animationType == 2)
                {
                    //сброс стандартного триггера для всех отрядов
                    squad.ResetAnimationTrigger("Idle");
                    //сброс уникальных триггеров для конкретных отрядов
                    if (squad.miniGame.GetType() == typeof(GameForArcher)) squad.ResetAnimationTrigger("Shot");
                    if (squad.miniGame.GetType() == typeof(GameForSpears)) squad.ResetAnimationTrigger("Bayonet");
                    //установка нового триггера
                    squad.SetAnimationTrigger("Walk");
                }
            }
        }
    }

    private void UpdateSound(bool needClipReset)
    {
        if (lineBrain.IsEmpty())
        {
            audioSource.Pause();
            return;
        }

        if (needClipReset)
        {
            mechanismCounter--;
            if (mechanismCounter == 0)
            {
                audioSource.clip = defaultSound;
                if (speed != 0) audioSource.Play();
            }
        }
    }
}