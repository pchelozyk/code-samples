﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLegacy : MonoBehaviour
{
    public float speed;
    public Vector3 direction;

    void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }
}
