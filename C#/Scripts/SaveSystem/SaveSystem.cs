﻿using UnityEngine;
using System;
using System.IO;

public class SaveSystem
{
    public static void SaveProgress(Save m_save)
    {
        string directoryPath = Path.Combine(Application.persistentDataPath, "Saves");
        string filePath = Path.Combine(directoryPath, "Save.json");

        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);

        File.WriteAllText(filePath, JsonUtility.ToJson(m_save));
    }

    public static Save LoadProgress()
    {
        Save m_save = new Save();

        string directoryPath = Path.Combine(Application.persistentDataPath, "Saves");
        string filePath = Path.Combine(directoryPath, "Save.json");

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
        else if (File.Exists(filePath))
        {
            m_save = JsonUtility.FromJson<Save>(File.ReadAllText(filePath));
        }

        return m_save;
    }
}

[Serializable]
public class Save
{
    //структура сейва
    //индекс в массиве это номер уровня
    //первый массив - доступность уровней
    public bool[] availableLevels = new bool[5];
    //второй массив - доступный уровень сложности
    //1 -> доступна только первая сложность
    //2 -> доступна только первая и вторая сложности
    //3 -> доступны все сложности для уровня 
    public int[] availableDifficulty = new int[5];
    //третий массив - просмотрена ли кат-сцена
    public bool[] watchedCutScene = new bool[3];

    //описание конструктора класса
    //сейв, созданный конструктором, дает доступ только к начальному испытанию первого уровня
    public Save()
    {
        for (int i = 0; i < availableLevels.Length; i++)
        {
            //инициализация стартовой доступности уровней
            availableLevels[i] = i == 0 ? true : false;
            //инициализация стартовой доступности сложностей для уровней
            availableDifficulty[i] = 1;
        }

        //инициализация состояния просмотра кат-сцены
        for (int i = 0; i < watchedCutScene.Length; i++)
        {
            watchedCutScene[i] = false;
        }
    }
}