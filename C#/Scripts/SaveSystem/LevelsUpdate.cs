﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelsUpdate : MonoBehaviour
{
    //слои карты для каждого уровня
    public List<MapLayers> Layers;

    private void Start()
    {
        Save m_save = new Save();
        m_save = SaveSystem.LoadProgress();

        GameObject buttonsMap = transform.GetChild(1).gameObject;
        GameObject levelName = transform.GetChild(2).gameObject;

        int i = 0;

        do
        {
            //включение кнопок взаимодействия с уровнем (когда сейв предоставляет доступ к этому уровню)
            buttonsMap.transform.GetChild(i).gameObject.SetActive(true);
            //включение подписи для кнопки уровня
            levelName.transform.GetChild(i).gameObject.SetActive(true);

            i++;
        } while ((i < m_save.availableLevels.Length) && (m_save.availableLevels[i] == true));

        //инициализация слоев карты
        Image mapLayer_1 = transform.parent.GetChild(0).GetComponent<Image>();
        Image mapLayer_2 = transform.parent.GetChild(1).GetComponent<Image>();

        //очистка слоя перед установкой нового состояния
        mapLayer_1.sprite = null;
        mapLayer_2.sprite = null;

        //инициализация номера последнего пройденного уровня
        int lvlNum = i - 1;

        //состояние, когда разблокированно больше одного уровня
        if (lvlNum > 0)
        {
            //сброс подсветки для предыдущего подсвеченного уровня
            levelName.transform.GetChild(lvlNum - 1).GetComponent<Image>().color = Color.white;
            levelName.transform.GetChild(lvlNum - 1).GetChild(0).gameObject.SetActive(false);
        }

        //установка спец. подсветки для последнего разблокированного уровня
        levelName.transform.GetChild(lvlNum).GetComponent<Image>().color = new Color(0.21f, 1, 1, 1);
        levelName.transform.GetChild(lvlNum).GetChild(0).gameObject.SetActive(true);

        //обновление слоев для карты
        mapLayer_1.sprite = Layers[lvlNum].MapLayer_1;
        mapLayer_2.sprite = Layers[lvlNum].MapLayer_2;
    }
}

[System.Serializable]
public class MapLayers
{
    [Header("Первый слой карты")]
    public Sprite MapLayer_1;

    [Header("Второй слой карты")]
    public Sprite MapLayer_2;
}
