﻿using UnityEngine;
using UnityEngine.EventSystems;
using Steamworks;

public class ProgressUpdate : MonoBehaviour, IPointerDownHandler
{
    public int levelIndex;

    public bool challengesComplete;

    public void OnPointerDown(PointerEventData eventData)
    {
        Save m_save = SaveSystem.LoadProgress();

        levelIndex -= 1;

        //инициализация текущего уровня сложности
        int curDifficulty = DifficultySettings.currentDifficulty;

        if (challengesComplete)
        {
            //предоставление доступа к следующей сложности текущего уровня
            if (curDifficulty != 3)
                m_save.availableDifficulty[levelIndex] = curDifficulty + 1;

            //предоставление доступа к следующему уровню
            if (levelIndex + 1 < m_save.availableLevels.Length)
                m_save.availableLevels[levelIndex + 1] = true;

            //обновление структуры сейва
            SaveSystem.SaveProgress(m_save);

            //запрос актуальных данных о пользователе
            SteamUserStats.RequestCurrentStats();
            //инициализация имени текущего достижения и его состояния
            string achievementName = $"{levelIndex + 1}_{curDifficulty - 1}";
            bool isAchieved;
            SteamUserStats.GetAchievement(achievementName, out isAchieved);
            //получение пользователем достижения
            if (!isAchieved)
            {
                SteamUserStats.SetAchievement(achievementName);
                SteamUserStats.StoreStats();
            }
        }
    }
}
