﻿using UnityEngine;

public class BallistaDamage : MonoBehaviour
{
    private LineBrain targetLine;

    private void Start()
    {
        //номер слота, в котором находится баллиста
        int lineNum = transform.parent.parent.GetSiblingIndex();
        //линия, с которой связана баллиста
        targetLine = LineBrain.linesHolder[lineNum];
    }

    private void OnTriggerEnter(Collider slot)
    {
        int slotNum = slot.transform.GetSiblingIndex();
        DragItem targetSquad = targetLine.squadHolder[slotNum];

        if (targetSquad != null)
        {
            Hp squadHp = targetSquad.GetComponentHp();
            squadHp.SetDamage(2);
        }
    }
}