﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Ballista : MonoBehaviour
{
    private LineBrain targetLine;
    private Move lineMove;
    private ZonesInfo zonesInfo;

    private ExtendedButton buttonRush;
    private ExtendedButton buttonStop;

    [Header("Префаб эффекта молний")]
    public GameObject lightningPrefab;
    private GameObject stunEffect;
    private Transform projectile;
    private BoxCollider projectileCollider;
    private TrailRenderer projectileTrail;
    private Transform damageArea;
    private Animator ballistaAnimator;
    private Animator[] warriorAnim = new Animator[2];
    private AudioSource shotSound;
    private AudioSource loudRunSound;

    [Header("Минимальная/максимальная задержка до выстрела")]
    public float[] targetTimeLimit = new float[2];
    [Header("Задержка до выстрела в дальней/средней зоне")]
    public float[] shotDelayTime = new float[2];
    [Header("Продолжительность ускорения/остановки линии")]
    public float[] actionTimeLimit = new float[2];
    private float stun;
    private bool isActiveRequest;

    private void Start()
    {
        //номер слота, в котором находится баллиста
        int lineNum = transform.parent.GetSiblingIndex();
        //линия, с которой связана баллиста
        targetLine = LineBrain.linesHolder[lineNum];
        lineMove = targetLine.GetComponent<Move>();
        zonesInfo = targetLine.GetComponent<ZonesInfo>();

        //создание событий для кнопок смены скорости движения
        buttonRush = targetLine.transform.GetChild(5).GetChild(0).GetComponent<ExtendedButton>();
        buttonRush.onClick.AddListener(OnRush);
        buttonStop = targetLine.transform.GetChild(5).GetChild(1).GetComponent<ExtendedButton>();
        buttonStop.onClick.AddListener(OnStop);

        projectile = transform.GetChild(0);
        damageArea = transform.GetChild(1);
        stunEffect = transform.GetChild(2).gameObject;

        projectileCollider = projectile.GetComponent<BoxCollider>();
        projectileTrail = projectile.GetChild(0).GetComponent<TrailRenderer>();
        ballistaAnimator = transform.GetComponent<Animator>();
        warriorAnim[0] = transform.GetChild(transform.childCount - 1).GetComponent<Animator>();
        warriorAnim[1] = transform.GetChild(transform.childCount - 2).GetComponent<Animator>();
        shotSound = transform.GetComponent<AudioSource>();
        loudRunSound = targetLine.transform.GetChild(5).GetComponent<AudioSource>();

        //инициализация стартового значения оглушения
        stun = 0;

        isActiveRequest = false;

        StartCoroutine(ShotPreparation(true));
    }

    private void OnRush()
    {
        //сброс блокировки движния от копейщиков
        lineMove.ResetMovementLock(2);

        //установка скорости для текущего события
        lineMove.SetSpeed(1.5f);

        //отключение кнопок смены скорости
        buttonRush.gameObject.SetActive(false);
        buttonStop.gameObject.SetActive(false);

        loudRunSound.Play();

        //возврат исходной скорости с задержкой
        //короутина запускается на другом объекте для выполнения в случае уничтожения баллисты
        lineMove.StartCoroutine(ResetSpeed(actionTimeLimit[0]));
    }

    private void OnStop()
    {
        //инициализация скорости для текущего события
        lineMove.SetSpeed(0, 3);

        //отключение кнопок смены скорости
        buttonRush.gameObject.SetActive(false);
        buttonStop.gameObject.SetActive(false);

        isActiveRequest = true;

        //возврат исходной скорости с задержкой
        //короутина запускается на другом объекте для выполнения в случае уничтожения баллисты
        //lineMove.StartCoroutine(ResetSpeed(actionTimeLimit[1]));
    }

    private IEnumerator ShotPreparation(bool generateTargetTime)
    {
        if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
        else
        {
            float targetTime = generateTargetTime ? Random.Range(targetTimeLimit[0], targetTimeLimit[1]) : 0;
            while (targetTime > 0 || stun > 0)
            {
                if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) break;

                if (stun > 0) stun -= Time.deltaTime;
                else targetTime -= Time.deltaTime;

                if (stun > 0 && !stunEffect.activeSelf)
                {
                    stunEffect.SetActive(true);

                    warriorAnim[0].SetTrigger("Stun");
                    warriorAnim[1].SetTrigger("Stun");
                }
                else if (stun <= 0 && stunEffect.activeSelf)
                {
                    stunEffect.SetActive(false);

                    warriorAnim[0].SetTrigger("Idle");
                    warriorAnim[1].SetTrigger("Idle");
                }

                yield return null;
            }

            if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
            else
            {
                //определене целевых координат выстрела
                Vector3 targetPos = targetLine.transform.GetChild(2).position;
                float offset = Random.Range(-7.0f, 4.0f);
                targetPos = new Vector3(targetPos.x, targetPos.y, targetPos.z - offset);

                //подключение видимости области поражения
                damageArea.position = targetPos;
                damageArea.gameObject.SetActive(true);

                //включение кнопок смены скорости для линии
                buttonRush.gameObject.SetActive(true);
                buttonStop.gameObject.SetActive(true);

                //выполнение задержки до выстрела (текущая зона линии определяется по последнему слоту)
                float delay = shotDelayTime[zonesInfo.GetSlotZone(0)];
                while (delay > 0)
                {
                    if (targetLine.IsEmpty() || !zonesInfo.IsReachable())
                    {
                        //отключение кнопок смены скорости
                        buttonRush.gameObject.SetActive(false);
                        buttonStop.gameObject.SetActive(false);
                        break;
                    }

                    if (stun > 0)
                    {
                        //сброс отсановки
                        if (isActiveRequest)
                        {
                            lineMove.SetSpeed(0.4f, 3);
                            isActiveRequest = false;
                        }

                        //отключение кнопок смены скорости
                        buttonRush.gameObject.SetActive(false);
                        buttonStop.gameObject.SetActive(false);
                        //отключение зоны поражения
                        damageArea.gameObject.SetActive(false);
                        //запуск повторного процесса поиска цели без генерации основной задержки
                        StartCoroutine(ShotPreparation(false));
                        yield break;
                    }

                    delay -= Time.deltaTime;

                    yield return null;
                }

                if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
                else StartCoroutine(Shoot());
            }
        }
    }

    private IEnumerator Shoot()
    {
        //проигрывание анимации выстрела
        ballistaAnimator.SetTrigger("Shot");

        //задержка на время переклюючения между анимациями
        int frameDelay = 6;
        while (frameDelay > 0)
        {
            frameDelay--;
            yield return null;
        }

        //инициализация начальных координат снаряда
        Vector3 startPos = projectile.position;
        Quaternion startRot = projectile.rotation;
        //проигрывание звука выстрела
        shotSound.Play();

        while (Vector3.Distance(projectile.position, damageArea.position) > 1)
        {
            //определение шага
            float step;
            if (Vector3.Distance(transform.position, projectile.position) > 1)
                step = 15 * Time.deltaTime;
            else step = 4 * Time.deltaTime;

            //полет снаряда
            projectile.position = Vector3.MoveTowards(projectile.position, damageArea.position, step);

            //вращение снаряда
            Vector3 relativePos = (damageArea.position - projectile.position) * -1;
            projectile.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

            if (stun > 0 && !stunEffect.activeSelf)
            {
                stunEffect.SetActive(true);

                warriorAnim[0].SetTrigger("Stun");
                warriorAnim[1].SetTrigger("Stun");
            }

            yield return null;
        }

        //возврат исходной скорости линии
        if (isActiveRequest)
        {
            lineMove.SetSpeed(0.4f, 3);
            isActiveRequest = false;
        }
        else lineMove.SetSpeed(0.4f);

        //отключение кнопок смены скорости
        buttonRush.gameObject.SetActive(false);
        buttonStop.gameObject.SetActive(false);

        //включение блокировки D&D
        targetLine.SetDragState(false, 1);
        //включение коллайдера, который считывает результаты попадания
        projectileCollider.enabled = true;
        //задержка в 1 кадр
        yield return new WaitForFixedUpdate();
        //отключение коллайдера, который считывает результаты попадания
        projectileCollider.enabled = false;
        //отключение блокировки D&D
        targetLine.SetDragState(true, 1);

        //отключение видимости области поражения
        damageArea.gameObject.SetActive(false);
        //создание эффекта
        Instantiate(lightningPrefab, damageArea.position, lightningPrefab.transform.rotation);

        //перемещение снаряда на исходную позицию
        projectile.gameObject.SetActive(false);
        projectile.position = startPos;
        projectile.rotation = startRot;
        projectileTrail.Clear();

        while (ballistaAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.length == 3)
        {
            yield return null;
        }

        projectile.gameObject.SetActive(true);

        //запуск процесса подготовки следующего выстрела
        StartCoroutine(ShotPreparation(true));
    }

    //возврат исходной скорости линии с задержкой 
    private IEnumerator ResetSpeed(float delay)
    {
        float startTime = Time.time;
        float startSpeed = lineMove.GetSpeed();
        bool speedChanged = false;

        //проверка на изменение скорости во время задержки
        while (Time.time - startTime < delay)
        {
            if (startSpeed != lineMove.GetSpeed())
            {
                speedChanged = true;
                loudRunSound.Stop();
                break;
            }

            yield return null;
        }

        //возврат исходной скорости
        if (!speedChanged)
        {
            lineMove.SetSpeed(0.4f);
            loudRunSound.Stop();
        }
    }

    private void DestroyWithEffects()
    {
        //уничтожение отряда лучников
        Destroy(transform.gameObject);

        //включение анимации телепорта
        transform.parent.GetChild(1).gameObject.SetActive(true);

        //удаление второстепенных отрядов когда нет цели для выстрела
        if (targetLine.IsEmpty())
        {
            foreach (Transform child in transform.parent)
            {
                if (!child.gameObject.activeSelf) Destroy(child.gameObject);
            }
        }
        //активация второстепенных отрядов
        else if (transform.parent.childCount >= 4)
        {
            transform.parent.GetChild(2).gameObject.SetActive(true);
        }
    }

    public void SetStun(float value)
    {
        stun = value;
    }
}