﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDefence : MonoBehaviour
{
    //единственный существующий экземпляр этого класса 
    public static LineDefence instance = null;
    public Transform[] slots = new Transform[5];

    private void Awake()
    {
        //инициализация существующего экземпляра и удаление лишних экземпляров 
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        for (int i = 0; i < 5; i++)
        {
            slots[i] = transform.GetChild(i);
        }
    }

    private void OnDestroy()
    {
        //сброс информации о существующем экземпляре при запуске сцены
        instance = null;
    }
}