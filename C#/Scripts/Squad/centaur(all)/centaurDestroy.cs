﻿using UnityEngine;

public class centaurDestroy : MonoBehaviour
{
    private void Start()
    {
        Centaur.isFullDamageOccurred = false;
    }

    private void OnTriggerEnter(Collider invader)
    {
        Destroy(invader.gameObject);
    }
}
