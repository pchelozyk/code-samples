﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnPrePlace : MonoBehaviour
{
    public GameObject centaurPrefab;
    public GameObject spawnEffect;
    //данные о каждом месте спавна
    public List<SpawnData> spawnData;

    private void Start()
    {
        if (spawnData.Count > 0 && spawnData.Count <= 5)
        {
            //исходный порядок линий
            int[] lineOrder = { 0, 1, 2, 3, 4 };

            for (int i = 0; i < spawnData.Count; i++)
            {
                //случайный сдвиг порядка для одного элемента
                int randInd = Random.Range(i, 5);
                if (i != randInd) (lineOrder[i], lineOrder[randInd]) = (lineOrder[randInd], lineOrder[i]);

                //создание нового экземпляра зоны респавна
                var spawnArea = new GameObject();
                spawnArea.name = $"SpawnArea{i + 1}";
                spawnArea.transform.SetParent(transform);

                //инициализация номера линии, к которой относится зона спавна
                int targetLineNum = lineOrder[i];

                //инициализаци координат-пределов для позиции спавна
                Vector3[] posLimit = { LineBrain.linesHolder[targetLineNum].transform.position, transform.position };
                //расчет расстояния смещения от стартовой точки
                float offset = Vector3.Distance(posLimit[0], posLimit[1]) * spawnData[i].posFactor;
                //обновление координат зоны спавна
                spawnArea.transform.position = new Vector3(posLimit[0].x, posLimit[0].y, posLimit[0].z + offset);

                //создание компонента centaurSpawn для экземпляра зоны спавна
                CentaurSpawn spawnComponent = spawnArea.AddComponent<CentaurSpawn>();
                //инициализация параметров для созданного компонента
                spawnComponent.centaurPrefab = centaurPrefab;
                spawnComponent.targetLineNum = targetLineNum;
                spawnComponent.spawnEffect = spawnEffect;
                spawnComponent.spawnLimit = spawnData[i].spawnLimit;
                spawnComponent.minTargetTime = spawnData[i].minTime;
                spawnComponent.maxTargetTime = spawnData[i].maxTime;
            }
        }
    }
}

[System.Serializable]
public class SpawnData
{
    [Header("Положение зоны спавна")]
    [Range(0.01f, 1.0f)]
    public float posFactor;

    [Header("Максимальное кол-во спавнов")]
    public int spawnLimit;

    [Header("Минимальное время до спавна")]
    public float minTime;

    [Header("Максимальное время до спавна")]
    public float maxTime;
}