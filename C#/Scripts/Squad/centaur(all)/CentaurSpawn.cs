﻿using UnityEngine;
using System.Collections;

public class CentaurSpawn : MonoBehaviour
{
    [Header("Префаб отряда конницы")]
    public GameObject centaurPrefab;
    [Header("Префаб эффекта при спавне")]
    public GameObject spawnEffect;
    [Header("Максимальное кол-во спавнов")]
    public int spawnLimit;
    [Header("Минимальное время до спавна")]
    public float minTargetTime;
    [Header("Максимальное время до спавна")]
    public float maxTargetTime;
    [Header("Номер целевой линии")]
    public int targetLineNum;

    private int spawnCount;

    private void Start()
    {
        spawnCount = 0;
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while (spawnCount < spawnLimit)
        {
            float effectDelay = 5.0f;
            float targetTime = Random.Range(minTargetTime, maxTargetTime) - effectDelay;

            yield return new WaitForSeconds(targetTime);
            Instantiate(spawnEffect, transform);
            yield return new WaitForSeconds(effectDelay);

            //создание отряда
            var squad = Instantiate(centaurPrefab, transform.position, centaurPrefab.transform.rotation);
            squad.name = "Centaur";
            squad.transform.SetParent(transform);
            spawnCount++;
            //инициализация номера целевой линии для отряда
            var centaur = squad.GetComponent<Centaur>();
            centaur.SetTargetLineNum(targetLineNum);
        }
    }
}