﻿using UnityEngine;
using System.Collections;

public class Centaur : MonoBehaviour
{
    public static bool isFullDamageOccurred;

    private int targetLineNum;
    private LineBrain targetLine;
    private CapsuleCollider _collider;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    private MoveLegacy _moveLegacy;
    private Animator[] unitAnimator = new Animator[2];

    private void Start()
    {
        _collider = transform.GetComponent<CapsuleCollider>();
        _rigidbody = transform.GetComponent<Rigidbody>();
        _audioSource = transform.GetComponent<AudioSource>();
        _moveLegacy = transform.GetComponent<MoveLegacy>();
        unitAnimator[0] = transform.GetChild(0).GetComponent<Animator>();
        unitAnimator[1] = transform.GetChild(1).GetComponent<Animator>();
    }

    public void SetTargetLineNum(int value)
    {
        targetLineNum = value;
        targetLine = LineBrain.linesHolder[targetLineNum];
    }

    private void OnTriggerExit(Collider slot)
    {
        int slotNum = slot.transform.GetSiblingIndex();
        if (slotNum == 0)
        {
            //включение D&D
            targetLine.SetDragState(true, 2);
            if (_moveLegacy.speed != 0)
            {
                isFullDamageOccurred = true;
                Destroy(this);
            }
        }
    }

    private void OnTriggerEnter(Collider slot)
    {
        //отключение D&D
        int slotNum = slot.transform.GetSiblingIndex();
        if (slotNum == 4) targetLine.SetDragState(false, 2);

        DragItem squad = targetLine.squadHolder[slotNum];
        if (squad != null)
        {
            Hp squadHp = squad.GetComponentHp();

            if (squad.miniGame.GetType() == typeof(GameForSpears))
            {
                GameForSpears squadMiniGame = (GameForSpears)squad.miniGame;
                if (squadMiniGame.GetDefenceModeState())
                {
                    //увеличение счетчика выполненных мини-игр для копейщиков
                    GameForSpears.completedGameCount += 1;

                    //удаление всех компонентов, которые не нужны после смерти отряда конницы
                    Destroy(_collider);
                    Destroy(_rigidbody);
                    Destroy(_audioSource);

                    //подключение анимации смерти для отряда конницы
                    unitAnimator[0].SetTrigger("Death");
                    unitAnimator[1].SetTrigger("Death");

                    //после смерти отряд конницы становится дочерним объетом уровня
                    transform.SetParent(transform.root.GetChild(0));

                    //отмена блокировки D&D 
                    targetLine.SetDragState(true, 2);

                    //запуск уничтожения с задержкой
                    StartCoroutine(WaitToDestroy());
                }
                else squadHp.SetDamage(2);
            }
            else squadHp.SetDamage(2);
        }
    }

    //удаление объекта с задержкой 
    private IEnumerator WaitToDestroy()
    {
        //остановка движения объекта
        //в ожидании проваливания под землю
        _moveLegacy.direction = Vector3.zero;
        _moveLegacy.speed = 0;

        //установка времени задержки
        yield return new WaitForSeconds(5.0f);

        //смена направления движения объекта для проваливания под землю
        _moveLegacy.direction = Vector3.down;
        //обновление скорости проваливания под землю
        _moveLegacy.speed = 0.1f;

        //установка времени задержки
        yield return new WaitForSeconds(5.0f);

        //окончательное удаление объекта
        Destroy(transform.gameObject);
    }
}