﻿using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    private Archer parentArcher;
    private CapsuleCollider capsuleCollider;
    private TrailRenderer trailRenderer;

    private void Start()
    {
        parentArcher = transform.parent.parent.GetComponent<Archer>();
        capsuleCollider = this.GetComponent<CapsuleCollider>();
        trailRenderer = this.GetComponent<TrailRenderer>();
    }

    private void OnTriggerEnter(Collider invader)
    {
        //состояние, когда стрела пронзает коллайдер земли
        if (invader.gameObject.layer == 9)
        {
            //стрела становится дочерним объектом коллайдера, в который попала
            transform.SetParent(invader.transform);
        }

        if (invader.gameObject.layer == 10)
        {
            //увеличение счетчика столкновений с препятствием
            parentArcher.AddEarlyCollision();
            //обновление родителя для стрелы (становится дочерним объектом отряда)
            transform.SetParent(invader.transform.parent);
            //уничтожение стрелы после 5 сек с момента столкновения
            Destroy(transform.gameObject, 5);
        }

        //уничтожение компонентов, которые больше не будут использоваться
        Destroy(capsuleCollider);
        Destroy(trailRenderer);
        Destroy(this);
    }
}