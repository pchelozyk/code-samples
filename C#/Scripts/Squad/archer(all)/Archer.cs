﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : MonoBehaviour
{
    [Header("Префаб стрелы")]
    public GameObject arrowPrefab;
    [Header("Префаб зоны поражения")]
    public GameObject damageAreaPrefab;
    private GameObject damageArea;

    [Range(0.01f, 1.0f)]
    public float heightFactor;
    [Range(0.01f, 1.0f)]
    public float positionFactor;
    public float speed = 12.0f;

    [Header("Минимальное время до выстрела")]
    public float minTargetTime;
    [Header("Максимальное время до выстрела")]
    public float maxTargetTime;
    [Header("Время активации в разных зонах")]
    public float[] shotDelayTime = new float[2];

    private float stun;
    private GameObject stunEffect;
    private LineBrain targetLine;
    private Move lineMove;
    private ZonesInfo zonesInfo;
    private AudioSource archerSound;
    private Animator[] warriorAnim = new Animator[4];
    private int earlyCollisionCount;

    private void Start()
    {
        stun = 0;
        int lineNum = transform.parent.GetSiblingIndex();
        targetLine = LineBrain.linesHolder[lineNum];
        lineMove = targetLine.GetComponent<Move>();
        zonesInfo = targetLine.GetComponent<ZonesInfo>();
        archerSound = transform.GetComponent<AudioSource>();
        stunEffect = transform.GetChild(0).gameObject;
        for (int i = 0; i < 4; i++)
        {
            warriorAnim[i] = transform.GetChild(i + 1).GetComponent<Animator>();
        }

        StartCoroutine(ShotPreparation(true));
    }

    private IEnumerator ShotPreparation(bool generateTargetTime)
    {
        if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
        else
        {
            float targetTime = generateTargetTime ? Random.Range(minTargetTime, maxTargetTime) : 0;
            while (targetTime > 0 || stun > 0)
            {
                if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) break;

                if (stun > 0) stun -= Time.deltaTime;
                else targetTime -= Time.deltaTime;

                if (stun > 0 && !stunEffect.activeSelf)
                {
                    stunEffect.SetActive(true);

                    for (int i = 0; i < warriorAnim.Length; i++)
                    {
                        warriorAnim[i].SetTrigger("Stun");
                    }
                }
                else if (stun <= 0 && stunEffect.activeSelf)
                {
                    stunEffect.SetActive(false);

                    for (int i = 0; i < warriorAnim.Length; i++)
                    {
                        warriorAnim[i].SetTrigger("Idle");
                    }
                }

                yield return null;
            }

            if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
            else
            {
                int targetSlotNum = FindTarget();
                if (targetSlotNum == -1) DestroyWithEffects();
                else
                {
                    SetDamageArea(targetSlotNum);
                    float delay = shotDelayTime[zonesInfo.GetSlotZone(targetSlotNum)];

                    while (delay > 0)
                    {
                        if (targetLine.IsEmpty() || !zonesInfo.IsReachable())
                        {
                            ResetShield(targetSlotNum);
                            break;
                        }

                        if (stun > 0)
                        {
                            damageArea.SetActive(false);
                            ResetShield(targetSlotNum);
                            StartCoroutine(ShotPreparation(false));
                            yield break;
                        }

                        delay -= Time.deltaTime;

                        ActivateShield(targetSlotNum);

                        yield return null;
                    }

                    if (targetLine.IsEmpty() || !zonesInfo.IsReachable()) DestroyWithEffects();
                    else StartCoroutine(Shoot(targetSlotNum));
                }
            }
        }
    }

    //использование функции предполагает что в линии есть хотя бы 1 отряд
    //при ненайденой цели для выстрела функция возвращает -1
    private int FindTarget()
    {
        //поиск приоритетных приделов
        int[] targetArea = zonesInfo.GetTargetArea();
        if (targetArea == null) return -1;

        //заполнение массивов приоритетных и второстепенных индексов        
        int targetAreaLenght = targetArea[1] - targetArea[0] + 1;
        int[] highPriorityIndexes = new int[targetAreaLenght];
        int[] lowPriorityIndexes = new int[5 - targetAreaLenght];
        int highPriorityAreaLength = 0;
        int lowPriorityAreaLength = 0;
        for (int i = 0; i < 5; i++)
        {
            if (i >= targetArea[0] && i <= targetArea[1])
            {
                highPriorityIndexes[highPriorityAreaLength] = i;
                highPriorityAreaLength++;
            }
            else
            {
                lowPriorityIndexes[lowPriorityAreaLength] = i;
                lowPriorityAreaLength++;
            }
        }

        //поиск цели в слотах с приоритетными индексами в случайном порядке
        for (int i = 0; i < highPriorityAreaLength; i++)
        {
            int targetIndex = Random.Range(i, highPriorityAreaLength);
            if (i != targetIndex)
            {
                (highPriorityIndexes[i], highPriorityIndexes[targetIndex]) =
                (highPriorityIndexes[targetIndex], highPriorityIndexes[i]);
            }

            if (targetLine.squadHolder[highPriorityIndexes[i]] != null)
            {
                return highPriorityIndexes[i];
            }
        }

        //поиск цели в оставщихся слотах в случайном порядке
        for (int i = 0; i < lowPriorityAreaLength; i++)
        {
            int targetIndex = Random.Range(i, lowPriorityAreaLength);
            if (i != targetIndex)
            {
                (lowPriorityIndexes[i], lowPriorityIndexes[targetIndex]) =
                (lowPriorityIndexes[targetIndex], lowPriorityIndexes[i]);
            }

            if (targetLine.squadHolder[lowPriorityIndexes[i]] != null && zonesInfo.GetSlotZone(lowPriorityIndexes[i]) != 2)
            {
                return lowPriorityIndexes[i];
            }
        }

        return -1;
    }

    public void AddEarlyCollision()
    {
        earlyCollisionCount++;
    }

    private IEnumerator Shoot(int targetSlotNum)
    {
        //проигрывание анимации выстрела
        for (int i = 0; i < warriorAnim.Length; i++)
        {
            warriorAnim[i].SetTrigger("Shot");
        }

        //задержка на время переклюючения между анимациями
        int frameDelay = 10;
        while (frameDelay > 0)
        {
            frameDelay--;
            yield return null;
        }

        earlyCollisionCount = 0;
        //инициализация цели выстрела
        Slot targetSlot = targetLine.slotHolder[targetSlotNum];
        //расчет расстояния от точки слежения до целевых координат
        float dist = Vector3.Distance(transform.position, targetSlot.transform.position);
        //расчет высоты и положения для точки следования
        float posY = transform.position.y + dist * heightFactor;
        float posZ = transform.position.z - dist * positionFactor;
        //инициализация позиций для точки следования
        Vector3 followPoint = new Vector3(transform.position.x, posY, posZ);

        //создание клона стрелы и присвоение ему стартовых значений
        GameObject arrow = Instantiate(arrowPrefab, transform, false);
        //инициализация звука попадания стрелы в цель
        AudioSource arrowSound = arrow.GetComponent<AudioSource>();

        //подключение звука
        archerSound.Play();

        bool isHitPrepared = false;
        //цикл продолжается пока объект не достигнет цели или все дочерние стрелы не попадут в прептствие
        while (Vector3.Distance(arrow.transform.position, followPoint) > 1 && arrow.transform.childCount > 0)
        {
            //инициализация координат целевого слота
            Vector3 targetPos = targetSlot.transform.position;

            //инициализация дочернего отряда слота
            DragItem childSquad = targetSlot.GetSquad();
            //корректировка целевых координат для Bridge
            if (childSquad != null && childSquad.miniGame.GetType() == typeof(GameForBridge))
            {
                targetPos = new Vector3(targetPos.x, targetPos.y + 0.5f, targetPos.z);
            }
            //корректировка целевых координат для всех остальных отрядов
            else targetPos = new Vector3(targetPos.x, targetPos.y - 2, targetPos.z - 1.5f);

            //инициализаця шага для прямолинейного движения
            float step = speed * Time.deltaTime;
            //движение точки следования
            if (Vector3.Distance(followPoint, targetPos) > 1)
            {
                followPoint = Vector3.MoveTowards(followPoint, targetPos, step);
            }
            //перемещение стрелы
            arrow.transform.position = Vector3.MoveTowards(arrow.transform.position, followPoint, step);
            //вращение стрелы
            Vector3 relativePos = (followPoint - arrow.transform.position) * (-1);
            arrow.transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

            if (Vector3.Distance(arrow.transform.position, targetPos) < 5.5F && !isHitPrepared)
            {
                //запуск звука попадания стрелы в цель
                arrowSound.Play();
                //включение блокировки D&D 
                targetLine.SetDragState(false, 0);
                isHitPrepared = true;
            }

            //включение кнопки для щитовиков
            ActivateShield(targetSlotNum);

            if (stun > 0 && !stunEffect.activeSelf)
            {
                stunEffect.SetActive(true);

                for (int i = 0; i < warriorAnim.Length; i++)
                {
                    warriorAnim[i].SetTrigger("Stun");
                }
            }

            //задержака в 1 кадр после каждой итерации цикла
            yield return null;
        }

        //  |
        //  v
        //все дочерние стрелы остановленны триггером земли
        //или cтрела достигла точки следования

        //инициализация целевого отряда
        DragItem targetSquad = targetSlot.GetSquad();
        //урон не наносится при пустом слоте или недолёте всех стрел до цели 
        if (targetSquad != null && earlyCollisionCount < 4)
        {
            //проверка неуязвимости целевого отряда
            //доп. действия для отдельных отрядов
            bool isImmortal = false;
            if (targetSquad.miniGame.GetType() == typeof(GameForShield))
            {
                GameForShield miniGame = (GameForShield)targetSquad.miniGame;
                isImmortal = miniGame.GetDefenceModeState();
                miniGame.ResetDefenceMode();
            }
            else if (targetSquad.miniGame.GetType() == typeof(GameForBridge)) isImmortal = true;

            //нанесение урона отрядам без неуязвимости к урону от стрел
            if (!isImmortal)
            {
                Hp targetHp = targetSquad.GetComponentHp();
                targetHp.SetDamage(2);

                //запрос на отображение подсказки
                if ((ArmyPresetManager.levelIndex == 1) && (DifficultySettings.currentDifficulty == 1))
                {
                    PromptLevel.instance.showMessage(3, targetSquad);
                }
            }
        }
        //сброс мини-игры щитовиков при недолете всех стрел
        else if (targetSquad != null && targetSquad.miniGame.GetType() == typeof(GameForShield) && earlyCollisionCount == 4)
        {
            GameForShield miniGame = (GameForShield)targetSquad.miniGame;
            miniGame.ResetDefenceMode();
        }

        //отмена блокировки D&D
        targetLine.SetDragState(true, 0);
        //возврат исходной скорости линии
        lineMove.SetSpeed(0.4f);
        //отключение отображения зоны поражения
        damageArea.SetActive(false);
        //уничтожение созданного экземпляра стрелы
        Destroy(arrow);
        //запуск процесса подготовки следующего выстрела
        StartCoroutine(ShotPreparation(true));
    }

    private void DestroyWithEffects()
    {
        //уничтожение зоны поражения
        if (damageArea != null) Destroy(damageArea);

        //уничтожение отряда лучников
        Destroy(transform.gameObject);

        //включение анимации телепорта
        transform.parent.GetChild(1).gameObject.SetActive(true);

        //удаление второстепенных отрядов когда нет цели для выстрела
        if (targetLine.IsEmpty())
        {
            foreach (Transform child in transform.parent)
            {
                if (!child.gameObject.activeSelf) Destroy(child.gameObject);
            }
        }
        //активация второстепенных отрядов
        else if (transform.parent.childCount >= 4)
        {
            transform.parent.GetChild(2).gameObject.SetActive(true);
        }
    }

    public void SetStun(int value)
    {
        stun = value;
    }

    private void ActivateShield(int slotIndex)
    {
        DragItem targetSquad = targetLine.squadHolder[slotIndex];

        if (targetSquad != null && targetSquad.miniGame.GetType() == typeof(GameForShield))
        {
            GameForShield miniGame = (GameForShield)targetSquad.miniGame;
            if (!miniGame.GetDefenceModeState() && !miniGame.ButtonActiveSelf())
            {
                miniGame.ButtonSetActive(true);
                //запрос на отображение подсказки
                if ((ArmyPresetManager.levelIndex == 1) && (DifficultySettings.currentDifficulty == 1))
                {
                    PromptLevel.instance.showMessage(1, targetSquad);
                }
            }
        }
    }

    private void ResetShield(int slotIndex)
    {
        if (targetLine.squadHolder[slotIndex] != null
        && targetLine.squadHolder[slotIndex].miniGame.GetType() == typeof(GameForShield))
        {
            GameForShield miniGame = targetLine.squadHolder[slotIndex].miniGame as GameForShield;
            miniGame.ResetDefenceMode();
        }
    }

    private void SetDamageArea(int slotIndex)
    {
        //инициализация целевого слота
        Slot targetSlot = targetLine.slotHolder[slotIndex];

        //определение координат появления зоны поражения
        Vector3 slotPos = targetSlot.transform.position;
        Vector3 targetPos = new Vector3(slotPos.x, slotPos.y + 0.9f, slotPos.z);

        //отображение зоны поражения
        if (damageArea == null)
        {
            damageArea = Instantiate(damageAreaPrefab, targetPos, damageAreaPrefab.transform.rotation, targetSlot.transform.parent);
        }
        else
        {
            damageArea.transform.position = targetPos;
            damageArea.SetActive(true);
        }

        //запрос на отображение подсказки
        if ((ArmyPresetManager.levelIndex == 1) && (DifficultySettings.currentDifficulty == 1))
        {
            DragItem targetSquad = targetSlot.GetSquad();
            if (targetSquad != null) PromptLevel.instance.showMessage(0, targetSquad);
        }
    }
}