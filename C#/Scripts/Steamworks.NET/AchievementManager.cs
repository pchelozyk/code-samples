﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class AchievementManager : MonoBehaviour
{
    private static AchievementManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        ResetAllAchievements();
    }

    public static void ResetAllAchievements()
    {
        //запрос актуальных данных о пользователе
        SteamUserStats.RequestCurrentStats();

        //перебор всех достижений
        for (int i = 1; i <= 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                //инициализация имени текущего достижения и его состояния
                string achievementName = $"{i}_{j}";
                bool isAchieved;
                SteamUserStats.GetAchievement(achievementName, out isAchieved);

                //откат достижения
                if (isAchieved)
                    SteamUserStats.ClearAchievement(achievementName);
            }
        }

        //сохранение изменений
        SteamUserStats.StoreStats();
    }
}