﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBrain : MonoBehaviour
{
    public static LineBrain[] linesHolder = new LineBrain[5];
    private static bool globalDragState;

    private DragItem itemBeingDragged;
    private Slot selectedSlot;
    private bool localDragState;

    [HideInInspector]
    public DragItem[] squadHolder = new DragItem[5];
    [HideInInspector]
    public Slot[] slotHolder = new Slot[5];
    //запросы на блокировку D&D в линии
    private int[] dragLockRequest = new int[4];

    private float offset_X;
    private float offset_Y;
    private float offset_Z;

    private void Awake()
    {
        int lineNum = transform.GetSiblingIndex() - 1;
        linesHolder[lineNum] = this;

        itemBeingDragged = null;
        selectedSlot = null;
        globalDragState = true;
        localDragState = true;

        //запрос от лучников в защите
        dragLockRequest[0] = 0;
        //запрос от баллисты
        dragLockRequest[1] = 0;
        //запрос от конницы
        dragLockRequest[2] = 0;
        //запрос от зоны подсчета воинов
        dragLockRequest[3] = 0;

        for (int i = 0; i < 5; i++)
        {
            slotHolder[i] = transform.GetChild(i).GetComponent<Slot>();
        }
    }

    public DragItem GetDraggedItem()
    {
        return itemBeingDragged;
    }

    public Slot GetSelectedSlot()
    {
        return selectedSlot;
    }

    public void HandleItemGrab(DragItem interactionItem, int slotIndex)
    {
        if (itemBeingDragged != null) return;

        itemBeingDragged = interactionItem;
        interactionItem.SetActivePosition();

        Vector3 dist = Camera.main.WorldToScreenPoint(interactionItem.transform.position);
        offset_X = Input.mousePosition.x - dist.x;
        offset_Y = Input.mousePosition.y - dist.y;
        offset_Z = Input.mousePosition.z - dist.z;

        for (int i = 0; i < 5; i++)
        {
            slotHolder[i].SetMeshActive(true);
            if (squadHolder[i] != null) squadHolder[i].StaminaVisualSetActive(true);
        }
        slotHolder[slotIndex].SetColor(1);
        CamDrag.LockCamera(0);
    }

    public void HandleItemMove(DragItem interactionItem)
    {
        if (itemBeingDragged != interactionItem) return;

        Vector3 itemPos = interactionItem.transform.position;
        Vector3 correctedPos = new Vector3(Input.mousePosition.x - offset_X,
        Input.mousePosition.y - offset_Y,
        Input.mousePosition.z - offset_Z);

        correctedPos = Camera.main.ScreenToWorldPoint(correctedPos);
        correctedPos = new Vector3(itemPos.x, itemPos.y, correctedPos.z);

        interactionItem.transform.position = correctedPos;
    }

    public void HandleItemDrop()
    {
        if (itemBeingDragged == null) return;

        if (selectedSlot != null)
        {
            int parentSlotIndex = itemBeingDragged.transform.parent.GetSiblingIndex();
            int targetSlotIndex = selectedSlot.transform.GetSiblingIndex();
            int stepCount = Mathf.Abs(parentSlotIndex - targetSlotIndex);

            if (stepCount <= itemBeingDragged.stamina)
            {
                itemBeingDragged.stamina = itemBeingDragged.stamina - stepCount;

                int startIndex = parentSlotIndex > targetSlotIndex ? parentSlotIndex - 1 : parentSlotIndex + 1;
                int stepType = parentSlotIndex > targetSlotIndex ? 1 : -1;

                while (stepCount != 0)
                {
                    slotHolder[parentSlotIndex].SetSquad(slotHolder[startIndex].GetSquad());

                    parentSlotIndex -= stepType;
                    startIndex -= stepType;

                    stepCount--;
                }

                slotHolder[targetSlotIndex].SetSquad(itemBeingDragged);
                itemBeingDragged.StartCoroutine(itemBeingDragged.AddStamina());
            }
        }

        DragReset();
    }

    public void HandleSlotCollision(Collider invader, Slot slot)
    {
        if ((itemBeingDragged == null) || (invader.gameObject != itemBeingDragged.gameObject)) return;

        if (slot.gameObject != invader.transform.parent.gameObject)
        {
            if (selectedSlot != null) selectedSlot.SetColor(0);
            selectedSlot = slot;

            int parentSlotIndex = invader.transform.parent.GetSiblingIndex();
            int targetSlotIndex = slot.transform.GetSiblingIndex();
            int staminaRequired = Mathf.Abs(parentSlotIndex - targetSlotIndex);

            if (staminaRequired > itemBeingDragged.stamina)
            {
                itemBeingDragged.StaminaVisualChange("0", Slot.colorError);
                slot.SetColor(2);
            }
            else
            {
                string m_text = (itemBeingDragged.stamina - staminaRequired).ToString();
                itemBeingDragged.StaminaVisualChange(m_text, Slot.colorActive);
                slot.SetColor(3);
            }
        }
        else
        {
            itemBeingDragged.StaminaVisualChange(itemBeingDragged.stamina.ToString(), Slot.colorStart);
            if (selectedSlot != null) selectedSlot.SetColor(0);
            selectedSlot = null;
        }
    }

    private void DragReset()
    {
        if (itemBeingDragged == null) return;

        itemBeingDragged.SetDefaultPosition();
        itemBeingDragged = null;
        selectedSlot = null;

        for (int i = 0; i < 5; i++)
        {
            slotHolder[i].SetMeshActive(false);
            slotHolder[i].SetColor(0);
            if (squadHolder[i] != null) squadHolder[i].StaminaVisualSetActive(false);
        }
        CamDrag.UnlockCamera(0);
    }

    public void SetDragState(bool state)
    {
        //прерывание смены состояния 
        if (state == localDragState || (!globalDragState && !localDragState)) return;
        if (state)
        {
            for (int i = 0; i < dragLockRequest.Length; i++)
            {
                if (dragLockRequest[i] > 0) return;
            }
        }

        localDragState = state;
        DragReset();

        for (int i = 0; i < 5; i++)
        {
            if (squadHolder[i] != null) squadHolder[i].ColliderSetActive(state);
        }
    }

    public void SetDragState(bool state, int eventIndex)
    {
        if (!state) dragLockRequest[eventIndex]++;
        else if (dragLockRequest[eventIndex] > 0) dragLockRequest[eventIndex]--;

        SetDragState(state);
    }

    public static void SetGlobalDragState(bool state)
    {
        if (state == globalDragState) return;

        globalDragState = state;
        for (int i = 0; i < linesHolder.Length; i++)
        {
            linesHolder[i].SetDragState(state);
        }
    }

    public bool IsEmpty()
    {
        for (int i = 0; i < squadHolder.Length; i++)
        {
            if (squadHolder[i] != null) return false;
        }
        
        return true;
    }

    public int GetLineNum()
    {
        int lineNum = transform.GetSiblingIndex() - 1;
        return lineNum;
    }
}