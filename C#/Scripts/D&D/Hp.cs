﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hp : MonoBehaviour
{
    private int hp;
    private LineBrain lineBrain;
    private DragItem dragItem;

    private UnityEvent _onSquadDeath = new UnityEvent();
    public UnityEvent onSquadDeath
    {
        get { return _onSquadDeath; }
        set { _onSquadDeath = value; }
    }

    private void Start()
    {
        hp = 4;
        lineBrain = transform.parent.parent.GetComponent<LineBrain>();
        dragItem = transform.GetComponent<DragItem>();
    }

    public int GetHp()
    {
        return hp;
    }

    public void SetDamage(int damage)
    {
        if (damage > hp) damage = hp;

        while (damage != 0)
        {
            SetDeathTrigger(hp);

            hp--;
            damage--;
        }

        if (hp == 0)
        {
            dragItem.Reset();
            Destroy(dragItem.gameObject);
            int slotIndex = transform.parent.GetSiblingIndex();
            lineBrain.slotHolder[slotIndex].SetSquad(null);
            onSquadDeath.Invoke();
        }
    }

    private void SetDeathTrigger(int childNum)
    {
        childNum -= 1;

        //инициализация связаных с отрядом аниматоров
        Animator warriorAnim = dragItem.GetWarriorAnimator(childNum);

        //ресет стандартных триггеров
        //такие триггера есть у каждого отряда
        warriorAnim.ResetTrigger("Walk");
        warriorAnim.ResetTrigger("Idle");

        //ресет уникальных триггеров
        //такие триггера есть только у определенных отрядов
        if (dragItem.miniGame.GetType() == typeof(GameForArcher)) warriorAnim.ResetTrigger("Shot");
        if (dragItem.miniGame.GetType() == typeof(GameForSpears)) warriorAnim.ResetTrigger("Bayonet");

        //установка триггера на смерть
        warriorAnim.SetTrigger("Death");
        //присвоение мертвому объекту нового родителя
        CasualtiesManager.AddDeadWarrior(warriorAnim.transform);
        //разрыв связи между отрядом и мертвым объектом
        dragItem.RemoveWarriorAnimator(childNum);
    }
}