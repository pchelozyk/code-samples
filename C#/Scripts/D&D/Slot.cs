﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    public static Color colorDefault = new Color(0.012f, 0.66f, 0.96f, 0.5f);
    public static Color colorError = new Color(0.96f, 0.26f, 0.21f, 0.5f);
    public static Color colorActive = new Color(0.08f, 0.88f, 0.18f, 0.5f);
    public static Color colorStart = new Color(0.55f, 0.84f, 0.95f, 0.5f);

    private MeshRenderer slotMesh;
    private LineBrain lineBrain;
    private DragItem attachedSquad = null;

    private int slotIndex;
    private BoxCollider slotCollider;

    private void Awake()
    {
        if (lineBrain == null) lineBrain = transform.parent.GetComponent<LineBrain>();
        slotMesh = this.GetComponent<MeshRenderer>();
        slotCollider = this.GetComponent<BoxCollider>();
        slotIndex = transform.GetSiblingIndex();
    }

    public void SetSquad(DragItem squad)
    {
        if (squad != null)
        {
            attachedSquad = squad;
            squad.transform.SetParent(transform, false);
            squad.transform.position = transform.position;
        }

        if (lineBrain == null)
        {
            lineBrain = transform.parent.GetComponent<LineBrain>();
            slotIndex = transform.GetSiblingIndex();
        }

        lineBrain.squadHolder[slotIndex] = squad;
    }

    public DragItem GetSquad()
    {
        if (transform.childCount != 1) return null;

        return attachedSquad;
    }

    public BoxCollider GetSlotCollider()
    {
        return slotCollider;
    }

    public void SetMeshActive(bool value)
    {
        slotMesh.enabled = value;
    }

    public void SetColor(int colorType)
    {
        switch (colorType)
        {
            case 0:
                slotMesh.material.color = colorDefault;
                break;
            case 1:
                slotMesh.material.color = colorStart;
                break;
            case 2:
                slotMesh.material.color = colorError;
                break;
            case 3:
                slotMesh.material.color = colorActive;
                break;
            default:
                slotMesh.material.color = colorDefault;
                break;
        }
    }

    private void OnTriggerEnter(Collider invader)
    {
        lineBrain.HandleSlotCollision(invader, this);
    }
}