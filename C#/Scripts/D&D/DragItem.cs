﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DragItem : MonoBehaviour
{
    public Component miniGame;
    public int stamina;

    private int staminaLimit;
    private float recoveryTime;
    private Hp hp;
    private Animator[] warriorsAnim = new Animator[4];
    private LineBrain lineBrain;
    private CapsuleCollider attachedCollider;
    private Text staminaText;
    private Animator staminaAnimator;
    private GameObject addStaminaAnim;

    private void Awake()
    {
        lineBrain = transform.parent.parent.GetComponent<LineBrain>();
        hp = this.GetComponent<Hp>();
        attachedCollider = this.GetComponent<CapsuleCollider>();
        staminaText = transform.GetChild(0).GetChild(1).GetComponent<Text>();
        staminaAnimator = transform.GetChild(0).GetChild(1).GetComponent<Animator>();
        addStaminaAnim = transform.GetChild(0).GetChild(2).gameObject;

        for (int i = 0; i < warriorsAnim.Length; i++)
        {
            warriorsAnim[i] = transform.GetChild(i + 1).GetComponent<Animator>();
        }

        staminaLimit = stamina;
        recoveryTime = 25;

        //прямая установка связи между слотом и отрядом
        Slot parentSlot = transform.parent.GetComponent<Slot>();
        parentSlot.SetSquad(this);
    }

    public void Reset()
    {
        int parentSlotIndex = transform.parent.GetSiblingIndex();
        lineBrain.squadHolder[parentSlotIndex] = null;
    }

    public void SetActivePosition()
    {
        Vector3 currentPos = transform.position;
        transform.position = new Vector3(currentPos.x, currentPos.y + 0.5f, currentPos.z);
    }

    public void SetDefaultPosition()
    {
        transform.position = transform.parent.position;
    }

    public int GetStaminaLimit()
    {
        return staminaLimit;
    }

    public Hp GetComponentHp()
    {
        return hp;
    }

    public CapsuleCollider GetAttachedColider()
    {
        return attachedCollider;
    }

    public Animator GetWarriorAnimator(int warriorIndex)
    {
        return warriorsAnim[warriorIndex];
    }

    public void RemoveWarriorAnimator(int warriorIndex)
    {
        warriorsAnim[warriorIndex] = null;
    }

    public void SetAnimationTrigger(string trigger)
    {
        for (int i = 0; i < warriorsAnim.Length; i++)
        {
            if (warriorsAnim[i] != null) warriorsAnim[i].SetTrigger(trigger);
            else break;
        }
    }

    public void ResetAnimationTrigger(string trigger)
    {
        for (int i = 0; i < warriorsAnim.Length; i++)
        {
            if (warriorsAnim[i] != null) warriorsAnim[i].ResetTrigger(trigger);
            else break;
        }
    }

    private void OnMouseDown()
    {
        int parentSlotIndex = transform.parent.GetSiblingIndex();
        lineBrain.HandleItemGrab(this, parentSlotIndex);
    }

    private void OnMouseDrag()
    {
        lineBrain.HandleItemMove(this);
    }

    private void OnMouseUp()
    {
        lineBrain.HandleItemDrop();
    }

    public void ColliderSetActive(bool value)
    {
        attachedCollider.enabled = value;
    }

    public void StaminaVisualSetActive(bool value)
    {
        staminaText.gameObject.SetActive(value);

        if (value)
        {
            int fontSize = lineBrain.GetDraggedItem() == this ? 100 : 75;
            Color textColor = lineBrain.GetDraggedItem() == this ? Slot.colorStart : Slot.colorDefault;
            textColor.a = 1;

            staminaText.text = stamina.ToString();
            staminaText.fontSize = fontSize;
            staminaText.color = textColor;
            staminaAnimator.SetTrigger("In");
        }
        else
        {
            staminaText.fontSize = 75;
            Color textColor = Slot.colorDefault;
            textColor.a = 1;
            staminaText.color = textColor;
        }
    }

    public void StaminaVisualChange(string value, Color textColor)
    {
        staminaText.text = value;
        textColor.a = 1;
        staminaText.color = textColor;
    }

    public IEnumerator AddStamina()
    {
        //установка задержки времени перед выполнением
        yield return new WaitForSeconds(recoveryTime);

        //остановка всех параллельно работающих короутин, если для одного объекта выполняется более 1-й
        //т.к. работать должна толька одна для одного объекта
        StopAllCoroutines();

        //состояние, когда лимит стамины не достигнут
        if (stamina < staminaLimit)
        {
            //увеличение значения стамины
            stamina++;
            //включение анимации пополнения стамины
            addStaminaAnim.SetActive(true);

            DragItem draggedItem = lineBrain.GetDraggedItem();
            //состояние, когда есть перемещаемый объект
            if (draggedItem != null)
            {
                //обработка событий для перемещаемого объекта
                Slot selectedSlot = lineBrain.GetSelectedSlot();
                if ((draggedItem == this) && (selectedSlot != null))
                {
                    lineBrain.HandleSlotCollision(attachedCollider, selectedSlot);
                }
                //обработка событий для всех остальных объектов
                else staminaText.text = stamina.ToString();
            }
        }

        //состояние, когда лимит стамины не достигнут после пополнения
        if (stamina < staminaLimit) StartCoroutine(AddStamina());
    }
}