﻿using UnityEngine;

public class DisableObject : MonoBehaviour {

	void Disable() {
		transform.gameObject.SetActive(false);
	}
}