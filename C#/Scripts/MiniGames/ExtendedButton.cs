﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("UI/ExtendedButton")]
public class ExtendedButton : Button
{
    private UnityEvent _onPointerDown = new UnityEvent();
    private UnityEvent _onPointerUp = new UnityEvent();
    private UnityEvent _onEnable = new UnityEvent();

    public UnityEvent onPointerDown
    {
        get { return _onPointerDown; }
        set { _onPointerDown = value; }
    }

    public UnityEvent onPointerUp
    {
        get { return _onPointerUp; }
        set { _onPointerUp = value; }
    }

    public UnityEvent onEnable
    {
        get { return _onEnable; }
        set { _onEnable = value; }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!TimeManager.IsPaused())
        {
            base.OnPointerDown(eventData);
            _onPointerDown.Invoke();
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (!TimeManager.IsPaused())
        {
            base.OnPointerUp(eventData);
            _onPointerUp.Invoke();
        }
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!TimeManager.IsPaused())
            base.OnPointerClick(eventData);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        onEnable.Invoke();
    }
}
