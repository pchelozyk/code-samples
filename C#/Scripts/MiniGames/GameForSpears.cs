﻿using UnityEngine.UI;
using UnityEngine;

public class GameForSpears : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;

    [Header("Иконка режима бездействия")]
    public Sprite idleModeSprite;

    [Header("Иконка оборонительного режима")]
    public Sprite defenceModeSprite;

    //триггер указывает нажата ли кнопка 
    private bool isPressed;

    //триггер определяет состояние отряда (находится ли он в оборонительном режиме)
    private bool defenceMode;

    //время, после которого с кнопкой можно снова взаимодействовать
    private float resetTime;
    //время, в течении которого с кнопкой не возможно взаимодействовать
    //после смены режима отряда
    private float freezingTime = 1.0f;

    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;
    //спрайт для кнопки
    private Image interactionButtonImage;

    private Move lineMove;
    private ZonesInfo zonesInfo;
    private DragItem dragItem;
    private AudioSource audioSource;


    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;

        //инициализация кнопки над отрядом и события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onPointerDown.AddListener(OnPointerDown);
        interactionButton.onPointerUp.AddListener(OnPointerUp);

        //инициализация спрайта кнопки
        interactionButtonImage = interactionButton.GetComponent<Image>();

        lineMove = transform.parent.parent.GetComponent<Move>();
        zonesInfo = transform.parent.parent.GetComponent<ZonesInfo>();
        dragItem = this.GetComponent<DragItem>();
        audioSource = this.GetComponent<AudioSource>();

        Hp squadHp = GetComponent<Hp>();
        squadHp.onSquadDeath.AddListener(OnDeath);

        //инициализация исходных значений
        isPressed = false;
        defenceMode = false;
        resetTime = 0;
    }

    public bool GetDefenceModeState()
    {
        return defenceMode;
    }

    //событие по нажатию на кнопку
    private void OnPointerDown()
    {
        //триггер указывает, что кнопка нажата
        isPressed = true;
        //"заморозка" камеры
        CamDrag.LockCamera(4);
    }

    //событие по отжатию кнопки
    private void OnPointerUp()
    {
        //триггер указывает, что кнопка не нажата
        isPressed = false;
        //отмена запроса на "заморозку" камеры
        CamDrag.UnlockCamera(4);
    }

    private void OnDeath()
    {
        if (isPressed)
        {
            isPressed = false;
            CamDrag.UnlockCamera(4);
        }

        if (defenceMode)
            lineMove.SetSpeed(0.4f, 2);
    }

    //событие на удержание кнопки
    private void OnButtonPress()
    {
        //Нажатие кнопки при активном режиме защиты 
        if (defenceMode && (Time.time > resetTime))
        {
            if (interactionButtonImage.sprite == idleModeSprite)
            {
                //увеличение закрашивания кнопки
                if (interactionButtonImage.fillAmount < 1) interactionButtonImage.fillAmount += 0.05f;

                //состояние, когда кнопка закрашена полностью
                if (interactionButtonImage.fillAmount >= 1)
                {
                    //триггер указывает, что отряд в обычном режиме
                    defenceMode = false;

                    //обновление времени, после которого появится возможность взаимодействовать с кнопкой
                    resetTime = Time.time + freezingTime;

                    //переход к анимации безбействия для случаев, когда линия остаётся неподвижной
                    dragItem.SetAnimationTrigger("Idle");
                    //запуск движения линии
                    lineMove.SetSpeed(0.4f, 2);

                    audioSource.Play();
                }
            }
            else if (interactionButtonImage.sprite == defenceModeSprite)
            {
                //откат закрашивания кнопки
                interactionButtonImage.fillAmount = 0;
                //обновление спрайта кнопки
                interactionButtonImage.sprite = idleModeSprite;
            }
        }
        //Нажатие кнопки при не включенном режиме защиты 
        else if (!defenceMode && (Time.time > resetTime))
        {
            if (interactionButtonImage.sprite == defenceModeSprite)
            {
                //увеличение закрашивания кнопки
                if (interactionButtonImage.fillAmount < 1) interactionButtonImage.fillAmount += 0.05f;

                //состояние, когда кнопка полностью закрашена
                if (interactionButtonImage.fillAmount >= 1)
                {
                    //триггер указывает, что отряд в оборонительном режиме
                    defenceMode = true;

                    //обновление времени, после которого появится возможность взаимодействовать с кнопкой
                    resetTime = Time.time + freezingTime;
                    //остановка линии
                    lineMove.SetSpeed(0, 2);

                    //подключение анимации
                    dragItem.SetAnimationTrigger("Bayonet");

                    audioSource.Play();
                }
            }
            else if (interactionButtonImage.sprite == idleModeSprite)
            {
                //откат закрашивания кнопки
                interactionButtonImage.fillAmount = 0;
                //обновление спрайта кнопки
                interactionButtonImage.sprite = defenceModeSprite;
            }
        }
    }

    //событие при отсутствии удержания кнопки
    private void OnButtonInactive()
    {
        //Отмена режима обороны если линия начала движение
        if (defenceMode && (lineMove.GetSpeed() != 0))
        {
            //триггер указывает, что отряд в обычном режиме
            defenceMode = false;

            //обновление времени, после которого можно взаимодействовать с кнопкой
            resetTime = Time.time + freezingTime;
            //обновление спрайта кнопки
            interactionButtonImage.sprite = idleModeSprite;
            //установка полного закрашивания кнопки
            interactionButtonImage.fillAmount = 1;
        }

        //Состояние, когда кнопка в промежуточном переходе
        if (interactionButtonImage.fillAmount < 1)
        {
            //уменьшение закрашивания кнопки
            interactionButtonImage.fillAmount -= 0.05f;
        }

        //состояние, когда у кнопки полностью исчезло закрашивание
        if (interactionButtonImage.fillAmount <= 0)
        {
            //состояние, когда у кнопки спрайт обычного режима
            if (interactionButtonImage.sprite == idleModeSprite)
            {
                //обновление спрайта для кнопки
                interactionButtonImage.sprite = defenceModeSprite;
                //полное закрашивание кнопки
                interactionButtonImage.fillAmount = 1;
            }
            //состояние, когда у кнопки спрайт оборонительного режима
            else if (interactionButtonImage.sprite == defenceModeSprite)
            {
                //обновление спрайта для кнопки
                interactionButtonImage.sprite = idleModeSprite;
                //полное закрашивание кнопки
                interactionButtonImage.fillAmount = 1;
            }
        }
    }

    private void FixedUpdate()
    {
        //отключение кнопки над отрядом, когда родительская линия пересекает границу ближней зоны
        if (!zonesInfo.IsReachable()) interactionButton.gameObject.SetActive(false);

        if (isPressed) OnButtonPress();
        else OnButtonInactive();
    }
}