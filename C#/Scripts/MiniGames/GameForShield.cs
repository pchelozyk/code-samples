﻿using UnityEngine;

public class GameForShield : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;
    //триггер текущего состояния отряда 
    private bool defMode;
    //слот, в котором была активирована мини игра
    private GameObject activationSlot;
    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;
    private Move lineMove;
    private AudioSource attachedAudio;

    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;

        //инициализация кнопки над отрядом и события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onClick.AddListener(TaskOnClick);
        interactionButton.onEnable.AddListener(OnButtonEnable);

        lineMove = transform.parent.parent.GetComponent<Move>();
        attachedAudio = GetComponent<AudioSource>();

        Hp squadHp = GetComponent<Hp>();
        squadHp.onSquadDeath.AddListener(OnDeath);
    }

    //метод выполняется при активации кнопки
    private void OnButtonEnable()
    {
        activationSlot = transform.parent.gameObject;
    }

    private void TaskOnClick()
    {
        //изменение состояния отряда
        defMode = true;
        //остановка линии
        lineMove.SetSpeed(0, 0);
        //подключение звука
        attachedAudio.Play();
        //отключение отображения кнопки
        interactionButton.gameObject.SetActive(false);
        completedGameCount++;
    }

    //метод для получения состояния режима защиты из внешних скриптов
    public bool GetDefenceModeState()
    {
        return defMode;
    }

    //метод, который вызывается при попадании стрелы в отряд
    public void ResetDefenceMode()
    {
        if (defMode)
        {
            defMode = false;
            lineMove.SetSpeed(0.4f, 0);
        }
        else interactionButton.gameObject.SetActive(false);
    }

    private void Update()
    {
        //отключение кнопки над отрядом, когда он был перемещен в другой слот без выполнения мини-игры
        GameObject currentSlot = transform.parent.gameObject;
        if (interactionButton.gameObject.activeSelf && (activationSlot != currentSlot))
        {
            interactionButton.gameObject.SetActive(false);
        }

        //возврат отряда в исходное состояние, когда он был перемещен в другой слот после выполнения мини-игры
        if (defMode && (activationSlot != currentSlot))
        {
            defMode = false;
            lineMove.SetSpeed(0.4f, 0);
        }
    }

    //метод для изменения состояние кнопи из внешних скриптов
    public void ButtonSetActive(bool value)
    {
        interactionButton.gameObject.SetActive(value);
    }

    //метод для получения состояния кнопки из внешних скриптов
    public bool ButtonActiveSelf()
    {
        return interactionButton.gameObject.activeSelf;
    }

    private void OnDeath()
    {
        if (defMode)
        {
            defMode = false;
            lineMove.SetSpeed(0.4f, 0);
        }
    }
}