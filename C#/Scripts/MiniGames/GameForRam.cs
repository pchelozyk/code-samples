﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameForRam : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;
    private int interactionCount;
    //используемые ID аниматора
    private int default_state_hash;
    private int getReady_state_hash;
    private int getReady_parameter_hash;
    private int hit_state_hash;
    private int hit_parameter_hash;

    private List<Animator> ramsChain = new List<Animator>();

    private LineBrain lineBrain;
    private Animator ramAnimator;
    private Animator[] gateAnimator;
    private AudioSource[] gateAudioSource;
    private AudioSource ramAudioSource;

    private Transform slider;
    private ExtendedButton interactionButton;
    private Image interactionButtonImage;

    private void Start()
    {
        completedGameCount = 0;
        interactionCount = 0;
        //инициализация используемых ID аниматора
        default_state_hash = Animator.StringToHash("Base Layer.Default");
        getReady_state_hash = Animator.StringToHash("Base Layer.GetReady");
        getReady_parameter_hash = Animator.StringToHash("GetReady_motionTime");
        hit_state_hash = Animator.StringToHash("Base Layer.Hit");
        hit_parameter_hash = Animator.StringToHash("Hit_motionTime");

        lineBrain = transform.parent.parent.GetComponent<LineBrain>();
        ramAnimator = transform.GetChild(transform.childCount - 1).GetChild(0).GetComponent<Animator>();
        ramAudioSource = GetComponent<AudioSource>();

        slider = transform.GetChild(0).GetChild(0);
        interactionButton = slider.GetChild(1).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onClick.AddListener(OnButtonInteraction);
        interactionButtonImage = interactionButton.GetComponent<Image>();
    }

    private IEnumerator MiniGameIteration()
    {
        //инициализация состояния аниматора
        bool isReady = ramAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash == getReady_state_hash ? true : false;

        //запуск анимаций с переходом и без перехода в зависимости от текущего активного состояния
        if (!isReady)
        {
            if (ramAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash == hit_state_hash)
                SetAnimStateSynchronously(getReady_state_hash, 0.25f);
            else
                SetAnimStateSynchronously(getReady_state_hash, 0);
        }

        int startInteractionCount = interactionCount;
        float animTime = 0;
        while (animTime < 1)
        {
            //расчет текущего периода для слайдера и анимации
            animTime += Time.deltaTime;

            //применение текущего периода для слайдера
            SetSliderValue(animTime);

            //применение текущего периода для анимации
            if (!isReady)
                SetAnimParameterSynchronously(getReady_parameter_hash, animTime);

            //момент отображения кнопки
            if (animTime > 0.75f && !interactionButton.interactable && startInteractionCount == interactionCount)
                interactionButton.interactable = true;

            yield return null;
        }

        //отключение кнопки по завершению итерации мини-игры
        interactionButton.interactable = false;

        if (startInteractionCount == interactionCount)
        {
            //возвращение слайдера к исходному положению
            animTime = 1;
            while (animTime > 0)
            {
                animTime -= Time.deltaTime;
                SetSliderValue(animTime);

                yield return null;
            }

            //запуск новой итерации мини-игры
            StartCoroutine(MiniGameIteration());
        }
        else
        {
            //моментальный запуск анимации удара
            SetAnimStateSynchronously(hit_state_hash, 0);

            animTime = 0;
            while (animTime < 1)
            {
                //расчет текущего периода для слайдера и анимации
                animTime += 2.5f * Time.deltaTime;

                //применение текущего периода для слайдера
                SetSliderValue(1 - animTime);

                //применение текущего периода для анимации
                SetAnimParameterSynchronously(hit_parameter_hash, animTime);

                yield return null;
            }

            //проигрывание звука удара
            ramAudioSource.Play();

            if (interactionCount >= 3)
                OnGameComplete();
            else
                StartCoroutine(MiniGameIteration());
        }
    }

    public void Activate()
    {
        slider.gameObject.SetActive(true);
        RefreshRamsChain();
        StartCoroutine(MiniGameIteration());
    }

    public void Deactivate()
    {
        slider.gameObject.SetActive(false);
        interactionButton.interactable = false;
        interactionCount = 0;
        SetSliderValue(0);
        StopAllCoroutines();

        //сброс анимации до исходной
        SetAnimStateSynchronously(default_state_hash, 0);

        //TO DO сброс всех изменений кнопки (если такие изменения происходят в процессе мини-игры)
    }

    private void OnGameComplete()
    {
        //увеличение счетчика выполненных мини-игр
        completedGameCount += 1;

        //отключение слайдера
        slider.gameObject.SetActive(false);

        //запуск подсчета выживших отрядов 
        ZoneExit.CountWarriors(lineBrain);

        //воспроизведение анимации и звука для ворот
        for (int i = 0; i < gateAnimator.Length; i++)
        {
            gateAnimator[i].enabled = true;
            gateAudioSource[i].Play();
        }
    }

    private void SetSliderValue(float value)
    {
        value = Mathf.Clamp01(value);

        RectTransform handleRect = interactionButton.transform as RectTransform;

        Vector2 targetAnchorMax = handleRect.anchorMax;
        targetAnchorMax.x = value;

        Vector2 targetAnchorMin = handleRect.anchorMin;
        targetAnchorMin.x = value;

        handleRect.anchorMax = targetAnchorMax;
        handleRect.anchorMin = targetAnchorMin;
    }

    private void OnButtonInteraction()
    {
        interactionCount++;
        interactionButton.interactable = false;
    }

    private void RefreshRamsChain()
    {
        ramsChain.Clear();

        for (int i = lineBrain.squadHolder.Length - 1; i >= 0; i--)
        {
            if (lineBrain.squadHolder[i] != null && lineBrain.squadHolder[i].miniGame is GameForRam)
            {
                GameForRam ramMiniGame = (GameForRam)lineBrain.squadHolder[i].miniGame;
                ramsChain.Add(ramMiniGame.GetAnimator());
            }
            else break;
        }
    }

    private Animator GetAnimator()
    {
        return ramAnimator;
    }

    //метод для инициализации значения attachedGate из сторонних скриптов
    public void SetAttachedGate(Animator[] gateAnimator, AudioSource[] gateAudioSource)
    {
        this.gateAnimator = new Animator[gateAnimator.Length];
        this.gateAnimator = gateAnimator;

        this.gateAudioSource = new AudioSource[gateAudioSource.Length];
        this.gateAudioSource = gateAudioSource;
    }

    private void SetAnimStateSynchronously(int stateHash, float transitionDuration)
    {
        for (int i = 0; i < ramsChain.Count; i++)
        {
            ramsChain[i].CrossFade(stateHash, transitionDuration, 0);
            ramsChain[i].Update(0);
        }
    }

    private void SetAnimParameterSynchronously(int parameterID, float value)
    {
        for (int i = 0; i < ramsChain.Count; i++)
        {
            ramsChain[i].SetFloat(parameterID, value);
        }
    }
}