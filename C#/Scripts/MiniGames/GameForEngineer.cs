﻿using UnityEngine;
using UnityEngine.UI;

public class GameForEngineer : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;
    //стена, которую должен уничтожить отряд инженеров
    //инициализируется в скрипте "ZonePaling"
    private ZoneDestruction attachedZone;
    //триггер указывает началось ли выполнение мини-игры
    private bool gameActive;
    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;
    private Image interactionButtonImage;
    private Move lineMove;
    private AudioSource attachedAudio;

    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;
        //переключение триггера в исходное состояние
        gameActive = false;

        //инициализация кнопки над отрядом и события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onClick.AddListener(TaskOnClick);
        //установка исходного цвета для кнопки
        interactionButtonImage = interactionButton.GetComponent<Image>();
        interactionButtonImage.color = new Color(1, 1, 1, 1);

        lineMove = transform.parent.parent.GetComponent<Move>();
        attachedAudio = this.GetComponent<AudioSource>();
    }

    //событие по нажатию на кнопку
    private void TaskOnClick()
    {
        //включение звукового эффекта
        attachedAudio.Play();
        //триггер указывает, что мини-игра началась
        gameActive = true;

        //состояние, когда условие мини-игры выполнено
        //т.е. кнопка имеет определенный цвет
        if (interactionButtonImage.color.g <= 0)
        {
            //возврат триггера в исходное состояние
            gameActive = false;
            //отключение зоны, с которой взаимодействует отряд
            attachedZone.DisableZone();
            //отключение кнопки над отрядом
            interactionButton.gameObject.SetActive(false);
            //возврат исходной скорости линии
            lineMove.SetSpeed(0.4f);
            //увеличение счетчика выполненных мини-игр для инженеров
            completedGameCount += 1;
        }
        //обновление цвета кнопки (покраснение)
        else interactionButtonImage.color = new Color(1, (interactionButtonImage.color.g - 0.2f), (interactionButtonImage.color.b - 0.2f), 1);
    }

    //метод для изменения состояние кнопи из внешних скриптов
    public void ButtonSetActive(bool value)
    {
        interactionButton.gameObject.SetActive(value);
    }

    //метод для инициализации связаной с отрядом стеной
    public void SetAttachedWall(ZoneDestruction attachedZone)
    {
        this.attachedZone = attachedZone;
    }

    private void FixedUpdate()
    {
        //состояние, когда мини-игра началась, но ее условие не выполнено
        if (gameActive && (interactionButtonImage.color.g < 1))
        {
            //обновление цвета кнопки (обесцвечивание)
            interactionButtonImage.color = new Color(1, (interactionButtonImage.color.g + 0.005f), (interactionButtonImage.color.b + 0.005f), 1);
        }
    }
}