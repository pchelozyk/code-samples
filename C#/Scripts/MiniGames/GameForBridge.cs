﻿using System.Collections;
using UnityEngine;

public class GameForBridge : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;

    private GameObject attachedZone;

    //триггер состояния кнопки
    private bool isPressed;
    private bool isComplete;
    private bool isTransparent;

    //аниматор, параметры которого будут меняться
    //с помощью этого аниматора реализован наклон моста
    private Animator bridgeAnimator;
    private Renderer transparentPart;
    private Color[] defColors;

    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;

    private LineBrain lineBrain;
    private Move lineMove;
    private DragItem dragItem;
    private Rigidbody squadRigidbody;
    private AudioSource audioSource;
    private MoveCannon[] wheelMove = new MoveCannon[4];

    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;

        //инициализация кнопки над отрядом и события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onPointerDown.AddListener(OnPointerDown);
        interactionButton.onPointerUp.AddListener(OnPointerUp);

        isPressed = false;
        isComplete = false;
        isTransparent = false;

        lineBrain = transform.parent.parent.GetComponent<LineBrain>();
        lineMove = transform.parent.parent.GetComponent<Move>();
        dragItem = this.GetComponent<DragItem>();
        squadRigidbody = this.GetComponent<Rigidbody>();
        audioSource = transform.GetComponent<AudioSource>();

        Transform bridgeParts = transform.GetChild(transform.childCount - 1);
        for (int i = 0; i < wheelMove.Length; i++)
        {
            wheelMove[i] = bridgeParts.GetChild(i).GetComponent<MoveCannon>();
        }

        //инициализация и "заморозка" аниматора для моста
        bridgeAnimator = transform.GetChild(5).GetComponent<Animator>();
        bridgeAnimator.speed = 0;

        transparentPart = transform.GetChild(5).GetChild(5).GetComponent<Renderer>();
        defColors = new Color[transparentPart.materials.Length];
        for (int i = 0; i < transparentPart.materials.Length; i++)
        {
            defColors[i] = transparentPart.materials[i].color;
        }
    }

    //метод для изменения состояние кнопи из внешних скриптов
    public void ButtonSetActive(bool value)
    {
        interactionButton.gameObject.SetActive(value);
    }

    //метод для установки связаной зоны из внешних скриптов
    public void SetAttachedZone(GameObject value)
    {
        attachedZone = value;
    }

    //событие по нажатию на кнопку 
    private void OnPointerDown()
    {
        //изменение триггера состояния кнопки
        isPressed = true;
        //"заморозка" камеры
        CamDrag.LockCamera(5);

        //подключение анимации толчка моста для двух первых воинов отряда
        for (int i = 0; i < 2; i++)
        {
            Animator warriorAnimator = dragItem.GetWarriorAnimator(i);
            if (warriorAnimator != null)
            {
                warriorAnimator.ResetTrigger("Idle");
                warriorAnimator.SetTrigger("Push");
            }
        }

        audioSource.Play();
    }

    //событие по отжатию кнопки
    private void OnPointerUp()
    {
        //изменение триггера состояния кнопки
        isPressed = false;
        //разморозка камеры
        CamDrag.UnlockCamera(5);

        //подключение исходной анимации для двух первых воинов отряда
        for (int i = 0; i < 2; i++)
        {
            Animator warriorAnimator = dragItem.GetWarriorAnimator(i);
            if (warriorAnimator != null)
            {
                warriorAnimator.ResetTrigger("Push");
                warriorAnimator.SetTrigger("Idle");
            }
        }
    }

    //событие на удержание кнопки
    private void OnButtonPress()
    {
        //инициализация состояния анимации наклона моста
        //начало наклона моста -> 0
        //мост полностью наклонился -> 1
        float animTime = bridgeAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;

        //обновление скорости проигрывания анимации
        bridgeAnimator.speed = 0.25f;
        //установка воспроизведения анимации в обычном порядке
        bridgeAnimator.SetFloat("Direction", 1.0f);

        //состояние, когда анимация установки моста полностью проигралась
        if (animTime >= 1.0f)
        {
            isPressed = false;
            isComplete = true;
            interactionButton.gameObject.SetActive(false);
            StartCoroutine(OnGameComplete());
        }
    }

    //событие при отсутствии удержания кнопки
    private void OnButtonInactive()
    {
        //инициализация состояния анимации наклона моста
        //начало наклона моста -> 0
        //мост полностью наклонился -> 1
        float animTime = bridgeAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;

        //состояние, когда анимация наклона начала воспроизводиться
        if (animTime > 0)
        {
            //установка воспроизведения анимации задом наперед
            bridgeAnimator.SetFloat("Direction", -1.0f);
            //обновление скорости проигрывания анимации
            bridgeAnimator.speed = 0.10f;
            //отключение кнопки над отрядом
            interactionButton.gameObject.SetActive(false);
        }
        //состояние, когда анимация привела наклон моста к исходному
        else if (attachedZone != null)
        {
            //обновление скорости проигрывания анимации
            bridgeAnimator.speed = 0;
            //включение кнопки над отрядом
            interactionButton.gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        if (!isComplete)
        {
            int slotIndex = transform.parent.GetSiblingIndex();

            //отключение отображения кнопки, когда отряд не находится в 5-м слоте
            if (interactionButton.gameObject.activeSelf && slotIndex != 4) interactionButton.gameObject.SetActive(false);

            if (slotIndex != 4 && !isTransparent)
            {
                for (int i = 0; i < transparentPart.materials.Length; i++)
                {
                    Color temp = transparentPart.materials[i].color;
                    temp = new Color(temp.r, temp.g, temp.b, 0.3f);
                    transparentPart.materials[i].color = temp;
                }
                SetMaterialTransparent();
                isTransparent = true;
            }
            else if (slotIndex == 4 && isTransparent)
            {
                for (int i = 0; i < transparentPart.materials.Length; i++)
                {
                    transparentPart.materials[i].color = defColors[i];
                }
                SetMaterialOpaque();
                isTransparent = false;
            }

            if (isPressed) OnButtonPress();
            else OnButtonInactive();
        }
    }

    private IEnumerator OnGameComplete()
    {
        //увеличение счетчика установленных мостов
        completedGameCount += 1;

        //подключение анимации для воинов отряда
        dragItem.SetAnimationTrigger("StartBridge");
        //разморозка камеры
        CamDrag.UnlockCamera(5);

        //запуск событий, которые необходимо выполнить перед уничтожением отряда
        Hp squadHp = dragItem.GetComponentHp();
        squadHp.onSquadDeath.Invoke();

        //уничтожение компонентов отряда, которые больше не будут использоваться
        Destroy(squadHp);
        Destroy(dragItem.GetAttachedColider());
        Destroy(dragItem);
        Destroy(squadRigidbody);

        //Удаление скрипта, который отвечает за вращение колес
        for (int i = 0; i < wheelMove.Length; i++)
        {
            Destroy(wheelMove[i]);
        }

        //обновление родительского объекта для отряда
        transform.SetParent(transform.root.GetChild(0));

        //ожидание определенного времени
        yield return new WaitForSeconds(3.5f);
        //уничтожение связанной с отрядом зоны
        Destroy(attachedZone);
        //обновление скорости для линии
        lineMove.SetSpeed(0.4f);

        //корректировка высоты отрядов, которые переходят мост
        //выполняется для всех слотов пока позиция последнего слота не будет скорректирована
        float bridgeEndPos = transform.position.z + 2.54F;
        Transform lastSlotTransform = lineBrain.slotHolder[0].transform;
        while (lastSlotTransform.position.z < bridgeEndPos || lastSlotTransform.position.y < transform.position.y)
        {
            for (int i = 0; i < 4; i++)
            {
                Transform slotTransform = lineBrain.slotHolder[i].transform;
                //изменение высоты отряда, когда он в процессе пересечения моста
                if ((slotTransform.position.z > transform.position.z - 1.2f) && (slotTransform.position.z < transform.position.z + 2.54f))
                {
                    //обновление позиций высоты слота
                    float step = 0.1f * Time.deltaTime;
                    Vector3 newPos = new Vector3(slotTransform.position.x, transform.position.y + 0.14f, slotTransform.position.z);
                    slotTransform.position = Vector3.MoveTowards(slotTransform.position, newPos, step);
                }
            }

            yield return null;
        }

        //уничтожение кнопки для отряда моста (вместе со скриптом)
        Destroy(transform.GetChild(0).gameObject);
        Destroy(this);
    }

    private void SetMaterialOpaque()
    {
        foreach (Material material in transparentPart.materials)
        {
            material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            material.SetInt("_ZWrite", 1);
            material.DisableKeyword("_ALPHATEST_ON");
            material.DisableKeyword("_ALPHABLEND_ON");
            material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            material.renderQueue = -1;
        }
    }

    private void SetMaterialTransparent()
    {
        foreach (Material material in transparentPart.materials)
        {
            material.SetFloat("_Mode", 2);
            material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            material.SetInt("_ZWrite", 0);
            material.DisableKeyword("_ALPHATEST_ON");
            material.EnableKeyword("_ALPHABLEND_ON");
            material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            material.renderQueue = 3000;
        }
    }
}