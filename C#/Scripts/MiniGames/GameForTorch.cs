﻿using UnityEngine.UI;
using UnityEngine;

public class GameForTorch : MonoBehaviour
{
    //указывает, находился ли отряд в состоянии, когда свет полность погас
    //значение переменной общее для всех отрядов в армии
    public static bool lightFaded;

    public bool isShining
    {
        get
        {
            return lighting.intensity > 0 ? true : false;
        }
    }

    //триггер состояния кнопки
    private bool isPressed = false;
    //инициализация цвета для "активного" режима кнопки
    private Color colActive = new Vector4(1, 1, 0, 1);
    //персональный источник света для отряда
    private Light lighting;

    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;
    //спрайт над кнопкой
    private Image interactionButtonImage;
    private AudioSource audioSource;


    private void Start()
    {
        //инициализация связаного с отрядом источника света
        lighting = transform.GetChild(0).GetChild(3).GetComponent<Light>();
        //установка максимальной яркости для источника света
        lighting.intensity = 1;

        //инициализация кнопки над отрядом и событий для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onPointerDown.AddListener(OnPointerDown);
        interactionButton.onPointerUp.AddListener(OnPointerUp);

        //инициализация спрайта над кнопкой
        interactionButtonImage = interactionButton.GetComponent<Image>();
        //при старте кнопка факелов полностью закрашена
        interactionButtonImage.color = colActive;

        audioSource = transform.GetComponent<AudioSource>();
    }

    //событие по нажатию на кнопку
    private void OnPointerDown()
    {
        //обновление триггера состояния кнопки
        isPressed = true;
        //"заморозка" камеры
        CamDrag.LockCamera(3);
        //запуск звука
        audioSource.Play();
    }

    //событие по отжатию кнопки
    private void OnPointerUp()
    {
        //обновление триггера состояния кнопки
        isPressed = false;
        //отмена запроса на "заморозку" камеры
        CamDrag.UnlockCamera(3);
    }

    //событие на удержание кнопки
    private void OnButtonPress()
    {
        //состояние, когда у кнопки исходный цвет и полное закрашивание 
        if ((interactionButtonImage.color == Color.white) && (interactionButtonImage.fillAmount == 1))
        {
            //обнуление закрашивания
            interactionButtonImage.fillAmount = 0;
            //обновление цвета кнопки на "активный"
            interactionButtonImage.color = colActive;
        }

        //состояние, когда у кнопки "активный" цвет
        if (interactionButtonImage.color == colActive)
        {
            //увеличение параметра закрашивания кнопки
            interactionButtonImage.fillAmount += 0.01f;
        }
    }

    //событие при отсутствии удержания кнопки
    private void OnButtonInactive()
    {
        //при "активном" цвете кнопки ее закрашивание постепенно уменьшается 
        if (interactionButtonImage.color == colActive)
        {
            interactionButtonImage.fillAmount -= 0.0005f;
        }

        //состояние, когда свет от отряда полностью погас
        if ((interactionButtonImage.color == colActive) && (interactionButtonImage.fillAmount == 0))
        {
            //установка исходного цвета для кнопки
            interactionButtonImage.color = Color.white;
            //полное закрашивание кнопки
            interactionButtonImage.fillAmount = 1;

            //обновление триггера, что указывает на полное угасание свет
            lightFaded = true;
        }
    }

    private void FixedUpdate()
    {
        //состояние, когда у кнопки "активный" цвет и кнопка частично закрашена
        if ((interactionButtonImage.color == colActive) && (interactionButtonImage.fillAmount >= 0))
        {
            //Привязка яркости источника света к параметру закрашивания кнопки
            lighting.intensity = interactionButtonImage.fillAmount * 2.5f;
        }
        //состояние, когда у кнопки исходный цвет
        else if (interactionButtonImage.color == Color.white)
        {
            //полное отключение яркости для источника света
            lighting.intensity = 0;
        }

        if (isPressed) OnButtonPress();
        else OnButtonInactive();
    }
}