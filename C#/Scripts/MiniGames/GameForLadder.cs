﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameForLadder : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;
    private int actionCounter;

    //лестница
    private Transform ladder;
    private LineBrain lineBrain;
    private AudioSource audioSource;
    private GameObject teleportEffect;

    //кнопка для запуска мини-игры
    private ExtendedButton interactionButton;
    //кнопка наклона лестницы вправо
    private ExtendedButton leanRightButton;
    //кнопка наклона лестницы влево
    private ExtendedButton leanLeftButton;

    //состояние лестницы
    private bool ladderFell;
    private bool gameComplete;

    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;
        actionCounter = 0;

        ladder = transform.GetChild(5).GetChild(6);
        lineBrain = transform.parent.parent.GetComponent<LineBrain>();
        audioSource = transform.GetComponent<AudioSource>();

        //позиция телепорта смещается, когда присутствует доп. оборонительный отряд на стене
        Vector3 teleportPos = Vector3.zero;
        if (LineDefence.instance.slots[lineBrain.GetLineNum()].childCount > 2)
            teleportPos = LineDefence.instance.slots[lineBrain.GetLineNum()].GetChild(2).position;
        else
            teleportPos = LineDefence.instance.slots[lineBrain.GetLineNum()].GetChild(1).position;
        //создание доп. эффекта телепортации, который является копией исходного
        GameObject teleportInstance = LineDefence.instance.slots[lineBrain.GetLineNum()].GetChild(1).gameObject;
        teleportEffect = Instantiate(teleportInstance, teleportPos, teleportInstance.transform.rotation, LineDefence.instance.slots[lineBrain.GetLineNum()]);
        teleportEffect.name = "tp";

        //инициализация кнопки начала мини-игры и создание события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onClick.AddListener(TaskOnClick);

        //инициализация кнопки наклона лестницы вправо и создание события для неё
        leanRightButton = transform.GetChild(0).GetChild(3).GetComponent<ExtendedButton>();
        leanRightButton.onClick.AddListener(LeanRight);

        //инициализация кнопки наклона лестницы влево и создание события для неё
        leanLeftButton = transform.GetChild(0).GetChild(4).GetComponent<ExtendedButton>();
        leanLeftButton.onClick.AddListener(LeanLeft);

        ladderFell = false;
        gameComplete = false;
    }

    //изменения состояние кнопи из внешних скриптов
    public void ButtonSetActive(bool value)
    {
        if (!ladderFell) interactionButton.gameObject.SetActive(value);
    }

    //событие для кнопки начала мини-игры (раскладывание лестницы)
    private void TaskOnClick()
    {
        //отключение кнопки старта мини-игры над отрядом
        interactionButton.gameObject.SetActive(false);
        //бловировка D&D
        lineBrain.SetDragState(false);
        //проигрывание звукового эффекта
        audioSource.Play();

        int lineNum = lineBrain.GetLineNum();
        bool isEmptySlot = LineDefence.instance.slots[lineNum].childCount > 1 ? false : true;

        //обработка события, когда оборонительный отряд препятствует установке лестниц
        if (!isEmptySlot) StartCoroutine(StartGame());
        //обработка события, когда ничто не препятствует установке лестниц
        else
        {
            //увеличение счетчика выполненных мини-игр
            completedGameCount += 1;
            //запуск полной анимации 
            StartCoroutine(PlayFullAnimation());
            //запуск подсчета выживших отрядов 
            ZoneExit.CountWarriors(lineBrain);
        }
    }

    //событие при нажатии на кнопку наклона лестницы влево
    private void LeanLeft()
    {
        //запуск короутины, которая делает анимацию наклона плавнее
        StartCoroutine(RotateLeft());
    }

    //короутина, которая плавно наклоняет лестницу влево
    private IEnumerator RotateLeft()
    {
        actionCounter++;
        //получение угла и оси поворота главной секции лестницы
        float angle = 0.0f;
        Vector3 axis = Vector3.zero;
        ladder.rotation.ToAngleAxis(out angle, out axis);
        //определение велечины множителя
        float factor = angle > 120 ? 4.5f : 3f;

        Quaternion startRotation = ladder.rotation;
        Quaternion targetRotation = startRotation * Quaternion.Euler(Vector3.left * 10.0f * factor);
        float timeCount = 0.0f;
        while (ladder.rotation != targetRotation)
        {
            if ((timeCount != 0 && actionCounter > 1) || ladderFell || gameComplete)
            {
                actionCounter--;
                yield break;
            }

            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += 2 * Time.deltaTime;
            yield return null;
        }
        actionCounter--;
    }

    //событие при нажатии на кнопку наклона лестницы вправо
    private void LeanRight()
    {
        //запуск короутины, которая делает анимацию наклона плавнее
        StartCoroutine(RotateRight());
    }

    //короутина, которая плавно наклоняет лестницу вправо
    private IEnumerator RotateRight()
    {
        actionCounter++;
        //получение угла и оси поворота главной секции лестницы
        float angle = 0.0f;
        Vector3 axis = Vector3.zero;
        ladder.rotation.ToAngleAxis(out angle, out axis);
        //определение велечины множителя
        float factor = angle < 60 ? 4.5f : 3f;

        Quaternion startRotation = ladder.rotation;
        Quaternion targetRotation = startRotation * Quaternion.Euler(Vector3.right * 10.0f * factor);
        float timeCount = 0.0f;
        while (ladder.rotation != targetRotation)
        {
            if ((timeCount != 0 && actionCounter > 1) || ladderFell || gameComplete)
            {
                actionCounter--;
                yield break;
            }

            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += 2 * Time.deltaTime;
            yield return null;
        }
        actionCounter--;
    }

    //приведение лестницы к перпендикулярному положению
    private IEnumerator FirstStage()
    {
        Quaternion targetRotation = Quaternion.Euler(90.0f, 0, 0);
        Quaternion startRotation = ladder.rotation;
        float timeCount = 0.0f;

        while (ladder.rotation != targetRotation)
        {
            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime * 2;
            yield return null;
        }
    }

    //приведение верхней секции лестницы к перпендикулярному положению
    private IEnumerator SecondStage()
    {
        Quaternion targetRotation = Quaternion.Euler(0, 0, 0);
        Quaternion startRotation = ladder.GetChild(0).localRotation;
        float timeCount = 0.0f;

        while (ladder.GetChild(0).localRotation != targetRotation)
        {
            ladder.GetChild(0).localRotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime * 2;
            yield return null;
        }
    }

    //приведение лестницы к наклону о стену
    private IEnumerator ThirdStage()
    {
        Quaternion targetRotation = Quaternion.Euler(120.0f, 0, 0);
        Quaternion startRotation = ladder.rotation;
        float timeCount = 0.0f;

        while (ladder.rotation != targetRotation)
        {
            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime * 2;
            yield return null;
        }
    }

    //приведение основной секции лестницы к исходному состоянию
    private IEnumerator FourthStage()
    {
        Quaternion targetRotation = Quaternion.Euler(-33.0f, 0, 0);
        Quaternion startRotation = ladder.rotation;
        float timeCount = 0.0f;

        while (ladder.rotation != targetRotation)
        {
            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime * 2;
            yield return null;
        }
    }

    //приведение основной секции лестницы к исходному состоянию
    private IEnumerator FifthStage()
    {
        Quaternion targetRotation = Quaternion.Euler(-33.0f, 0, 0);
        Quaternion startRotation = ladder.rotation;
        float timeCount = 0.0f;

        while (ladder.rotation != targetRotation)
        {
            ladder.rotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime;
            yield return null;
        }
    }

    //приведение верхней секции лестницы к исходному состоянию
    private IEnumerator SixthStage()
    {
        Quaternion targetRotation = Quaternion.Euler(160, 0, 0);
        Quaternion startRotation = ladder.GetChild(0).localRotation;
        float timeCount = 0.0f;

        while (ladder.GetChild(0).localRotation != targetRotation)
        {
            ladder.GetChild(0).localRotation = Quaternion.Slerp(startRotation, targetRotation, timeCount);
            timeCount += Time.deltaTime * 2;
            yield return null;
        }
    }

    private IEnumerator PlayFullAnimation()
    {
        yield return StartCoroutine(FirstStage());
        yield return StartCoroutine(SecondStage());
        yield return StartCoroutine(ThirdStage());
        StartCoroutine(PlayClimbingAnim());
    }

    private IEnumerator StartGame()
    {
        //запуск анимации лестницы с задержками
        yield return StartCoroutine(FirstStage());
        yield return StartCoroutine(SecondStage());

        //включение кнопок наклона лестницы
        leanRightButton.gameObject.SetActive(true);
        leanLeftButton.gameObject.SetActive(true);

        //инициализация времени начала мини-игры
        float gameStartTime = Time.time;
        //продолжительность мини-игры
        float gameDuration = 0;

        //направление падения главной секции лестницы
        //0 -> исходное состояние (состояние равновесия)
        //1 -> наклон влево (менее 90 градусов)
        //2 -> наклон вправо (более 90 градусов)
        int direction = UnityEngine.Random.Range(1, 3);
        //выход подвижной секция из зоны условной стабильности
        bool lostBalance = false;

        //выполнение мини-игры
        while (!ladderFell && gameDuration < 10)
        {
            //получение угла и оси поворота главной секции лестницы
            float angle = 0.0f;
            Vector3 axis = Vector3.zero;
            ladder.rotation.ToAngleAxis(out angle, out axis);

            //расчет величины ускорения при вращении главной секции лестницы
            float rotationBoost = 0;
            if (axis.x > 0 && axis.x < 2) rotationBoost = Mathf.Abs(Mathf.Cos(angle * Mathf.Deg2Rad));
            else rotationBoost = 1;

            //состояние потери равновесия
            if ((angle < 89.0f || angle > 91.0f) && !lostBalance) lostBalance = true;
            //обретение равновесия, определение направления падения
            if ((angle >= 89.0f && angle <= 91.0f) && lostBalance)
            {
                lostBalance = false;
                direction = UnityEngine.Random.Range(1, 3);
            }
            //корректировка текущего направления падения (для случаев, когда напрвление было изменено пользователем)
            else if (angle < 89.0f && direction == 2) direction = 1;
            else if (angle > 91.0f && direction == 1) direction = 2;

            //состояние, когда лестница падает влево
            if (direction == 1 && actionCounter == 0)
            {
                //ускоренное вращение на начальном этапе
                if (angle > 88.0f)
                {
                    ladder.Rotate(Vector3.left * 0.1f);
                }
                //стандартное вращение
                else ladder.Rotate(Vector3.left * rotationBoost * 1.8f);
            }
            //состояние, когда лестница падает вправо
            else if (direction == 2 && actionCounter == 0)
            {
                //ускоренное вращение на начальном этапе
                if (angle < 92.0f)
                {
                    ladder.Rotate(Vector3.right * 0.1f);
                }
                //стандартное вращение
                else ladder.Rotate(Vector3.right * rotationBoost * 1.8f);
            }

            if (angle <= 40.0f || angle >= 140.0f)
            {
                //отключение бловировки D&D 
                lineBrain.SetDragState(true);

                //отключение кнопок наклона лестницы
                leanRightButton.gameObject.SetActive(false);
                leanLeftButton.gameObject.SetActive(false);

                //возврат лестницы в исходное состояние
                if (angle <= 40) StartCoroutine(FourthStage());
                else StartCoroutine(FifthStage());
                StartCoroutine(SixthStage());

                //прерывание мини-игры
                ladderFell = true;
            }

            //задержка в 1 кадр
            yield return new WaitForFixedUpdate();

            //расчет продолжительности игры
            gameDuration = Time.time - gameStartTime;
        }

        //завершение пройденной мини-игры
        if (!ladderFell)
        {
            gameComplete = true;
            //увеличение счетчика выполненных мини-игр
            completedGameCount += 1;

            //отключение кнопок наклона лестницы
            leanRightButton.gameObject.SetActive(false);
            leanLeftButton.gameObject.SetActive(false);

            //запуск подсчета выживших отрядов 
            ZoneExit.CountWarriors(lineBrain);

            //включение эффекта телепорта для оборонительного отряда на стене
            GameObject temp = Instantiate(teleportEffect, LineDefence.instance.slots[lineBrain.GetLineNum()]);
            temp.name = "tp";
            teleportEffect.SetActive(true);
            teleportEffect = temp;
            //уничтожение оборонительного отряда на стене
            Destroy(LineDefence.instance.slots[lineBrain.GetLineNum()].GetChild(0).gameObject);

            //проигрывание звукового эффекта
            audioSource.Play();

            //анимация установки лестницы
            yield return StartCoroutine(ThirdStage());
            //анимация подъема по лестнице
            StartCoroutine(PlayClimbingAnim());
        }
    }

    //анимированный подъем отрядов по лестнице
    private IEnumerator PlayClimbingAnim()
    {
        //выявление отрядов для которых возможно прпоигрывание анимации
        for (int i = 4; i >= 0; i--)
        {
            if (lineBrain.squadHolder[i] != null)
            {
                Type miniGameType = lineBrain.squadHolder[i].miniGame.GetType();
                if (miniGameType != typeof(GameForLadder) && miniGameType != typeof(GameForRam))
                {
                    for (int j = 3; j >= 0; j--)
                    {
                        Animator warriorAnimator = lineBrain.squadHolder[i].GetWarriorAnimator(j);
                        if (warriorAnimator != null)
                        {
                            yield return new WaitForSeconds(2.0f);
                            StartCoroutine(SetClimbingWarrior(warriorAnimator));
                            warriorAnimator.transform.SetParent(transform.root.GetChild(0));
                        }
                    }
                    Destroy(lineBrain.squadHolder[i].gameObject);
                }
            }
        }
    }

    //анимация подъема одного воина
    private IEnumerator SetClimbingWarrior(Animator warriorAnimator)
    {
        //перемещение воина к лестнице
        warriorAnimator.SetTrigger("Walk");
        Vector3 targetPos = ladder.GetChild(1).transform.position;
        while (warriorAnimator.transform.position != targetPos)
        {
            float step = 0.4f * Time.deltaTime;
            warriorAnimator.transform.position = Vector3.MoveTowards(warriorAnimator.transform.position, targetPos, step);
            yield return null;
        }

        //ходьба воина по лестнице
        targetPos = ladder.GetChild(2).transform.position;
        while (warriorAnimator.transform.position != targetPos)
        {
            float step = 0.4f * Time.deltaTime;
            warriorAnimator.transform.position = Vector3.MoveTowards(warriorAnimator.transform.position, targetPos, step);
            yield return null;
        }

        //подъем воина по лестнице
        warriorAnimator.SetTrigger("Idle");
        warriorAnimator.SetTrigger("Climbing");
        targetPos = ladder.GetChild(3).transform.position;
        while (warriorAnimator.transform.position != targetPos)
        {
            float step = 0.4f * Time.deltaTime;
            warriorAnimator.transform.position = Vector3.MoveTowards(warriorAnimator.transform.position, targetPos, step);
            yield return null;
        }

        //включение эффекта телепорта
        GameObject temp = Instantiate(teleportEffect, LineDefence.instance.slots[lineBrain.GetLineNum()]);
        temp.name = "tp";
        teleportEffect.SetActive(true);
        teleportEffect = temp;

        //уничтожение анимируемого объекта
        Destroy(warriorAnimator.gameObject);
    }

    private void FixedUpdate()
    {
        //отключение отображения кнопки, когда отряд не находится в 5-м слоте
        if (interactionButton.gameObject.activeSelf)
        {
            int slotIndex = transform.parent.GetSiblingIndex();
            if (slotIndex != 4) interactionButton.gameObject.SetActive(false);
        }
    }
}