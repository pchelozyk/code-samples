﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class GameForArcher : MonoBehaviour
{
    //кол-во полностью выполненых мини-игр
    //счетчик общий для всех экземпляров класса
    public static int completedGameCount;

    //кнопка для выполнения мини-игры
    private ExtendedButton interactionButton;
    private Image interactionButtonImage;
    public Sprite idleMode;
    public Sprite activeMode;

    private Move lineMove;
    private ZonesInfo zonesInfo;
    private DragItem dragItem;
    private Transform targetSlot;
    private Component target;
    private AudioSource shotSound;

    [Range(0.01f, 1.0f)]
    public float heightFactor;
    [Range(0.01f, 1.0f)]
    public float positionFactor;
    public float[] targetTimeLimit = new float[2];
    private float targetTime;
    private bool isPressed;
    private bool isFlying;
    private bool shotAnimSet;

    private void Start()
    {
        //инициализация счетчика 
        completedGameCount = 0;

        //инициализация кнопки над отрядом и события для неё
        interactionButton = transform.GetChild(0).GetChild(0).GetComponent<ExtendedButton>();
        interactionButton.onPointerDown.AddListener(OnPointerDown);
        interactionButton.onPointerUp.AddListener(OnPointerUp);
        //инициализация спрайта кнопки
        interactionButtonImage = interactionButton.GetComponent<Image>();

        lineMove = transform.parent.parent.GetComponent<Move>();
        zonesInfo = transform.parent.parent.GetComponent<ZonesInfo>();
        dragItem = transform.GetComponent<DragItem>();
        shotSound = transform.GetComponent<AudioSource>();

        Hp squadHp = GetComponent<Hp>();
        squadHp.onSquadDeath.AddListener(OnDeath);

        //поиск цели выстрела
        int lineNum = lineMove.transform.GetSiblingIndex() - 1;
        targetSlot = LineDefence.instance.slots[lineNum];
        //определение типа цели
        target = targetSlot.GetChild(0).GetComponent<Archer>();
        if (target == null)
            target = targetSlot.GetChild(0).GetComponent<Ballista>();

        targetTime = 0;
        isPressed = false;
        isFlying = false;
        shotAnimSet = false;
    }

    private void OnPointerDown()
    {
        isPressed = true;

        //остановка линии
        lineMove.SetSpeed(0, 1);
        //"заморозка" камеры
        CamDrag.LockCamera(2);

        if (interactionButtonImage.sprite == idleMode)
        {
            //смена режима кнопки и откат ее "заполнения"
            interactionButtonImage.sprite = activeMode;
            interactionButtonImage.fillAmount = 0;
        }
    }

    private void OnPointerUp()
    {
        isPressed = false;
        shotAnimSet = false;

        dragItem.SetAnimationTrigger("Idle");
        //запуск движения линии 
        lineMove.SetSpeed(0.4f, 1);
        //проверка на возможность "разморозить" камеру
        CamDrag.UnlockCamera(2);

        //отключение возможности взаимодействовать с кнопкой
        interactionButtonImage.raycastTarget = false;
    }

    private void OnDeath()
    {
        if (isPressed)
        {
            //запуск движения линии 
            lineMove.SetSpeed(0.4f, 1);
            //проверка на возможность "разморозить" камеру
            CamDrag.UnlockCamera(2);
        }
    }

    private void FixedUpdate()
    {
        //обновление отсчета времени до выстрела
        if (targetTime > 0) targetTime -= Time.deltaTime;

        //обновление данных о текущей зоне для отряда
        int slotIndex = transform.parent.GetSiblingIndex();
        int currentZone = zonesInfo.GetSlotZone(slotIndex);

        if (currentZone != 1 && interactionButton.gameObject.activeSelf)
        {
            //возврат кнопки в исходное состояние
            interactionButton.gameObject.SetActive(false);
            interactionButtonImage.sprite = idleMode;
            interactionButtonImage.fillAmount = 1;
        }

        if (currentZone == 1 && targetTime <= 0)
        {
            if (!interactionButton.gameObject.activeSelf && !isFlying)
            {
                //включение кнопки
                interactionButton.gameObject.SetActive(true);
                interactionButtonImage.raycastTarget = true;
            }

            // Состояние, когда кнопка не нажата
            if (!isPressed)
            {
                Animator warriorAnimator = dragItem.GetWarriorAnimator(0);
                AnimatorClipInfo[] m_CurrentClipInfo = warriorAnimator.GetCurrentAnimatorClipInfo(0);

                //если кнопка в активном режиме
                if (interactionButtonImage.sprite == activeMode)
                {
                    //если присутствует "заполнение" кнопки
                    if (interactionButtonImage.fillAmount > 0)
                    {
                        interactionButtonImage.fillAmount -= 0.05f;
                    }
                    //если отсутствует "заполнение" кнопки и анимация сброшена полностью
                    else if (m_CurrentClipInfo[0].clip.name != "Shot")
                    {
                        //смена режима кнопки
                        interactionButtonImage.sprite = idleMode;
                        interactionButtonImage.fillAmount = 1;

                        //включение возможности взаимодействовать с кнопкой
                        interactionButtonImage.raycastTarget = true;
                    }
                }
            }
            //cостояние, когда кнопка нажата и линия остаётся неподвижной
            else if (lineMove.GetSpeed() == 0)
            {
                //срабатывание триггера на выстрел
                if (!shotAnimSet)
                {
                    Animator warriorAnimator = dragItem.GetWarriorAnimator(0);
                    AnimatorClipInfo[] m_CurrentClipInfo = warriorAnimator.GetCurrentAnimatorClipInfo(0);
                    if (m_CurrentClipInfo[0].clip.name == "Idle")
                    {
                        dragItem.ResetAnimationTrigger("Idle");
                        dragItem.ResetAnimationTrigger("Walk");
                        dragItem.SetAnimationTrigger("Shot");
                        shotAnimSet = true;
                    }
                }

                //если в момент нажатия кнопка в активном режиме, но мини-игра еще не выполнена
                if (interactionButtonImage.sprite == activeMode && interactionButtonImage.fillAmount < 1)
                {
                    Animator warriorAnimator = dragItem.GetWarriorAnimator(0);
                    AnimatorClipInfo[] m_CurrentClipInfo = warriorAnimator.GetCurrentAnimatorClipInfo(0);
                    float animNormalizedTime = warriorAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;

                    if (m_CurrentClipInfo[0].clip.name == "Shot" && animNormalizedTime > interactionButtonImage.fillAmount)
                    {
                        interactionButtonImage.fillAmount = animNormalizedTime;
                    }
                    else interactionButtonImage.fillAmount += 0.005f;
                }

                //если в момент нажатия кнопка в активном режиме и мини-игра выполнена
                if (interactionButtonImage.sprite == activeMode && interactionButtonImage.fillAmount >= 1)
                {
                    completedGameCount += 1;
                    shotSound.Play();
                    StartCoroutine(Shoot());

                    //возврат кнопки в исходное состояние
                    interactionButton.gameObject.SetActive(false);
                    interactionButtonImage.sprite = idleMode;

                    //откат триггеров в исходное состояние и ускорение линии
                    OnPointerUp();
                }
            }
        }
    }

    private IEnumerator Shoot()
    {
        //расчет расстояния от стартовой точки до цели
        float dist = Vector3.Distance(transform.position, targetSlot.position);

        //определение стартового положения для точки следования
        float posY = transform.position.y + dist * heightFactor;
        float posZ = transform.position.z + dist * positionFactor;
        Vector3 followPoint = new Vector3(transform.position.x, posY, posZ);

        //подготовка стрелы к выстрелу
        int numInHierarchy = transform.childCount - 1;
        Transform arrow = transform.GetChild(numInHierarchy).GetChild(0);
        Vector3 relativePos = (followPoint - arrow.position) * (-1);
        arrow.rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        arrow.gameObject.SetActive(true);

        //полет стрелы
        while (Vector3.Distance(arrow.position, targetSlot.position) > 1)
        {
            isFlying = true;
            float step = 11 * Time.deltaTime;

            //перемещение точки следования
            followPoint = Vector3.MoveTowards(followPoint, targetSlot.position, step);
            //перемещение стрелы
            arrow.position = Vector3.MoveTowards(arrow.position, followPoint, step);
            //вращение стрелы
            relativePos = (followPoint - arrow.position) * (-1);
            arrow.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

            yield return null;
        }

        if (target != null)
        {
            //применение стана к цели
            if (target.GetType() == typeof(Archer))
            {
                Archer targetArcher = (Archer)target;
                targetArcher.SetStun(17);
            }
            else
            {
                Ballista targetBallista = (Ballista)target;
                targetBallista.SetStun(13);
            }
        }

        //исходное состояние стрелы
        arrow.gameObject.SetActive(false);
        arrow.localPosition = Vector3.zero;
        isFlying = false;

        //предустановка targetTime для следующего выстрела
        targetTime = Random.Range(targetTimeLimit[0], targetTimeLimit[1]);
    }
}