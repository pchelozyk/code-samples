﻿using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private static bool isPaused;

    void Start()
    {
        TimeFreeze();
    }

    public static void TimeFreeze()
    {
        Time.timeScale = 0f;
        isPaused = true;
        AudioListener.pause = true;
        LineBrain.SetGlobalDragState(false); 
    }

    public static void TimeUnfreeze()
    {
        Time.timeScale = 1;
        isPaused = false;
        AudioListener.pause = false;
        LineBrain.SetGlobalDragState(true); 
    }

    public static bool IsPaused ()
    {
        return isPaused;
    }
}
