﻿using Cinemachine;
using UnityEngine;

public class PlusCam : MonoBehaviour
{
    public static float m_FieldOfView;
    private CinemachineVirtualCamera vcam;

    [Header("Минимальное значение fov")]
    public float min;
    [Header("Максимальное значение fov")]
    public float max;
    [Header("Скорость скрола колесика")]
    public float SpeedScroll;

    private float smoothTime = 0.3f;
    private float Velocity = 0.0f;

    private bool mobileInput;

    private void Awake()
    {
        vcam = this.GetComponent<CinemachineVirtualCamera>();
    }

    void Start()
    {
        m_FieldOfView = max;
        mobileInput = false;

#if UNITY_ANDROID
        mobileInput = true;
#endif
    }

    void LateUpdate()
    {
        if (!mobileInput)
        {
            if (Time.timeScale == 1)
            {
                m_FieldOfView = Mathf.Clamp(m_FieldOfView - Input.GetAxis("Mouse ScrollWheel") * SpeedScroll, min, max);
            }

            if (Input.GetMouseButtonDown(2))
            {
                m_FieldOfView = 20.0f;
            }
        }

        if (mobileInput)
        {
            if (Time.timeScale == 1)
            {
                if (Input.touchCount == 2)
                {
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    float prevDist = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float curDist = (touchZero.position - touchOne.position).magnitude;

                    float difference = curDist - prevDist;
                    m_FieldOfView = Mathf.Clamp(m_FieldOfView - difference * SpeedScroll, min, max);
                }
            }
        }

        float curFieldOfView = vcam.m_Lens.FieldOfView;
        vcam.m_Lens.FieldOfView = Mathf.SmoothDamp(curFieldOfView, m_FieldOfView, ref Velocity, smoothTime);
    }
}