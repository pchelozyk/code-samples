﻿using UnityEngine;

public class CamDragLegacy : MonoBehaviour {
	[Header("Скорость")]
	public float speed;

	[Header("Нижний предел")]
	public float LoverLimit;

	[Header("Верхний предел")]
	public float UpperLimit;

	private float target_pos;
	private Vector2 start_pos;
	private Camera cam;

	private bool СheckSpeed;

	private void Start () {
		cam = GetComponent<Camera> ();
		target_pos = transform.position.z;
		СheckSpeed = false;
		
	}

	private void Update () {
		if (Input.GetMouseButtonDown (0)) {
			start_pos = cam.ScreenToViewportPoint (Input.mousePosition);
		} else if (Input.GetMouseButton (0)) {
			float pos = cam.ScreenToViewportPoint (Input.mousePosition).y - start_pos.y;
			target_pos = Mathf.Clamp (transform.position.z - pos, LoverLimit, UpperLimit);
		}

		if (TimeManager.IsPaused() && (СheckSpeed == false))
		{
			СheckSpeed = true;
			speed *= 10000;
		}

		if (!TimeManager.IsPaused() && (СheckSpeed == true))
		{
			СheckSpeed = false;
			speed /= 10000;
		}

		transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Lerp (transform.position.z, target_pos, speed * Time.deltaTime));

	}

}