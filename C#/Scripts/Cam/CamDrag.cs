﻿using UnityEngine;

public class CamDrag : MonoBehaviour
{
    public bool secondCamDrag = false;
    public float panSpeed;
    public float panBorderThickness = 40f;
    [Header("Нижний предел")]
    public float lowerLimit;
    [Header("Верхний предел")]
    public float upperLimit;

    private Vector2 startPos;
    //запросы на блокировку движения камеры
    private static int[] lockRequests = new int[6];
    //триггер для блокировки движения камеры
    private static bool camLock;


    void Start()
    {
        //Запрос от DragAndDrop system
        lockRequests[0] = 0;
        //Запрос от PromptLevel
        lockRequests[1] = 0;
        //Запрос от лучников(атакующих)
        lockRequests[2] = 0;
        //Запрос от факела
        lockRequests[3] = 0;
        //Запрос от копейщиков
        lockRequests[4] = 0;
        //Запрос от моста
        lockRequests[5] = 0;

        startPos = Vector2.zero;
        camLock = false;
    }

    void LateUpdate()
    {
        if (camLock) return;

        //управление камерой с помощью AD
        if (Input.GetKey(KeyCode.D))
        {
            Vector3 pos = transform.position;
            pos.z += panSpeed;
            pos.z = Mathf.Clamp(pos.z, lowerLimit, upperLimit);
            transform.position = Vector3.MoveTowards(transform.position, pos, panSpeed);
            return;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Vector3 pos = transform.position;
            pos.z -= panSpeed;
            pos.z = Mathf.Clamp(pos.z, lowerLimit, upperLimit);
            transform.position = Vector3.MoveTowards(transform.position, pos, panSpeed);
            return;
        }

        //управление камерой с помощью перемещения курсора к границе экрана
        if (!secondCamDrag)
        {
            Vector3 pos = transform.position;

            if (Input.mousePosition.y >= Screen.height - panBorderThickness && Input.mousePosition.x >= Screen.width * 0.8f)
            {
                pos.z += panSpeed;
            }
            else if (Input.mousePosition.y <= panBorderThickness && Input.mousePosition.x <= Screen.width * 0.2f)
            {
                pos.z -= panSpeed;
            }
            else if (Input.mousePosition.x >= Screen.width - panBorderThickness && Input.mousePosition.y >= Screen.height * 0.7f)
            {
                pos.z += panSpeed;
            }
            else if (Input.mousePosition.x <= panBorderThickness && Input.mousePosition.y <= Screen.height * 0.3f)
            {
                pos.z -= panSpeed;
            }

            pos.z = Mathf.Clamp(pos.z, lowerLimit, upperLimit);
            transform.position = Vector3.MoveTowards(transform.position, pos, panSpeed);
        }

        //управление камерой с помощью захвата и перетаскивания
        if (secondCamDrag)
        {
            //действия, выполняемые на отжатие ЛКМ
            if (Input.GetMouseButtonUp(0))
            {
                startPos = Vector2.zero;
            }

            //действия, выполняемые по первому нажатию ЛКМ
            if (Input.GetMouseButtonDown(0))
            {
                startPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            }
            //действия, выполняемые по удержанию ЛКМ нажатой
            else if ((Input.GetMouseButton(0)) && (startPos != Vector2.zero))
            {
                float posY = Camera.main.ScreenToViewportPoint(Input.mousePosition).y - startPos.y;
                float targetPosZ = Mathf.Clamp(transform.position.z - posY, lowerLimit, upperLimit);
                Vector3 targetPos = new Vector3(transform.position.x, transform.position.y, targetPosZ);
                transform.position = Vector3.MoveTowards(transform.position, targetPos, panSpeed);
            }
        }
    }

    public static void LockCamera(int eventIndex)
    {
        camLock = true;
        lockRequests[eventIndex] += 1;
    }

    public static void UnlockCamera(int eventIndex)
    {
        if (lockRequests[eventIndex] > 0) lockRequests[eventIndex] -= 1;
        for (int i = 0; i < lockRequests.Length; i++)
        {
            if (lockRequests[i] > 0) return;
        }
        camLock = false;
    }
}