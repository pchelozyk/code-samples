﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class ContinueGame : MonoBehaviour
{
    public GameObject pauseButton;
    public GameObject challengesButton;
    public GameObject CM_vcam2;

    private UnityEvent _onGameContinue = new UnityEvent();
    public UnityEvent onGameContinue
    {
        get { return _onGameContinue; }
        set { _onGameContinue = value; }
    }


    void Start()
    {
        transform.GetComponent<Button>().onClick.AddListener(_ContinueGame);
    }

    private void _ContinueGame()
    {
        pauseButton.SetActive(true);
        challengesButton.SetActive(true);
        CM_vcam2.SetActive(false);
        transform.parent.gameObject.SetActive(false);

        TimeManager.TimeUnfreeze();
        _onGameContinue.Invoke();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) _ContinueGame();
    }
}