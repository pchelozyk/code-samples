﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitButton : MonoBehaviour
{
    //индекс загружаемого уровня
    public int levelIndex;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(LoadLevel);
    }

    //действия, происходящие по нажатию кнопки
    private void LoadLevel()
    {
        //отключение игровой паузы
        TimeManager.TimeUnfreeze();
        //загрузка сцены с указаным индексом
        SceneManager.LoadScene(levelIndex);
    }
}