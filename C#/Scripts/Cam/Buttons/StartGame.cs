﻿using UnityEngine.UI;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public static bool levelStarted;

    public GameObject pauseButton;
    public GameObject challengesButton;

    void Awake()
    {
        levelStarted = false;
    }

    void Start()
    {
        transform.GetComponent<Button>().onClick.AddListener(_StartGame); 
    }

    private void _StartGame()
    {
        levelStarted = true;

        pauseButton.SetActive(true);
        challengesButton.SetActive(true);
        this.gameObject.SetActive(false);

        TimeManager.TimeUnfreeze();        
    }
}