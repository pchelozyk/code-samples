﻿using UnityEngine;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject challengesButton;
    public GameObject CM_vcam2;

    void Start()
    {
        transform.GetComponent<Button>().onClick.AddListener(_PauseGame);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) _PauseGame();
    }

    private void _PauseGame()
    {
        pauseMenu.SetActive(true);
        CM_vcam2.SetActive(true);
        challengesButton.SetActive(false);
        gameObject.SetActive(false);

        TimeManager.TimeFreeze();
    }
}