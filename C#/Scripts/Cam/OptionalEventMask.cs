﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionalEventMask : MonoBehaviour
{
    public LayerMask layerMask; 

    private void Start()
    {
        GetComponent<Camera>().eventMask = layerMask;
    }
}