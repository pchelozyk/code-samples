﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCam : MonoBehaviour
{
    private Vector2 startPos;
    private Camera cam;

    private void Awake()
    {
        cam = this.GetComponent<Camera>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) startPos = cam.ScreenToWorldPoint(Input.mousePosition);
        else if (Input.GetMouseButton(0))
        {
            float pos = cam.ScreenToWorldPoint(Input.mousePosition).x - startPos.x;
            transform.position = new Vector3(transform.position.x - pos, transform.position.y, transform.position.z);
        }
    }
}
