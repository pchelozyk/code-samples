﻿using UnityEngine;

public class ZoneExit : MonoBehaviour
{
    //номер линии, с которой связана зона
    public int lineNum;
    //счетчик отрядов, которые преодолели стену
    private static int unitCount;
    private LineBrain targetLine;
    private BoxCollider targetCollider;

    private void Start()
    {
        if (lineNum != 0)
        {
            targetLine = LineBrain.linesHolder[lineNum - 1];
            targetCollider = targetLine.slotHolder[4].GetSlotCollider();
        }
    }

    private void OnTriggerEnter(Collider invader)
    {
        if (invader == targetCollider) CountWarriors(targetLine);
    }

    public static void CountWarriors(LineBrain targetLine)
    {
        //подсчет живых воинов в линии
        for (int i = 0; i < targetLine.squadHolder.Length; i++)
        {
            DragItem squad = targetLine.squadHolder[i];
            if (squad != null && squad.miniGame.GetType() != typeof(GameForLadder) && squad.miniGame.GetType() != typeof(GameForRam))
            {
                Hp unitHp = targetLine.squadHolder[i].GetComponentHp();
                unitCount += unitHp.GetHp();
            }
        }

        //отключение D&D для линии
        targetLine.SetDragState(false, 3);
    }

    public static int GetUnitCount()
    {
        return unitCount;
    }

    public static void ResetUnitCount()
    {
        unitCount = 0;
    }
}