﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneJump : MonoBehaviour
{
    [Header("Номер линии, с которой взаимодействует:")]
    public int lineNum;
    [Header("Целевая высота:")]
    public float targetHeight;
    [Header("Шаг изменения высоты:")]
    public float step = 0.3F;
    private Move lineMove;
    private List<BoxCollider> targetColliders = new List<BoxCollider>();

    private void Start()
    {
        LineBrain targetLine = LineBrain.linesHolder[lineNum - 1];
        lineMove = targetLine.GetComponent<Move>();

        for (int i = 0; i < targetLine.slotHolder.Length; i++)
        {
            targetColliders.Add(targetLine.slotHolder[i].GetSlotCollider());
        }
    }

    private void OnTriggerEnter(Collider invader)
    {
        if (!targetColliders.Contains((BoxCollider)invader)) return;

        StartCoroutine(ChangeSlotHeight(invader.transform));
    }

    private IEnumerator ChangeSlotHeight(Transform slot)
    {
        while (slot.position.y != targetHeight)
        {
            if (lineMove.GetSpeed() != 0)
            {
                float factor = lineMove.GetSpeed() > 0.4f ? lineMove.GetSpeed() / 0.4f : 1;
                float posY = Mathf.MoveTowards(slot.position.y, targetHeight, step * factor);
                slot.position = new Vector3(slot.position.x, posY, slot.position.z);
            }

            yield return new WaitForFixedUpdate();
        }
    }
}