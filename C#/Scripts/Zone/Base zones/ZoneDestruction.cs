﻿using UnityEngine;
using System.Collections;
using UnityEngine.VFX;

public class ZoneDestruction : ZoneStop
{
    public bool alternativeDestruction;
    private DragItem vanguardSquad;
    private GameForEngineer engineerMiniGame;
    private Animator zoneAnimator;
    private BoxCollider zoneCollider;
    private VisualEffect fireEffect;
    private int propertyID;

    private void Awake()
    {
        zoneAnimator = GetComponent<Animator>();
        zoneCollider = GetComponent<BoxCollider>();
        if (transform.childCount == 0)
        {
            fireEffect = transform.parent.GetChild(2).GetComponent<VisualEffect>();
            propertyID = Shader.PropertyToID("fireScale");
        }

        vanguardSquad = null;
        engineerMiniGame = null;
    }

    protected override void OnTriggerStay(Collider invader)
    {
        base.OnTriggerStay(invader);
        if (invader != targetCollider) return;

        //запрос на отображение подсказки
        if (ArmyPresetManager.levelIndex == 1 && DifficultySettings.currentDifficulty == 1)
        {
            PromptLevel.instance.showMessage(2, transform);
        }

        if (engineerMiniGame != null && targetLine.squadHolder[4] != vanguardSquad)
        {
            //выключение кнопки над отрядом
            engineerMiniGame.ButtonSetActive(false);
            engineerMiniGame.SetAttachedWall(null);
            engineerMiniGame = null;
        }

        if (targetLine.squadHolder[4] != null && targetLine.squadHolder[4] != vanguardSquad)
        {
            vanguardSquad = targetLine.squadHolder[4];

            if (vanguardSquad.miniGame.GetType() == typeof(GameForEngineer))
            {
                engineerMiniGame = (GameForEngineer)vanguardSquad.miniGame;

                //включение кнопки над отрядом
                engineerMiniGame.ButtonSetActive(true);
                //инициализация стены, которую должени уничтожить отряд
                engineerMiniGame.SetAttachedWall(this);
            }
            else if (alternativeDestruction && vanguardSquad.miniGame.GetType() == typeof(GameForTorch))
            {
                StartCoroutine(AlternativeDestruction(vanguardSquad));
            }
        }
    }

    public void DisableZone()
    {
        zoneAnimator.enabled = true;
        zoneCollider.enabled = false;

        if (transform.childCount > 0)
        {
            //подключение эффекта взрыва при уничтожении стены
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            //подключение эффекта взрыва при уничтожении частокола
            transform.parent.GetChild(1).gameObject.SetActive(true);
        }
    }

    private IEnumerator AlternativeDestruction(DragItem interactingTorch)
    {
        float destructionTime = 10;
        float destructionTimer = destructionTime;
        GameForTorch torchMiniGame = interactingTorch.miniGame as GameForTorch;
        fireEffect.gameObject.SetActive(true);

        while (destructionTimer > 0)
        {
            if (torchMiniGame == null || targetLine.squadHolder[4] != interactingTorch)
            {
                fireEffect.SetFloat(propertyID, 0);
                fireEffect.gameObject.SetActive(false);
                yield break;
            }

            if (torchMiniGame.isShining)
            {
                destructionTimer -= Time.deltaTime;

                float step = Mathf.InverseLerp(0, destructionTime, destructionTime - destructionTimer);
                fireEffect.SetFloat(propertyID, step);
            }

            yield return null;
        }

        zoneAnimator.enabled = true;
        zoneCollider.enabled = false;
        lineMove.SetSpeed(0.4f);

        yield return new WaitForSeconds(0.4f);
        fireEffect.SetFloat(propertyID, 0);
        Destroy(transform.parent.gameObject);
    }
}