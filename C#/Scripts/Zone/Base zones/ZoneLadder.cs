﻿using UnityEngine;

public class ZoneLadder : ZoneStop
{
    private DragItem vanguardSquad;
    private GameForLadder squadMiniGame;

    protected override void OnTriggerStay(Collider invader)
    {
        base.OnTriggerStay(invader);
        if (invader != targetCollider) return;

        if ((targetLine.squadHolder[4] != null) && (targetLine.squadHolder[4] != vanguardSquad))
        {
            vanguardSquad = targetLine.squadHolder[4];

            if (vanguardSquad.miniGame.GetType() == typeof(GameForLadder))
            {
                squadMiniGame = (GameForLadder)vanguardSquad.miniGame;
                //включение кнопки над отрядом
                squadMiniGame.ButtonSetActive(true);
            }
        }
    }
}