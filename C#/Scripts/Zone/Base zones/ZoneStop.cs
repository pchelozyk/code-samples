﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneStop : MonoBehaviour
{
    public int lineNum;
    protected LineBrain targetLine;
    protected BoxCollider targetCollider;
    protected Move lineMove;

    protected virtual void Start()
    {
        targetLine = LineBrain.linesHolder[lineNum - 1];
        lineMove = targetLine.GetComponent<Move>();
        targetCollider = targetLine.slotHolder[4].GetSlotCollider();
    }

    protected virtual void OnTriggerStay(Collider invader)
    {
        if (invader != targetCollider) return;

        if (lineMove.GetSpeed() != 0) lineMove.SetSpeed(0);
    }
}