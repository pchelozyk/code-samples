﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneRam : ZoneStop
{
    [Header("Необходимое кол-во таранов")]
    public int ramRequirement;
    public GameObject[] gate;
    private Animator[] gateAnimator;
    private AudioSource[] gateAudioSource;
    private DragItem vanguardSquad;
    private GameForRam squadMiniGame;

    protected override void Start()
    {
        base.Start();

        gateAnimator = new Animator[gate.Length];
        gateAudioSource = new AudioSource[gate.Length];
        for (int i = 0; i < gate.Length; i++)
        {
            gateAnimator[i] = gate[i].GetComponent<Animator>();
            gateAudioSource[i] = gate[i].GetComponent<AudioSource>();
        }
    }

    protected override void OnTriggerStay(Collider invader)
    {
        base.OnTriggerStay(invader);
        if (invader != targetCollider) return;

        int ramCount = 0;
        for (int i = targetLine.squadHolder.Length - 1; i >= 0; i--)
        {
            if (targetLine.squadHolder[i] != null && targetLine.squadHolder[i].miniGame.GetType() == typeof(GameForRam))
            {
                ramCount++;
            }
            else break;
        }

        if ((squadMiniGame != null) && ((targetLine.squadHolder[4] != vanguardSquad) || (ramCount < ramRequirement)))
        {
            //отключение мини-игры
            squadMiniGame.Deactivate();
            
            vanguardSquad = null;
            squadMiniGame = null;
        }

        if ((targetLine.squadHolder[4] != null) && (targetLine.squadHolder[4] != vanguardSquad) && (ramCount >= ramRequirement))
        {
            vanguardSquad = targetLine.squadHolder[4];
            squadMiniGame = (GameForRam)vanguardSquad.miniGame;

            //включение мини-игры
            squadMiniGame.Activate();
            //инициализация ворот, которые должени уничтожить отряд
            squadMiniGame.SetAttachedGate(gateAnimator, gateAudioSource);
        }
    }
}