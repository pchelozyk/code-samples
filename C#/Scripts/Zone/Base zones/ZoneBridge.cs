﻿using UnityEngine;

public class ZoneBridge : ZoneStop
{
    private DragItem vanguardSquad;
    private GameForBridge squadMiniGame;

    protected override void OnTriggerStay(Collider invader)
    {
        base.OnTriggerStay(invader);
        if (invader != targetCollider) return;

        if ((targetLine.squadHolder[4] != null) && (targetLine.squadHolder[4] != vanguardSquad))
        {
            vanguardSquad = targetLine.squadHolder[4];

            if (vanguardSquad.miniGame.GetType() == typeof(GameForBridge))
            {
                squadMiniGame = (GameForBridge)vanguardSquad.miniGame;

                //включение кнопки над отрядом
                squadMiniGame.ButtonSetActive(true);
                //инициализация зоны, с которой взаимодействует отряд
                squadMiniGame.SetAttachedZone(this.gameObject);
            }
        }

        if ((squadMiniGame != null) && (targetLine.squadHolder[4] != vanguardSquad))
        {
            //выключение кнопки над отрядом
            squadMiniGame.ButtonSetActive(false);
            squadMiniGame.SetAttachedZone(null);
            squadMiniGame = null;
        }
    }
}