﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZonesInfo : MonoBehaviour
{
    private static bool lastZoneReached;

    public Collider midZoneCollider;
    public Collider nearZoneCollider;

    private int[] slotsInfo = { 0, 0, 0, 0, 0 };
    private bool[] targetArea = { false, false, false, true, true };

    private void Start()
    {
        lastZoneReached = false;
    }

    public static bool IsLastZoneReached()
    {
        return lastZoneReached;
    }

    public void SetSlotInfo(int slotIndex, int zoneNum)
    {
        slotsInfo[slotIndex] = zoneNum;

        if (zoneNum == 1) 
            targetArea[slotIndex] = true;
        if (zoneNum == 2)
        {
            targetArea[slotIndex] = false;
            lastZoneReached = true;
        } 
    }

    public int GetSlotZone(int slotIndex)
    {
        return slotsInfo[slotIndex];
    }

    public bool IsReachable()
    {
        bool result = slotsInfo[0] == 2 ? false : true;
        return result; 
    } 

    public int[] GetTargetArea()
    {
        int startIndex = -1;
        int endIndex = -1;

        for (int i = 0; i < slotsInfo.Length; i++)
        {
            if (targetArea[i] == true)
            {
                if (startIndex == -1) startIndex = i;
                if (endIndex < i) endIndex = i;
            }
        }

        if ((startIndex == -1) || (endIndex == -1)) return null;
        else return new int[] { startIndex, endIndex };
    }
}