﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotInfoUpdater : MonoBehaviour
{
    private ZonesInfo zonesInfo;
    private int slotIndex;

    void Start()
    {
        zonesInfo = transform.parent.GetComponent<ZonesInfo>();
        slotIndex = transform.GetSiblingIndex();        
    }

    private void OnTriggerEnter(Collider invader)
    {
        if (invader == zonesInfo.midZoneCollider)
        {
            zonesInfo.SetSlotInfo(slotIndex, 1);
        }

        if (invader == zonesInfo.nearZoneCollider)
        {
            zonesInfo.SetSlotInfo(slotIndex, 2);
        }
    }
}
