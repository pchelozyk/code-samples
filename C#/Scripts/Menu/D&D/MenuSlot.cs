﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class MenuSlot : MonoBehaviour, IDropHandler
{
    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            if ((DragHandler.itemBeingDragged != null) && (PanelResources.instance.GetResources() >= DragHandler.itemBeingDragged.unitCost))
            {
                if ((DragHandler.itemBeingDragged.startParent != transform) && (DragHandler.itemBeingDragged.startParent.tag == "ZoneSpawn"))
                {
                    DragHandler.itemBeingDragged.gameObject.transform.SetParent(transform);

                    DragHandler createdSquad = Instantiate(DragHandler.itemBeingDragged, DragHandler.itemBeingDragged.startParent);
                    createdSquad.transform.position = DragHandler.itemBeingDragged.startParent.position;
                    createdSquad.GetImageComponent().raycastTarget = true;
                    createdSquad.name = DragHandler.itemBeingDragged.name;

                    PanelResources.instance.DecreaseResources(DragHandler.itemBeingDragged);

                    int squadIndex = DragHandler.itemBeingDragged.GetUnitIndex();
                    int slotIndex = transform.GetSiblingIndex() - 1;
                    ArmyPresetManager.AddSquad(squadIndex, slotIndex);
                }
            }
        }

        if ((DragHandler.itemBeingDragged.startParent != null) && (DragHandler.itemBeingDragged.startParent.tag == "ZoneSwap"))
        {
            if ((transform.tag == "ZoneSwap") && (DragHandler.itemBeingDragged.startParent != transform))
            {
                if (transform.childCount > 0)
                {
                    transform.GetChild(0).SetParent(DragHandler.itemBeingDragged.startParent);
                }

                DragHandler.itemBeingDragged.transform.SetParent(transform);
                int targetSlotIndex = transform.GetSiblingIndex() - 1;
                int parentSlotIndex = DragHandler.itemBeingDragged.startParent.GetSiblingIndex() - 1;
                ArmyPresetManager.SwapSquads(parentSlotIndex, targetSlotIndex);
            }
        }

        if ((transform.tag == "ZoneDelete") && (DragHandler.itemBeingDragged.startParent.tag == "ZoneSwap"))
        {
            PanelResources.instance.IncreaseResources(DragHandler.itemBeingDragged);
            Destroy(DragHandler.itemBeingDragged.gameObject);

            PanelDelete.instance.animator.ResetTrigger("Enable");
            PanelDelete.instance.animator.ResetTrigger("Disable");
            PanelDelete.instance.animator.SetTrigger("Shake");
            this.enabled = false;

            int parentSlotIndex = DragHandler.itemBeingDragged.startParent.GetSiblingIndex() - 1;
            ArmyPresetManager.RemoveSquad(parentSlotIndex);
        }
    }
}