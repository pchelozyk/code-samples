﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelDelete : MonoBehaviour
{
    public static PanelDelete instance;

    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public MenuSlot slotComponent;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        animator = this.GetComponent<Animator>();
        slotComponent = transform.GetChild(0).GetComponent<MenuSlot>();
    }
}
