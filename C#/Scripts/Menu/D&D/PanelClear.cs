﻿using UnityEngine;
using UnityEngine.UI;

public class PanelClear : MonoBehaviour
{
    public GameObject PanelStart;
    public GameObject SquadsPanel;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        PanelResources.instance.ResetResources();
        PanelPresetManager.instance.ClearPreparationPanel();
        ArmyPresetManager.ResetArmyPreset();

        SquadsPanel.SetActive(false);
        PanelStart.SetActive(false);
    }
}