﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelResources : MonoBehaviour
{
    public static PanelResources instance;

    private List<GameObject> createdUnits = new List<GameObject>();
    //очки, ограничивающие кол-во размещенных отрядов
    private int resources;
    private int defaultResources;
    private Animator animator;
    private Text textComponent;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        animator = this.GetComponent<Animator>();
        if (textComponent == null) textComponent = this.GetComponent<Text>();
    }

    private void Start()
    {
        defaultResources = resources;
    }

    public void ResetResources()
    {
        resources = defaultResources;
        for (int i = 0; i < createdUnits.Count; i++)
        { 
            Destroy(createdUnits[i]);
        }
        createdUnits.Clear();
    }

    public int GetResources()
    {
        return resources;
    }

    public void SetResources(int value)
    {
        resources = value;
        if (textComponent == null) textComponent = this.GetComponent<Text>();
        textComponent.text = resources.ToString();
    }

    public void IncreaseResources(DragHandler destroyedUnit)
    {
        resources += destroyedUnit.unitCost;
        textComponent.text = resources.ToString();
        animator.SetTrigger("In");
        createdUnits.Remove(destroyedUnit.gameObject);
    }

    public void DecreaseResources(DragHandler createdUnit)
    {
        resources -= createdUnit.unitCost;
        textComponent.text = resources.ToString();
        animator.SetTrigger("In");
        createdUnits.Add(createdUnit.gameObject);
    }
}