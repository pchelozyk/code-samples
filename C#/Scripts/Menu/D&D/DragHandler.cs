﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class DragHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler, IDeselectHandler
{
    //перетаскиваемый объект
    public static DragHandler itemBeingDragged;

    //начальный родитель перетаскиваемого объекта
    [HideInInspector]
    public Transform startParent;
    //стоимость размещения текущего юнита 
    public int unitCost;
    //индекс отряда
    private int unitIndex;

    private Button button;
    private Image image;

    void Start()
    {
        unitIndex = transform.parent.GetSiblingIndex() + 1;

        button = this.GetComponent<Button>();
        if (image == null) image = this.GetComponent<Image>();
    }

    public int GetUnitIndex()
    {
        return unitIndex;
    }

    public Image GetImageComponent()
    {
        if (image == null) image = this.GetComponent<Image>();
        return image;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        itemBeingDragged = this;
        startParent = transform.parent;
        //извлечение перетаскиваемого объекта из начальной иерархии
        transform.SetParent(transform.root);
        image.raycastTarget = false;
        VideoManager.changeableInstance.ChangeVideo(unitIndex - 1);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (itemBeingDragged != null)
        {
            //отображение зоны уничтожения перетаскиваемых объектов
            if (startParent.tag == "ZoneSwap")
            {
                PanelDelete.instance.animator.ResetTrigger("Disable");
                PanelDelete.instance.animator.ResetTrigger("Shake");

                PanelDelete.instance.animator.SetTrigger("Enable");
                PanelDelete.instance.slotComponent.enabled = true;
            }

            //обновление позиции для перетаскиваемого объекта
            Vector3 objScreenPos = transform.position;
            Vector3 targetPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, objScreenPos.z);

            //переопределение координат для перетаскиваемого объекта
            transform.position = targetPos;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //состояние, когда метод OnEndDrag() не сможет сработать
        if (!eventData.dragging) ResetDrag();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Откл.отображение зоны уничтожения перетаскиваемых объектов
        if ((itemBeingDragged != null) && (startParent.tag == "ZoneSwap"))
        {
            PanelDelete.instance.animator.ResetTrigger("Enable");
            PanelDelete.instance.animator.ResetTrigger("Shake");

            PanelDelete.instance.animator.SetTrigger("Disable");
            PanelDelete.instance.slotComponent.enabled = false;
        }

        ResetDrag();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if ((itemBeingDragged != null) && (startParent.tag == "ZoneSwap"))
        {
            PanelDelete.instance.animator.ResetTrigger("Enable");
            PanelDelete.instance.animator.ResetTrigger("Shake");

            PanelDelete.instance.animator.SetTrigger("Disable");
            PanelDelete.instance.slotComponent.enabled = false;
        }

        ResetDrag();

        //откат анимации выделения
        button.enabled = false;
        button.enabled = true;
    }

    private void ResetDrag()
    {
        if ((transform.parent == startParent) || (transform.parent == transform.root))
        {
            transform.position = startParent.position;
            transform.SetParent(startParent);
        }

        image.raycastTarget = true;
        itemBeingDragged = null;
        startParent = null;
    }
}