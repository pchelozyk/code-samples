﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsManager : MonoBehaviour
{
    //единственный активный экземпляр класса
    private static UnitsManager instance;
    //изменяемое хранилище всех юнитов
    public GameObject[] units;
    //статическое хранилище (дублирует изменяемое хранилище)
    private static GameObject[] _units;

    private void Awake()
    {
        //инициализация активного экземпляра класса
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        _units = units;
    }

    //метод возвращает созданную копию отряда с запрашиваемым индексом
    public static GameObject GetUnitInstance(int unitIndex)
    {
        GameObject unitInstance = null;

        //метод возвращает null если хранилище не содержит отряда с таким индексом
        if ((unitIndex == 0) || (_units.Length <= unitIndex)) return unitInstance;

        //создание копии отряда с исходным именем
        unitInstance = Instantiate(_units[unitIndex - 1]);
        unitInstance.name = _units[unitIndex - 1].name;

        return unitInstance;
    }

    //метод создает экземпляр указанного отряда и присваивает его указанному объекту
    public static void CreateUnitInstance(int unitIndex, Transform parent)
    {
        //создание отряда произойдёт только если существует отряд с указанным индексом (без включения 0)
        if (unitIndex != 0 && unitIndex <= _units.Length)
        {
            //создание копии отряда с исходным именем
            GameObject unitInstance = Instantiate(_units[unitIndex - 1], parent, false);
            unitInstance.name = _units[unitIndex - 1].name;
        }
    }
}