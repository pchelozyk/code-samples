﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArmyPresetManager : MonoBehaviour
{
    //единственный активный экземпляр класса
    private static ArmyPresetManager instance;
    //индекс последнего выбраного уровня
    //0 - уровень ещё не был выбран
    public static int levelIndex = 0;
    //хранилище индексов отрядов в сформированой армии
    //индекс массива - номер слота; значение массива - индекс отряда, который находится в этом слоте
    public static int[] armyPreset = new int[25];
    //общий размер армии
    private static int armySize;

    public RectTransform content;
    public Button startButton;
    public Button cancelButton;
    public Button[] levelButton;

    public Color colorDefault;
    public Color colorError;
    public Color colorLoading;

    private Image panelImage;
    private Text panelText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        //инициализация числовых значений
        armySize = 0;
        for (int i = 0; i < armyPreset.Length; i++)
        {
            armyPreset[i] = 0;
        }

        //инициализация используемых компонентов
        panelImage = startButton.transform.parent.GetComponent<Image>();
        panelText = startButton.transform.parent.GetChild(0).GetComponent<Text>();

        //назначение событий для кнопок
        startButton.onClick.AddListener(ArmyPresetCheck);
        cancelButton.onClick.AddListener(PanelReset);
        //без промежуточной переменной temp все кнопки принимают максимальное значение i как параметр 
        for (int i = 0; i < levelButton.Length; i++)
        {
            int temp = i;
            levelButton[temp].onClick.AddListener(delegate { SetLevelIndex(temp); });
        }

        //перемещение камеры к последнему запущенному уровню
        if (levelIndex != 0)
        {
            Vector2 contentWorldPos = content.transform.position;
            RectTransform targetRect = levelButton[levelIndex - 1].transform as RectTransform;
            Vector2 contentLocalPos = targetRect.InverseTransformPoint(contentWorldPos);
            content.anchoredPosition = contentLocalPos;
        }
    }

    //метод обновления выбраного индекса уровня
    private void SetLevelIndex(int buttonIndex)
    {
        levelIndex = buttonIndex + 1;
    }

    //добавление отряда в итоговый пресет армии
    public static void AddSquad(int squadIndex, int slotIndex)
    {
        armyPreset[slotIndex] = squadIndex;
        armySize++;
    }

    //удаление отряда из итогового пресета армии
    public static void RemoveSquad(int slotIndex)
    {
        armyPreset[slotIndex] = 0;
        armySize--;
    }

    //перемещение отрядов в ячейках
    public static void SwapSquads(int parentSlotIndex, int targetSlotIndex)
    {
        int temp = armyPreset[parentSlotIndex];
        armyPreset[parentSlotIndex] = armyPreset[targetSlotIndex];
        armyPreset[targetSlotIndex] = temp;
    }

    public static void ResetArmyPreset()
    {
        for (int i = 0; i < armyPreset.Length; i++)
        {
            armyPreset[i] = 0;
        }
    }

    //возврат финальной панели в исходное состояние
    private void PanelReset()
    {
        startButton.interactable = true;
        panelImage.color = colorDefault;
        panelText.text = LocalizationManager.instance.GetLocalizedValue("start_panel_ready");
    }

    //метод выполняет загрузку уровня
    private void ArmyPresetCheck()
    {
        if (armySize < 6)
        {
            ShowError("start_panel_errcomplete");
            return;
        }

        if (levelIndex == 1)
        {
            if (DifficultySettings.currentDifficulty == 1)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 3))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 2)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 3)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
            }
        }
        else if (levelIndex == 2)
        {
            if (DifficultySettings.currentDifficulty == 1)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 2))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 2))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 2)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 3))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 3))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 3)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
            }
        }
        else if (levelIndex == 3)
        {
            if (DifficultySettings.currentDifficulty == 1)
            {
                if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 2))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 1, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(6, 2))
                {
                    ShowError("not_enough_torch");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 2)
            {
                if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 3))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 1, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(6, 2))
                {
                    ShowError("not_enough_torch");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 3)
            {
                if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 1, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(6, 4))
                {
                    ShowError("not_enough_torch");
                    return;
                }
            }
        }
        else if (levelIndex == 4)
        {
            if (LineOverflowCheck(8, 1))
            {
                ShowError("start_panel_errbridge");
                return;
            }

            if (DifficultySettings.currentDifficulty == 1)
            {
                if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 3))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 2)
            {
                if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 3))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 3)
            {
                if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 5))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
        }
        else if (levelIndex == 5)
        {
            if (LineOverflowCheck(8, 1))
            {
                ShowError("start_panel_errbridge");
                return;
            }

            if (DifficultySettings.currentDifficulty == 1)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 1, 1, 1 }, 3))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 2))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 3))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 2)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 1, 1, 1 }, 5))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 2))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 3))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
            else if (DifficultySettings.currentDifficulty == 3)
            {
                if (!SquadRequirementsCheck(2, new int[] { 1, 1, 1, 1, 1 }, 5))
                {
                    ShowError("not_enough_engineer");
                    return;
                }
                else if (!SquadRequirementsCheck(4, new int[] { 1, 1, 0, 1, 1 }, 4))
                {
                    ShowError("not_enough_ladder");
                    return;
                }
                else if (!SquadRequirementsCheck(5, new int[] { 0, 0, 2, 0, 0 }, 1))
                {
                    ShowError("not_enough_ram");
                    return;
                }
                else if (!SquadRequirementsCheck(8, new int[] { 1, 1, 1, 1, 1 }, 5))
                {
                    ShowError("not_enough_bridge");
                    return;
                }
            }
        }

        LoadLevel();
    }

    //проверка на требуемое кол-во определённого отряда в армии
    private bool SquadRequirementsCheck(int squadIndex, int[] lineRequirements, int requirementsLimit)
    {
        int squadCounter = 0;
        int requirementsCounter = 0;

        for (int i = 0; i < lineRequirements.Length; i++)
        {
            if (lineRequirements[i] == 0)
                continue;
            else
            {
                int lineStartIndex = 0 + (5 * i);
                for (int j = lineStartIndex; j < lineStartIndex + 5; j++)
                {
                    if (armyPreset[j] == squadIndex)
                        squadCounter++;
                }

                if (squadCounter >= lineRequirements[i])
                    requirementsCounter++;

                squadCounter = 0;
            }

            if (requirementsCounter == requirementsLimit)
                break;
        }

        return requirementsCounter == requirementsLimit ? true : false;
    }

    private bool SquadRequirementsCheck(int squadIndex, int requirementsLimit)
    {
        int squadCounter = 0;

        for (int i = 0; i < armyPreset.Length; i++)
        {
            if (armyPreset[i] == squadIndex)
                squadCounter++;

            if (squadCounter == requirementsLimit)
                break;
        }

        return squadCounter == requirementsLimit ? true : false;
    }

    //проверка на переполнение линии конкретным типом отрядов
    private static bool LineOverflowCheck(int squadIndex, int limitInLine)
    {
        int squadCounter = 0;

        for (int i = 0; i < armyPreset.Length; i++)
        {
            if (armyPreset[i] == squadIndex) squadCounter++;
            if (squadCounter > limitInLine) return true;

            //очистка счетчика при переходе к следующей линии
            if (((i + 1) % 5) == 0) squadCounter = 0;
        }

        return false;
    }

    private void ShowError(string errorKey)
    {
        startButton.interactable = false;
        panelImage.color = colorError;
        panelText.text = LocalizationManager.instance.GetLocalizedValue(errorKey);
    }

    private void LoadLevel()
    {
        //отображение финального состояния панели и загрузка уровня
        panelImage.color = colorLoading;
        panelText.text = LocalizationManager.instance.GetLocalizedValue("start_panel_loading");
        SceneManager.LoadScene(levelIndex);
    }
}