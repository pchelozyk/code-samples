﻿using UnityEngine;

public class ArmyPresetLoader : MonoBehaviour
{
    //единственный активный экземпляр класса
    private ArmyPresetLoader instance;
    public Transform content;

    private void Awake()
    {
        //инициализация активного экземпляра класса
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //сброс подсчета отрядов, прошедших уровень
        ZoneExit.ResetUnitCount();

        //счетчик кол-ва итераций в линии для обращения к слоту
        int stepCounter = 0;
        //номер текущей линии
        int lineNum = 1;
        //инициализация первой линии для заполнения отрядами
        Transform line = content.GetChild(lineNum);
        //временная переменная для хранения созданого отряда
        //GameObject createdSquad = null;

        for (int i = 0; i < 25; i++)
        {
            //после обработки 5 слотов - переход к следующей линии
            if (stepCounter > 4)
            {
                stepCounter = 0;
                lineNum++;
                line = content.GetChild(lineNum);
            }

            //создание отряда в слоте
            UnitsManager.CreateUnitInstance(ArmyPresetManager.armyPreset[i], line.GetChild(stepCounter));

            //увеличение счетчика итераций
            stepCounter++;
        }
    }
}