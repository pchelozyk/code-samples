﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioQueue : MonoBehaviour
{
    [Header("Список проигрываемых треков")]
    public List<AudioClip> TrackList;

    //треки в порядке проигрывания
    private static Queue<AudioClip> TrackQueue = new Queue<AudioClip>();

    private AudioSource m_audioSource;
    private bool isPaused = false;

    private void Start()
    {
        //инициализация компонента для текущего объекта
        if (m_audioSource == null) m_audioSource = GetComponent<AudioSource>();

        //состояние, когда не сформирован порядок воспроизведения треков
        if (TrackQueue.Count == 0)
        {
            //сформирование порядока воспроизведения треков
            //из предоставленного списка треков
            foreach (AudioClip track in TrackList)
            {
                TrackQueue.Enqueue(track);
            }
        }

        //запуск первого трека из очереди воспроизведения
        StartCoroutine(SetNextTrack());
    }

    public void Pause()
    {
        if (m_audioSource == null) m_audioSource = GetComponent<AudioSource>();
        m_audioSource.Pause();
        isPaused = true;
        StopAllCoroutines();
    }

    public void UnPause()
    {
        m_audioSource.UnPause();
        isPaused = false;
        StartCoroutine(SetNextTrack());
    }

    private IEnumerator SetNextTrack()
    {
        if (isPaused) yield break;

        //m_audioSource не ничего не проигрывает, когда отсутствует клип,
        //либо клип полностью проигрался 
        if (!m_audioSource.isPlaying)
        {
            //текущий трек для воспроизведения
            AudioClip curTrack = TrackQueue.Dequeue();
            //установка инициализированного трека на воспроизведение
            m_audioSource.clip = curTrack;
            m_audioSource.Play();
            //перемещение проигруемого трека в конец очереди воспроизведения
            TrackQueue.Enqueue(curTrack);

            //задержка на время проигрывания трека
            yield return new WaitForSeconds(m_audioSource.clip.length);
            StartCoroutine(SetNextTrack());
        }
        else
        {
            //инициализация времени воспроизведения текущего аудио трека
            float playbackTime = m_audioSource.time;
            //инициализация длинны текущего аудио трека
            float trackLength = m_audioSource.clip.length;

            //ожидание предполагаемого времени до конца текущего трека 
            yield return new WaitForSeconds(trackLength - playbackTime);
            StartCoroutine(SetNextTrack());
        }
    }
}