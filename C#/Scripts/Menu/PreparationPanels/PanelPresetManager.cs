﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelPresetManager : MonoBehaviour
{
    //единственный экземплр класса
    public static PanelPresetManager instance;
    //индекс последнего выбраного уровня 
    public static int levelIndex = 1;
    //oбъект PreparationPanel
    public GameObject preparationPanel;
    //панель с подсказкой
    public GameObject firstPromptSquad;
    public GameObject firstPromptPreparation;

    //кнопки выбора сложности
    public Button buttonNormal;
    public Button buttonHard;
    public Button buttonVeryHard;

    //хранилище изображений для каждого уровня
    public List<PanelPreset> PanelPresetStorage;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        buttonNormal.onClick.AddListener(SetDifficultyNormal);
        buttonHard.onClick.AddListener(SetDifficultyHard);
        buttonVeryHard.onClick.AddListener(SetDifficultyVeryHard);
    }

    private void SetDifficultyNormal()
    {
        SetPanelPreset(levelIndex, 1);
    }

    private void SetDifficultyHard()
    {
        SetPanelPreset(levelIndex, 2);
    }

    private void SetDifficultyVeryHard()
    {
        SetPanelPreset(levelIndex, 3);
    }

    //очистка PreparationPanel
    public void ClearPreparationPanel()
    {
        if (preparationPanel.transform.childCount > 0)
        {
            for (int i = 0; i < preparationPanel.transform.childCount; i++)
            {
                Destroy(preparationPanel.transform.GetChild(i).gameObject);
            }
        }
    }

    //levelNum: 1,2,3...
    //difficulty: 1,2,3
    private void SetPanelPreset(int levelNum, int difficulty)
    {
        GameObject newPanel = null;

        //создание в PreparationPanel дочерних панелей, кторые изменяются в зависимости от уровня сложности
        if (difficulty == 1) newPanel = Instantiate(PanelPresetStorage[levelNum].normalDifficultyPanel);
        if (difficulty == 2) newPanel = Instantiate(PanelPresetStorage[levelNum].hardDifficultyPanel);
        if (difficulty == 3) newPanel = Instantiate(PanelPresetStorage[levelNum].veryHardDifficultyPanel);
        newPanel.transform.SetParent(preparationPanel.transform, false);
        newPanel.SetActive(false);

        //создание в PreparationPanel дочерних панелей, кторые всегда отображаются для уровня
        //общие панели могут отсутствовать или их может быть несколько
        if (PanelPresetStorage[levelNum].commonPanelStorage.Length != 0)
        {
            int length = PanelPresetStorage[levelNum].commonPanelStorage.Length;
            for (int i = 0; i < length; i++)
            {
                newPanel = Instantiate(PanelPresetStorage[levelNum].commonPanelStorage[i]);
                newPanel.transform.SetParent(preparationPanel.transform, false);
                newPanel.SetActive(false);
            }
        }

        //активация первой панели в иерархии PreparationPanel
        preparationPanel.transform.GetChild(0).gameObject.SetActive(true);

        //активация опциональной панели с подсказками
        if ((levelNum == 0) && (difficulty == 1))
        {
            firstPromptPreparation.SetActive(true);
            firstPromptSquad.SetActive(true);
        }
        else
        {
            firstPromptPreparation.SetActive(false);
            firstPromptSquad.SetActive(false);
        }
    }
}

[System.Serializable]
public struct PanelPreset
{
    [Header("Common panels")]
    public GameObject[] commonPanelStorage;
    [Header("Unique panels")]
    public GameObject normalDifficultyPanel;
    public GameObject hardDifficultyPanel;
    public GameObject veryHardDifficultyPanel;
}