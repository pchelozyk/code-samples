﻿using UnityEngine.UI;
using UnityEngine;

public class PreparationPanelHandler : MonoBehaviour
{
    public Button rightSwitchButton;
    public Button leftSwitchButton;

    private int activePanel_ID;

    void Awake()
    {
        rightSwitchButton.onClick.AddListener(SwitchToNextPanel);
        leftSwitchButton.onClick.AddListener(SwitchToPreviousPanel);

        activePanel_ID = 0;
    }

    private void SwitchToNextPanel()
    {
        transform.GetChild(activePanel_ID).gameObject.SetActive(false);

        if (activePanel_ID == (transform.childCount - 1))
        {
            activePanel_ID = 0;
        }
        else activePanel_ID += 1;

        transform.GetChild(activePanel_ID).gameObject.SetActive(true);
    }

    private void SwitchToPreviousPanel()
    {
        transform.GetChild(activePanel_ID).gameObject.SetActive(false);

        if (activePanel_ID == 0)
        {
            activePanel_ID = transform.childCount - 1;
        }
        else activePanel_ID -= 1;

        transform.GetChild(activePanel_ID).gameObject.SetActive(true);
    }
}