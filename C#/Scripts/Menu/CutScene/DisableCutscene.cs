﻿using UnityEngine;
using UnityEngine.UI;

public class DisableCutscene : MonoBehaviour
{
    public int сutsceneIndex;
    private AudioQueue customAudioPlayer;

    private void Start()
    {
        customAudioPlayer = Camera.main.transform.GetChild(0).GetComponent<AudioQueue>();
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        Save m_save = SaveSystem.LoadProgress();
        m_save.watchedCutScene[сutsceneIndex - 1] = true;
        SaveSystem.SaveProgress(m_save);

        customAudioPlayer.UnPause();
    }
}