﻿using UnityEngine;

public class EnableCutscene : MonoBehaviour
{
    public GameObject[] cutscene;
    public GameObject[] cutsceneImage;

    private AudioQueue customAudioPlayer;

    private void Awake()
    {
        customAudioPlayer = Camera.main.transform.GetChild(0).GetComponent<AudioQueue>();

        Save m_save = SaveSystem.LoadProgress();

        //состояние, когда 1-ая кат-сцена не просмотрена
        if (m_save.watchedCutScene[0] == false)
        {
            //включение 1-ой кат сцены
            cutscene[0].SetActive(true);
            customAudioPlayer.Pause();
        }

        //состояние, когда 2-ая кат-сцена не просмотрена и доступен 3-ий уровень
        if ((m_save.watchedCutScene[1] == false) && (m_save.availableLevels[3] == true))
        {
            //включение 2-ой кат сцены
            cutscene[1].SetActive(true);
            customAudioPlayer.Pause();
        }

        //состояние, когда 3-я кат-сцена не просмотрена и 5-ый уровень пройден на нормальной сложности
        if ((m_save.watchedCutScene[2] == false) && (m_save.availableLevels[4] == true) && (m_save.availableDifficulty[4] == 2))
        {
            //включение 3-ей кат-сцены
            cutscene[2].SetActive(true);
            customAudioPlayer.Pause();
        }

        //состояние, когда 1-ая кат-сцена просмотрена
        if (m_save.watchedCutScene[0] == true)
        {
            //отключение 1-ой кат-сцены
            cutscene[0].SetActive(false);
        }

        //состояние, когда 2-ая кат-сцена просмотрена
        if (m_save.watchedCutScene[1] == true)
        {
            //отключение 2-ой кат-сцены
            cutscene[1].SetActive(false);
            cutsceneImage[0].SetActive(true);
        }

        //состояние, когда 3-я кат-сцена просмотрена
        if (m_save.watchedCutScene[2] == true)
        {
            //отключение 3-ей кат-сцены
            cutscene[2].SetActive(false);
            cutsceneImage[1].SetActive(true);
        }
    }
}