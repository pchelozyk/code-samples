﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEditor;

public class VideoManager : MonoBehaviour
{
    public bool dynamicAppearance;
    [Range(0.001f, 1)]
    public float step;

    private VideoPlayer videoPlayer;
    private RawImage rawImage;
    private Button pauseButton;
    private Image pauseButtonImage;
    private Animator attachedAnimator;
    private Text priceText;
    private Text staminaText;

    public bool isChangeable;
    public static VideoManager changeableInstance;
    private int lastItemDraggedIndex;
    public VideoClip[] videoContainer;

    private void Awake()
    {
        videoPlayer = this.GetComponent<VideoPlayer>();
        rawImage = this.GetComponent<RawImage>();

        if (transform.GetChild(0).childCount > 0)
        {
            pauseButton = transform.GetChild(0).GetComponent<Button>();
            pauseButton.onClick.AddListener(ChangeState);
            pauseButtonImage = transform.GetChild(0).GetChild(0).GetComponent<Image>();
        }

        videoPlayer.playOnAwake = false;
        videoPlayer.waitForFirstFrame = false;

        if (isChangeable && (changeableInstance == null)) changeableInstance = this;
        if (changeableInstance == this)
        {
            attachedAnimator = this.GetComponent<Animator>();
            priceText = transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Text>();
            staminaText = transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>();
        }

        lastItemDraggedIndex = -1;
    }

    private void ToDefaultState()
    {
        rawImage.color = dynamicAppearance ? new Color(1, 1, 1, 0) : Color.black;
    }

    private void ResetButton()
    {
        if (pauseButtonImage != null)
            pauseButtonImage.color = new Vector4(1, 1, 1, 0);
    }

    private void ChangeState()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
            pauseButtonImage.color = new Vector4(1, 1, 1, 0.7f);
        }
        else
        {
            videoPlayer.Play();
            pauseButtonImage.color = new Vector4(1, 1, 1, 0);
        }
    }

    private void OnEnable()
    {
        ToDefaultState();
        videoPlayer.Play();
        StartCoroutine(VideoAppearance());
    }

    private void OnDisable()
    {
        if (!this.gameObject.activeInHierarchy) lastItemDraggedIndex = -1;
        ResetButton();
        StopAllCoroutines();
    }

    private IEnumerator VideoAppearance()
    {
        bool isReady = false;
        bool _dynamicAppearance = dynamicAppearance;

        while (!isReady)
        {
            if (videoPlayer.isPrepared && (videoPlayer.frame > 1))
            {
                isReady = true;
            }
            else yield return null;
        }

        if (_dynamicAppearance)
        {
            while (rawImage.color != Color.white)
            {
                rawImage.color += new Color(0, 0, 0, step);
                yield return null;
            }
        }
        else rawImage.color = Color.white;
    }

    public void ChangeVideo(int squadIndex)
    {
        if (this != changeableInstance) return;

        if (lastItemDraggedIndex == -1)
        {
            lastItemDraggedIndex = squadIndex;
            attachedAnimator.SetBool("AnimationLoop", true);
        }
        else if ((lastItemDraggedIndex != -1) && (lastItemDraggedIndex != squadIndex))
        {
            lastItemDraggedIndex = squadIndex;
            attachedAnimator.SetTrigger("Out");
        }
        else return;

        switch (squadIndex)
        {
            case 0:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "1";
                staminaText.text = "2";
                break;
            case 1:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "1";
                staminaText.text = "1";
                break;
            case 2:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "1";
                staminaText.text = "2";
                break;
            case 3:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "2";
                staminaText.text = "1";
                break;
            case 4:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "2";
                staminaText.text = "1";
                break;
            case 5:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "1";
                staminaText.text = "1";
                break;
            case 6:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "1";
                staminaText.text = "2";
                break;
            case 7:
                videoPlayer.clip = videoContainer[squadIndex];
                priceText.text = "2";
                staminaText.text = "1";
                break;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(VideoManager))]
public class VideoManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Undo.RecordObject(target, "VideoManager update");
        var videoManager = target as VideoManager;

        videoManager.dynamicAppearance = GUILayout.Toggle(videoManager.dynamicAppearance, "Dynamic appearance");
        if (videoManager.dynamicAppearance)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Step");
            videoManager.step = EditorGUILayout.Slider(videoManager.step, 0.001f, 1);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }

        videoManager.isChangeable = GUILayout.Toggle(videoManager.isChangeable, "Changeable");
        if (videoManager.isChangeable)
        {
            EditorGUI.indentLevel++;
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("videoContainer"), true);
            serializedObject.ApplyModifiedProperties();
            EditorGUI.indentLevel--;
        }
    }
}
#endif