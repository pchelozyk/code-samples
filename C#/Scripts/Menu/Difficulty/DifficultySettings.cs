﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DifficultySettings : MonoBehaviour, IPointerDownHandler
{

	//уровень сложности для текущего уровня
	//это значение передается другим скриптам
    public static int currentDifficulty;

	//уровень сложности для конкретной кнопки
    public int difficulty;

    public void OnPointerDown(PointerEventData eventData)
    {
		//инициализация текущего уровня сложности
        currentDifficulty = difficulty;
    }
}
