﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DifficultyButtonsUpdate : MonoBehaviour
{
    //номер текущего уровня
    private int levelNum = 0;

    //кнопки выбора сложности уровня
    Button btnNormal;
    Button btnHard;
    Button btnVeryHard;

    void Start()
    {
        //инициализация события для кнопки объекта
        Button btn = transform.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        //инициализация номера уровня для каждой кнопки 
        //происходит только в первом по порядку объекте из иерархии
        //т.к. он инициализирует параметры для остальных объектов из иерархии
        if (levelNum == 0)
        {
            GameObject buttonsMap = transform.parent.gameObject;
            for (int i = 0; i < buttonsMap.transform.childCount; i++)
            {
                buttonsMap.transform.GetChild(i).GetComponent<DifficultyButtonsUpdate>().levelNum = i + 1;
            }
        }

        //инициализация кнопок выбора сложности
        btnNormal = transform.root.GetChild(2).GetChild(3).GetComponent<Button>();
        btnHard = transform.root.GetChild(2).GetChild(4).GetComponent<Button>();
        btnVeryHard = transform.root.GetChild(2).GetChild(5).GetComponent<Button>();
    }

    void TaskOnClick()
    {
        //нормальный уровень сложности всегда доступен для открытых уровней
        //откат кнопок до исходного состояния перед возможным включением
        btnNormal.interactable = true;
        btnHard.interactable = false;
        btnVeryHard.interactable = false;

        //загрузка текущего прогресса
        Save m_save = new Save();
        m_save = SaveSystem.LoadProgress();

        //сложный и оч. сложный уровни становятся доступными только после прохождения предыдущих
        if (m_save.availableDifficulty[levelNum - 1] == 2) btnHard.interactable = true;
        if (m_save.availableDifficulty[levelNum - 1] == 3)
        {
            btnHard.interactable = true;
            btnVeryHard.interactable = true;
        }
    }
}
