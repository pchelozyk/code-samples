﻿using UnityEngine;
using UnityEngine.UI;

public class SquadsActivation : MonoBehaviour
{
    public bool shieldActive;
    public bool engineerActive;
    public bool archerActive;
    public bool stairsActive;
    public bool ramActive;
    public bool torchActive;
    public bool spearActive;
    public bool bridgeActive;

    public GameObject content;

    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        PanelPresetManager.levelIndex = transform.GetSiblingIndex();

        int i = 0;
        foreach (Transform slot in content.transform)
        {
            switch (i)
            {
                case 0:
                    slot.gameObject.SetActive(shieldActive);
                    break;
                case 1:
                    slot.gameObject.SetActive(engineerActive);
                    break;
                case 2:
                    slot.gameObject.SetActive(archerActive);
                    break;
                case 3:
                    slot.gameObject.SetActive(stairsActive);
                    break;
                case 4:
                    slot.gameObject.SetActive(ramActive);
                    break;
                case 5:
                    slot.gameObject.SetActive(torchActive);
                    break;
                case 6:
                    slot.gameObject.SetActive(spearActive);
                    break;
                case 7:
                    slot.gameObject.SetActive(bridgeActive);
                    break;
                default:
                    slot.gameObject.SetActive(false);
                    break;
            }

            i++;
        }
    }
}