﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class SmoothTransition : MonoBehaviour
{
    public RectTransform content;
    private Vector3 velocity = Vector3.zero;
    private float smoothTime = 0.4F;

    private void Start()
    {
        Vector3 targetPos = new Vector3(-372, 1409, 0);
        this.GetComponent<Button>().onClick.AddListener(delegate { StartAnimation(targetPos); });
    }

    private IEnumerator SmoothAnimation(Vector3 targetPos)
    {
        while (Vector3.Distance(content.localPosition, targetPos) > 50 && !Input.GetMouseButtonDown(0))
        {
            content.localPosition = Vector3.SmoothDamp(content.localPosition, targetPos, ref velocity, smoothTime);
            yield return null;
        }
    }

    private void StartAnimation(Vector3 targetPos)
    {
        StopAllCoroutines();
        StartCoroutine(SmoothAnimation(targetPos));
    }

    private void Update()
    {
        if (Input.GetKeyUp("escape")) StartAnimation(Vector3.zero);
    }
}