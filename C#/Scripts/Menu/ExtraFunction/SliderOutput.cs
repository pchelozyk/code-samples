﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderOutput : MonoBehaviour
{
    private Slider attachedSlider;
    private Text attachedText;

    private void OnEnable()
    {
        if (attachedSlider == null)
        {
            attachedSlider = GetComponent<Slider>();
            attachedText = transform.GetChild(3).GetComponent<Text>();
            attachedSlider.onValueChanged.AddListener(delegate { UpdateValue(); });
        }

        UpdateValue();
    }

    private void UpdateValue()
    {
        attachedText.text = $"{Mathf.RoundToInt(attachedSlider.normalizedValue * 100)}%";
    }
}