﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonsManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public static Animator selectedAnimator;
    public bool highlightedOnEnable;
    private Button attachedButton;
    private Animator attachedAnimator;

    private void Awake()
    {
        attachedButton = this.GetComponent<Button>();
        attachedAnimator = this.GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if (!attachedButton.interactable) attachedAnimator.SetTrigger("Disabled");

        if (highlightedOnEnable)
        {
            attachedAnimator.SetTrigger("Highlighted");
            selectedAnimator = attachedAnimator;
        }
    }

    private void OnDisable()
    {
        selectedAnimator = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (attachedButton.interactable && (selectedAnimator != attachedAnimator))
        {
            selectedAnimator.SetTrigger("Normal");
            selectedAnimator = attachedAnimator;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (attachedButton.interactable && (selectedAnimator != attachedAnimator))
        {
            attachedAnimator.ResetTrigger("Normal");
            attachedAnimator.SetTrigger("Highlighted");
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (attachedButton.interactable && (selectedAnimator != attachedAnimator))
        {
            attachedAnimator.ResetTrigger("Highlighted");
            attachedAnimator.SetTrigger("Normal");
        }
    }
}