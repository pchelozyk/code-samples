﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMainMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Dropdown screenResolutionDropdown;
    public Dropdown screenModeDropdown;
    public Dropdown graphicQualityDropdown;
    public Dropdown vSyncDropdown;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider effectsVolumeSlider;
    public Slider ambientVolumeSlider;
    public Slider cameraSpeedSlider;
    public Slider cameraZoomSpeedSlider;
    public Toggle[] cameraModeToggle = new Toggle[2];
    public Button saveButton;
    public Button resetButton;

    private Settings activeSettings;
    private Settings defaultSettings;

    private List<Resolution> uniqResolutions;

    private void Start()
    {
        uniqResolutions = new List<Resolution>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Resolution temp = Screen.resolutions[i];
            temp.refreshRate = 0;

            if (!uniqResolutions.Contains(temp))
                uniqResolutions.Add(temp);
        }

        activeSettings = SettingsSystem.LoadSettings();
        ApplySettings(activeSettings);
        StartCoroutine(PrepareSettingsPanel(activeSettings));

        defaultSettings = GenerateDefaultSettings();

        GetComponent<Button>().onClick.AddListener(ResetSettingsPanel);
        saveButton.onClick.AddListener(SaveSettings);
        resetButton.onClick.AddListener(ResetSettings);

        screenResolutionDropdown.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        screenModeDropdown.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        graphicQualityDropdown.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        vSyncDropdown.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        masterVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        musicVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        effectsVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        ambientVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraSpeedSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraZoomSpeedSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraModeToggle[0].onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraModeToggle[1].onValueChanged.AddListener(delegate { UpdateSaveButton(); });
    }

    private void ApplySettings(Settings settings)
    {
        //применение настроек графики
        int screenResolutionIndex = uniqResolutions.Count > settings.screenResolutionIndex ?
                                                            settings.screenResolutionIndex :
                                                            uniqResolutions.Count - 1;

        if (Screen.width != uniqResolutions[screenResolutionIndex].width
            || Screen.height != uniqResolutions[screenResolutionIndex].height
            || Screen.fullScreen != settings.fullScreen)
        {
            Screen.SetResolution(uniqResolutions[screenResolutionIndex].width, 
                                 uniqResolutions[screenResolutionIndex].height, 
                                 settings.fullScreen);
        }

        if (QualitySettings.GetQualityLevel() != settings.qualityIndex)
            QualitySettings.SetQualityLevel(settings.qualityIndex);

        if (QualitySettings.vSyncCount != settings.vSync)
            QualitySettings.vSyncCount = settings.vSync;

        //применение настроек звука
        audioMixer.SetFloat("masterVol", settings.masterVolume);
        audioMixer.SetFloat("musicVol", settings.musicVolume);
        audioMixer.SetFloat("effectVol", settings.effectsVolume);
        audioMixer.SetFloat("backgraundVol", settings.ambientVolume);
    }

    private IEnumerator PrepareSettingsPanel(Settings settings)
    {
        while (!LocalizationManager.instance.GetIsReady())
        {
            yield return null;
        }

        List<string> options = new List<string>();

        //отображение списка разрешений экрана
        screenResolutionDropdown.ClearOptions();
        for (int i = 0; i < uniqResolutions.Count; i++)
        {
            string option = $"{uniqResolutions[i].width} x {uniqResolutions[i].height}";
            options.Add(option);
        }
        screenResolutionDropdown.AddOptions(options);
        screenResolutionDropdown.value = uniqResolutions.Count > settings.screenResolutionIndex ?
                                         settings.screenResolutionIndex : uniqResolutions.Count - 1;
        screenResolutionDropdown.RefreshShownValue();

        //отображение списка состояний окна
        options.Clear();
        options.Add(LocalizationManager.instance.GetLocalizedValue("screen_mode_0"));
        options.Add(LocalizationManager.instance.GetLocalizedValue("screen_mode_1"));
        screenModeDropdown.ClearOptions();
        screenModeDropdown.AddOptions(options);
        screenModeDropdown.value = settings.fullScreen ? 0 : 1;
        screenModeDropdown.RefreshShownValue();

        //отображение списка качества графики
        options.Clear();
        options.Add(LocalizationManager.instance.GetLocalizedValue("graphic_quality_0"));
        options.Add(LocalizationManager.instance.GetLocalizedValue("graphic_quality_1"));
        options.Add(LocalizationManager.instance.GetLocalizedValue("graphic_quality_2"));
        graphicQualityDropdown.ClearOptions();
        graphicQualityDropdown.AddOptions(options);
        graphicQualityDropdown.value = settings.qualityIndex;
        screenModeDropdown.RefreshShownValue();

        //отображение списка опций вертикальной синхронизации
        options.Clear();
        options.Add(LocalizationManager.instance.GetLocalizedValue("v_sync_option_0"));
        options.Add(LocalizationManager.instance.GetLocalizedValue("v_sync_option_1"));
        vSyncDropdown.ClearOptions();
        vSyncDropdown.AddOptions(options);
        vSyncDropdown.value = settings.vSync == 0 ? 0 : 1;
        vSyncDropdown.RefreshShownValue();

        //отображение текущих настроек звука
        masterVolumeSlider.value = settings.masterVolume;
        musicVolumeSlider.value = settings.musicVolume;
        effectsVolumeSlider.value = settings.effectsVolume;
        ambientVolumeSlider.value = settings.ambientVolume;

        //отображение текущих настроек игрового процесса
        cameraSpeedSlider.value = settings.cameraSpeed;
        cameraZoomSpeedSlider.value = settings.cameraZoomSpeed;
        if (settings.alternativeCameraControl)
            cameraModeToggle[1].isOn = true;
        else
            cameraModeToggle[0].isOn = true;
    }

    private void ResetSettingsPanel()
    {
        Settings inputSettings = GenerateCurrentSettings();

        //исходное состояние кнопоки Reset
        if (resetButton.interactable && activeSettings.Equals(defaultSettings))
            resetButton.interactable = false;
        else if (!resetButton.interactable && !activeSettings.Equals(defaultSettings))
            resetButton.interactable = true;

        //исходное состояние кнопоки Save
        saveButton.interactable = false;

        if (!inputSettings.Equals(activeSettings))
        {
            if (inputSettings.screenResolutionIndex != activeSettings.screenResolutionIndex)
            {
                screenResolutionDropdown.value = activeSettings.screenResolutionIndex;
                screenResolutionDropdown.RefreshShownValue();
            }

            if (inputSettings.fullScreen != activeSettings.fullScreen)
            {
                if (activeSettings.fullScreen)
                    screenModeDropdown.value = 0;
                else
                    screenModeDropdown.value = 1;
                screenModeDropdown.RefreshShownValue();
            }

            if (inputSettings.qualityIndex != activeSettings.qualityIndex)
            {
                graphicQualityDropdown.value = activeSettings.qualityIndex;
                graphicQualityDropdown.RefreshShownValue();
            }

            if (inputSettings.vSync != activeSettings.vSync)
            {
                vSyncDropdown.value = activeSettings.vSync == 0 ? 0 : 1;
                vSyncDropdown.RefreshShownValue();
            }

            if (inputSettings.masterVolume != activeSettings.masterVolume)
                masterVolumeSlider.value = activeSettings.masterVolume;

            if (inputSettings.musicVolume != activeSettings.musicVolume)
                musicVolumeSlider.value = activeSettings.musicVolume;

            if (inputSettings.effectsVolume != activeSettings.effectsVolume)
                effectsVolumeSlider.value = activeSettings.effectsVolume;

            if (inputSettings.ambientVolume != activeSettings.ambientVolume)
                ambientVolumeSlider.value = activeSettings.ambientVolume;

            if (inputSettings.cameraSpeed != activeSettings.cameraSpeed)
                cameraSpeedSlider.value = activeSettings.cameraSpeed;

            if (inputSettings.cameraZoomSpeed != activeSettings.cameraZoomSpeed)
                cameraZoomSpeedSlider.value = activeSettings.cameraZoomSpeed;

            if (inputSettings.alternativeCameraControl != activeSettings.alternativeCameraControl)
            {
                if (activeSettings.alternativeCameraControl)
                    cameraModeToggle[1].isOn = true;
                else
                    cameraModeToggle[0].isOn = true;
            }
        }
    }

    private void SaveSettings()
    {
        Settings inputSettings = GenerateCurrentSettings();
        activeSettings = inputSettings;
        SettingsSystem.SaveSettings(inputSettings);
        ApplySettings(inputSettings);

        //исходное состояние кнопоки Reset
        if (resetButton.interactable && activeSettings.Equals(defaultSettings))
            resetButton.interactable = false;
        else if (!resetButton.interactable && !activeSettings.Equals(defaultSettings))
            resetButton.interactable = true;

        saveButton.interactable = false;
    }

    private void ResetSettings()
    {
        activeSettings = defaultSettings;
        SettingsSystem.SaveSettings(defaultSettings);
        ApplySettings(defaultSettings);
        ResetSettingsPanel();
    }

    private Settings GenerateCurrentSettings()
    {
        Settings _settings = new Settings();

        _settings.screenResolutionIndex = screenResolutionDropdown.value;
        _settings.fullScreen = screenModeDropdown.value == 0 ? true : false;
        _settings.qualityIndex = graphicQualityDropdown.value;
        _settings.vSync = vSyncDropdown.value;
        _settings.masterVolume = masterVolumeSlider.value;
        _settings.musicVolume = musicVolumeSlider.value;
        _settings.effectsVolume = effectsVolumeSlider.value;
        _settings.ambientVolume = ambientVolumeSlider.value;
        _settings.cameraSpeed = cameraSpeedSlider.value;
        _settings.cameraZoomSpeed = cameraZoomSpeedSlider.value;
        _settings.alternativeCameraControl = cameraModeToggle[1].isOn ? true : false;

        return _settings;
    }

    private Settings GenerateDefaultSettings()
    {
        Settings _defaultSettings = new Settings();
        _defaultSettings.screenResolutionIndex = uniqResolutions.Count - 1;
        return _defaultSettings;
    }

    public void UpdateSaveButton()
    {
        Settings inputSettings = GenerateCurrentSettings();

        if (saveButton.interactable && inputSettings.Equals(activeSettings))
            saveButton.interactable = false;
        else if (!saveButton.interactable && !inputSettings.Equals(activeSettings))
            saveButton.interactable = true;
    }
}