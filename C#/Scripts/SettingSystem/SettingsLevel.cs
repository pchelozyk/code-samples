﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsLevel : MonoBehaviour
{
    public CamDrag camDrag;
    public PlusCam plusCam;
    public AudioMixer audioMixer;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider effectsVolumeSlider;
    public Slider ambientVolumeSlider;
    public Slider cameraSpeedSlider;
    public Slider cameraZoomSpeedSlider;
    public Toggle[] cameraModeToggle = new Toggle[2];
    public Button optionsButton;
    public Button saveButton;
    public Button resetButton;

    private Settings activeSettings;
    private Settings defaultSettings;

    private void Start()
    {
        activeSettings = SettingsSystem.LoadSettings();
        ApplySettings(activeSettings);
        defaultSettings = GenerateDefaultSettings(activeSettings);

        optionsButton.onClick.AddListener(ResetSettingsPanel);
        saveButton.onClick.AddListener(SaveSettings);
        resetButton.onClick.AddListener(ResetSettings);

        masterVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        musicVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        effectsVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        ambientVolumeSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraSpeedSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraZoomSpeedSlider.onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraModeToggle[0].onValueChanged.AddListener(delegate { UpdateSaveButton(); });
        cameraModeToggle[1].onValueChanged.AddListener(delegate { UpdateSaveButton(); });
    }

    private void ApplySettings(Settings settings)
    {
        //применение настроек звука
        audioMixer.SetFloat("masterVol", settings.masterVolume);
        audioMixer.SetFloat("musicVol", settings.musicVolume);
        audioMixer.SetFloat("effectVol", settings.effectsVolume);
        audioMixer.SetFloat("backgraundVol", settings.ambientVolume);

        //применение настроек игрового процесса
        camDrag.panSpeed = settings.cameraSpeed;
        plusCam.SpeedScroll = settings.cameraZoomSpeed;
        camDrag.secondCamDrag = settings.alternativeCameraControl;
    }

    private void ResetSettingsPanel()
    {
        Settings inputSettings = GenerateCurrentSettings();

        //исходное состояние кнопоки Reset
        if (resetButton.interactable && activeSettings.Equals(defaultSettings))
            resetButton.interactable = false;
        else if (!resetButton.interactable && !activeSettings.Equals(defaultSettings))
            resetButton.interactable = true;

        //исходное состояние кнопоки Save
        saveButton.interactable = false;

        if (!inputSettings.Equals(activeSettings))
        {
            if (inputSettings.masterVolume != activeSettings.masterVolume)
                masterVolumeSlider.value = activeSettings.masterVolume;

            if (inputSettings.musicVolume != activeSettings.musicVolume)
                musicVolumeSlider.value = activeSettings.musicVolume;

            if (inputSettings.effectsVolume != activeSettings.effectsVolume)
                effectsVolumeSlider.value = activeSettings.effectsVolume;

            if (inputSettings.ambientVolume != activeSettings.ambientVolume)
                ambientVolumeSlider.value = activeSettings.ambientVolume;

            if (inputSettings.cameraSpeed != activeSettings.cameraSpeed)
                cameraSpeedSlider.value = activeSettings.cameraSpeed;

            if (inputSettings.cameraZoomSpeed != activeSettings.cameraZoomSpeed)
                cameraZoomSpeedSlider.value = activeSettings.cameraZoomSpeed;

            if (inputSettings.alternativeCameraControl != activeSettings.alternativeCameraControl)
            {
                if (activeSettings.alternativeCameraControl)
                    cameraModeToggle[1].isOn = true;
                else
                    cameraModeToggle[0].isOn = true;
            }
        }
    }

    private void SaveSettings()
    {
        Settings inputSettings = GenerateCurrentSettings();
        activeSettings = inputSettings;
        SettingsSystem.SaveSettings(inputSettings);
        ApplySettings(inputSettings);

        //исходное состояние кнопоки Reset
        if (resetButton.interactable && activeSettings.Equals(defaultSettings))
            resetButton.interactable = false;
        else if (!resetButton.interactable && !activeSettings.Equals(defaultSettings))
            resetButton.interactable = true;

        saveButton.interactable = false;
    }

    private void ResetSettings()
    {
        //обновление настроек без учета параметров графики
        activeSettings.masterVolume = defaultSettings.masterVolume;
        activeSettings.musicVolume = defaultSettings.musicVolume;
        activeSettings.effectsVolume = defaultSettings.effectsVolume;
        activeSettings.ambientVolume = defaultSettings.ambientVolume;
        activeSettings.cameraSpeed = defaultSettings.cameraSpeed;
        activeSettings.cameraZoomSpeed = defaultSettings.cameraZoomSpeed;
        activeSettings.alternativeCameraControl = defaultSettings.alternativeCameraControl;

        //применение обновленных настроек
        SettingsSystem.SaveSettings(activeSettings);
        ApplySettings(activeSettings);
        ResetSettingsPanel();
    }

    private Settings GenerateCurrentSettings()
    {
        Settings _settings = new Settings();
        _settings.screenResolutionIndex = activeSettings.screenResolutionIndex;
        _settings.fullScreen = activeSettings.fullScreen;
        _settings.qualityIndex = activeSettings.qualityIndex;
        _settings.vSync = activeSettings.vSync;
        _settings.masterVolume = masterVolumeSlider.value;
        _settings.musicVolume = musicVolumeSlider.value;
        _settings.effectsVolume = effectsVolumeSlider.value;
        _settings.ambientVolume = ambientVolumeSlider.value;
        _settings.cameraSpeed = cameraSpeedSlider.value;
        _settings.cameraZoomSpeed = cameraZoomSpeedSlider.value;
        _settings.alternativeCameraControl = cameraModeToggle[1].isOn ? true : false;

        return _settings;
    }

    private Settings GenerateDefaultSettings(Settings _activeSettings)
    {
        Settings _defaultSettings = new Settings();
        _defaultSettings.screenResolutionIndex = _activeSettings.screenResolutionIndex;
        _defaultSettings.fullScreen = _activeSettings.fullScreen;
        _defaultSettings.qualityIndex = _activeSettings.qualityIndex;
        _defaultSettings.vSync = _activeSettings.vSync;
        return _defaultSettings;
    }

    public void UpdateSaveButton()
    {
        Settings inputSettings = GenerateCurrentSettings();

        if (saveButton.interactable && inputSettings.Equals(activeSettings))
            saveButton.interactable = false;
        else if (!saveButton.interactable && !inputSettings.Equals(activeSettings))
            saveButton.interactable = true;
    }
}