﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class SettingsSystem
{
    public static void SaveSettings(Settings settings)
    {
        string directoryPath = Path.Combine(Application.persistentDataPath, "Options");
        string filePath = Path.Combine(directoryPath, "Settings.json");

        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);

        File.WriteAllText(filePath, JsonUtility.ToJson(settings));
    }

    public static Settings LoadSettings()
    {
        //загрузка исходного сета настроек
        Settings m_settings = new Settings();

        //корректировка исходного сета настроек
        List<Resolution> uniqResolutions = new List<Resolution>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Resolution temp = Screen.resolutions[i];
            temp.refreshRate = 0;

            if (!uniqResolutions.Contains(temp))
                uniqResolutions.Add(temp);
        }
        m_settings.screenResolutionIndex = uniqResolutions.Count - 1;

        string directoryPath = Path.Combine(Application.persistentDataPath, "Options");
        string filePath = Path.Combine(directoryPath, "Settings.json");

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
        else if (File.Exists(filePath))
        {
            m_settings = JsonUtility.FromJson<Settings>(File.ReadAllText(filePath));
        }

        return m_settings;
    }
}

[Serializable]
public class Settings
{
    //графика
    public int screenResolutionIndex;
    public bool fullScreen;
    public int qualityIndex;
    public int vSync;

    //звук
    public float masterVolume;
    public float musicVolume;
    public float effectsVolume;
    public float ambientVolume;

    //настройки управления
    public float cameraSpeed;
    public float cameraZoomSpeed;
    public bool alternativeCameraControl;

    //исходный набор настроек
    public Settings()
    {
        screenResolutionIndex = 0;
        fullScreen = true;
        qualityIndex = 2;
        vSync = 1;

        masterVolume = 10;
        musicVolume = -33;
        effectsVolume = -13;
        ambientVolume = -18;

        cameraSpeed = 0.4f;
        cameraZoomSpeed = 50;
        alternativeCameraControl = true;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Settings s = obj as Settings;
        if ((object)s == null)
        {
            return false;
        }

        return (screenResolutionIndex == s.screenResolutionIndex) && (fullScreen == s.fullScreen)
            && (qualityIndex == s.qualityIndex) && (vSync == s.vSync)
            && (masterVolume == s.masterVolume) && (musicVolume == s.musicVolume)
            && (effectsVolume == s.effectsVolume) && (ambientVolume == s.ambientVolume)
            && (cameraSpeed == s.cameraSpeed) && (cameraZoomSpeed == s.cameraZoomSpeed)
            && (alternativeCameraControl == s.alternativeCameraControl);
    }

    public bool Equals(Settings s)
    {
        if ((object)s == null)
        {
            return false;
        }

        return (screenResolutionIndex == s.screenResolutionIndex) && (fullScreen == s.fullScreen)
            && (qualityIndex == s.qualityIndex) && (vSync == s.vSync)
            && (masterVolume == s.masterVolume) && (musicVolume == s.musicVolume)
            && (effectsVolume == s.effectsVolume) && (ambientVolume == s.ambientVolume)
            && (cameraSpeed == s.cameraSpeed) && (cameraZoomSpeed == s.cameraZoomSpeed)
            && (alternativeCameraControl == s.alternativeCameraControl);
    }

    public override int GetHashCode()
    {
        return screenResolutionIndex ^ fullScreen.GetHashCode() ^ qualityIndex ^ vSync
            ^ masterVolume.GetHashCode() ^ musicVolume.GetHashCode() ^ effectsVolume.GetHashCode()
            ^ ambientVolume.GetHashCode() ^ cameraSpeed.GetHashCode() ^ cameraZoomSpeed.GetHashCode()
            ^ alternativeCameraControl.GetHashCode();
    }
}