﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class LocalizedText : MonoBehaviour
{
    public static List<LocalizedText> allInstanceList = new List<LocalizedText>();
    public string key;
    private Text attachedText;

    private void Awake()
    {
        allInstanceList.Add(this);
        if (attachedText == null) attachedText = this.GetComponent<Text>();
    }

    private IEnumerator Start()
    {
        while (!LocalizationManager.instance.GetIsReady())
        {
            yield return null;
        }

        attachedText.text = LocalizationManager.instance.GetLocalizedValue(key);
    }

    public void SetKey(string m_key)
    {
        key = m_key;

        if (attachedText == null) attachedText = this.GetComponent<Text>();
        attachedText.text = LocalizationManager.instance.GetLocalizedValue(key);
    }

    public void SetText(string value)
    {
        attachedText.text = value;
    }

    private void OnDestroy()
    {
        allInstanceList.Clear();
    }
}