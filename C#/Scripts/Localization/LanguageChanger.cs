﻿using UnityEngine;
using UnityEngine.UI;

public class LanguageChanger : MonoBehaviour
{
    public string[] languageKeys;
    private Dropdown dropDown;

    private void Start()
    {
        dropDown = this.GetComponent<Dropdown>();
        dropDown.onValueChanged.AddListener(delegate { ChangeLanguage(); });

        if (PlayerPrefs.HasKey("Language"))
        {
            string curKey = PlayerPrefs.GetString("Language");
            int i = 0;

            foreach (string key in languageKeys)
            {
                if (key == curKey)
                {
                    dropDown.value = i;

                    return;
                }

                i++;
            }
        }

        dropDown.value = 0;
    }

    public void ChangeLanguage()
    {
        int langIndex = dropDown.value;
        string fileName = languageKeys[langIndex] + ".json";
        LocalizationManager.instance.LoadLocalizedText(fileName);
    }
}