﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;
using System.IO;
using Steamworks;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;

    private readonly string[,] localizationRef = new string[,] { { "english", "EN.json" },
                                                                 { "russian", "RU.json" } };

    private Dictionary<string, string> localizedText;
    private string missingTextString = "Localized text not found";

    private bool isReady = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        string directoryPath = Path.Combine(Application.persistentDataPath, "Localization");

        if (PlayerPrefs.HasKey("Version"))
        {
            string trackedVersion = PlayerPrefs.GetString("Version");

            if (trackedVersion != Application.version)
            {
                PlayerPrefs.SetString("Version", Application.version);

                if (Directory.Exists(directoryPath))
                    Directory.Delete(directoryPath, true);
            }
        }
        else
        {
            PlayerPrefs.SetString("Version", Application.version);

            if (Directory.Exists(directoryPath))
                Directory.Delete(directoryPath, true);
        }

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);

            string preferredLanguage = SteamApps.GetCurrentGameLanguage();
            bool isSynchronized = false;

            for (int i = localizationRef.GetLength(0) - 1; i >= 0; i--)
            {
                if (localizationRef[i, 0] == preferredLanguage)
                {
                    StartCoroutine(GetRequest(localizationRef[i, 1], true));
                    isSynchronized = true;
                }
                else if (i == 0 && !isSynchronized)
                    StartCoroutine(GetRequest(localizationRef[i, 1], true));
                else
                    StartCoroutine(GetRequest(localizationRef[i, 1], false));
            }
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetString("Language", "EN");
        }

        string fileName = PlayerPrefs.GetString("Language") + ".json";
        LoadLocalizedText(fileName);
    }

    public void LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.persistentDataPath, "Localization", fileName);

        if (File.Exists(filePath))
        {
            string nameWithoutExtension = fileName.Substring(0, fileName.Length - 5);
            PlayerPrefs.SetString("Language", nameWithoutExtension);

            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            foreach (LocalizedText item in LocalizedText.allInstanceList)
            {
                item.SetText(GetLocalizedValue(item.key));
            }

            isReady = true;
        }
        else isReady = false;
    }

    public string GetLocalizedValue(string key)
    {
        string result = missingTextString;
        if (localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;
    }

    public bool GetIsReady()
    {
        return isReady;
    }

    private IEnumerator GetRequest(string fileName, bool applyAfterCreation)
    {
        string uri = Path.Combine(Application.streamingAssetsPath, fileName);

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.LogError("Cannot find localization file: " + fileName);
            }
            else
            {
                string directoryPath = Path.Combine(Application.persistentDataPath, "Localization", fileName);
                File.WriteAllText(directoryPath, webRequest.downloadHandler.text);

                if (applyAfterCreation) LoadLocalizedText(fileName);
            }
        }
    }
}