﻿using UnityEngine;

public class LockFPS : MonoBehaviour
{
    public int frameRateLimit;

    private void Start()
    {
        Application.targetFrameRate = frameRateLimit;
    }
}